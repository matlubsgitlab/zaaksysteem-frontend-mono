#!/bin/sh

sed \
    -e "s/PROXY_HOST/${PROXY_HOST:-localhost}/g" \
        < /etc/nginx/nginx.conf.tpl > /etc/nginx/nginx.conf

exec "$@"
