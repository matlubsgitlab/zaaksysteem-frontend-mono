const createReactAppOverrides = require('@zaaksysteem/common/src/createReactAppOverrides');
const packageJSON = require('./package.json');

module.exports = createReactAppOverrides(packageJSON.homepage);
