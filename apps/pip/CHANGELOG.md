# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.6.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.5...@zaaksysteem/pip@0.6.6) (2020-04-14)

**Note:** Version bump only for package @zaaksysteem/pip





## [0.6.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.4...@zaaksysteem/pip@0.6.5) (2020-04-10)

**Note:** Version bump only for package @zaaksysteem/pip





## [0.6.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.3...@zaaksysteem/pip@0.6.4) (2020-04-09)

**Note:** Version bump only for package @zaaksysteem/pip





## [0.6.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.2...@zaaksysteem/pip@0.6.3) (2020-04-07)

**Note:** Version bump only for package @zaaksysteem/pip





## [0.6.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.1...@zaaksysteem/pip@0.6.2) (2020-04-06)

**Note:** Version bump only for package @zaaksysteem/pip





## [0.6.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.6.0...@zaaksysteem/pip@0.6.1) (2020-04-03)


### Bug Fixes

* **PipDocuments:** MINTY-3619 - fix PipDocuments not showing all rows ([67cf987](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/67cf987))





# [0.6.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.5.11...@zaaksysteem/pip@0.6.0) (2020-04-02)


### Features

* **Communication:** MINTY-3432 - add attachment PDF preview ([5bebd92](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5bebd92))





## [0.5.11](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.5.10...@zaaksysteem/pip@0.5.11) (2020-04-01)

**Note:** Version bump only for package @zaaksysteem/pip





## [0.5.10](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.5.9...@zaaksysteem/pip@0.5.10) (2020-04-01)

**Note:** Version bump only for package @zaaksysteem/pip





## [0.5.9](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.5.8...@zaaksysteem/pip@0.5.9) (2020-04-01)

**Note:** Version bump only for package @zaaksysteem/pip





## [0.5.8](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.5.7...@zaaksysteem/pip@0.5.8) (2020-03-31)

**Note:** Version bump only for package @zaaksysteem/pip





## [0.5.7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.5.6...@zaaksysteem/pip@0.5.7) (2020-03-31)

**Note:** Version bump only for package @zaaksysteem/pip





## [0.5.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.5.5...@zaaksysteem/pip@0.5.6) (2020-03-30)

**Note:** Version bump only for package @zaaksysteem/pip





## [0.5.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.5.4...@zaaksysteem/pip@0.5.5) (2020-03-30)

**Note:** Version bump only for package @zaaksysteem/pip





## [0.5.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.5.3...@zaaksysteem/pip@0.5.4) (2020-03-30)

**Note:** Version bump only for package @zaaksysteem/pip





## [0.5.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.5.2...@zaaksysteem/pip@0.5.3) (2020-03-27)

**Note:** Version bump only for package @zaaksysteem/pip





## [0.5.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.5.1...@zaaksysteem/pip@0.5.2) (2020-03-20)

**Note:** Version bump only for package @zaaksysteem/pip





## [0.5.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.5.0...@zaaksysteem/pip@0.5.1) (2020-03-19)

**Note:** Version bump only for package @zaaksysteem/pip





# [0.5.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.4.2...@zaaksysteem/pip@0.5.0) (2020-03-19)


### Features

* **DocumentExplorer:** MINTY-3300 - add initial PDF preview features, misc. ([a131814](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a131814))
* **DocumentExplorer:** MINTY-3300 - change Loader, exports ([b25d770](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/b25d770))
* **DocumentExplorer:** MINTY-3300 - split up DocumentExplorer functionality, create new Intake entrypoint, add Selectable state to table ([5bae0d3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5bae0d3))
* **Intake:** MINTY-3437 - add search component ([2bb171b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/2bb171b))





## [0.4.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.4.1...@zaaksysteem/pip@0.4.2) (2020-03-17)

**Note:** Version bump only for package @zaaksysteem/pip





## [0.4.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.4.0...@zaaksysteem/pip@0.4.1) (2020-03-17)

**Note:** Version bump only for package @zaaksysteem/pip





# [0.4.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.3.23...@zaaksysteem/pip@0.4.0) (2020-03-05)


### Bug Fixes

* **CaseDocuments:** apply MR comments ([778fd18](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/778fd18))


### Features

* **CaseDocuments:** add misc features ([8845760](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8845760))
* **CaseDocuments:** MINTY-3043, MINTY-3045: get documents from server, implement breadcrumb, error handling ([9519313](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/9519313))
* **CaseDocuments:** MINTY-3150 - show file status if not accepted ([7425526](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7425526))
* **CaseDocuments:** MINTY-3191 - add misc. UX tweaks, code layout ([11658c1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/11658c1))
* **CaseDocuments:** MINTY-3254 - set canUpload capability according to query parameter ([b98bc1e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/b98bc1e))
* **Documents:** MINTY-2861 - add initial CaseDocuments ([237e411](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/237e411))





## [0.3.23](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.3.22...@zaaksysteem/pip@0.3.23) (2020-03-03)

**Note:** Version bump only for package @zaaksysteem/pip





## [0.3.22](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.3.21...@zaaksysteem/pip@0.3.22) (2020-02-24)

**Note:** Version bump only for package @zaaksysteem/pip





## [0.3.21](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.3.20...@zaaksysteem/pip@0.3.21) (2020-02-21)

**Note:** Version bump only for package @zaaksysteem/pip





## [0.3.20](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.3.19...@zaaksysteem/pip@0.3.20) (2020-02-17)

**Note:** Version bump only for package @zaaksysteem/pip





## [0.3.19](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.3.18...@zaaksysteem/pip@0.3.19) (2020-02-12)

**Note:** Version bump only for package @zaaksysteem/pip





## [0.3.18](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.3.17...@zaaksysteem/pip@0.3.18) (2020-02-12)

**Note:** Version bump only for package @zaaksysteem/pip





## [0.3.17](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.3.16...@zaaksysteem/pip@0.3.17) (2020-02-12)

**Note:** Version bump only for package @zaaksysteem/pip





## [0.3.16](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.3.15...@zaaksysteem/pip@0.3.16) (2020-02-10)

**Note:** Version bump only for package @zaaksysteem/pip





## [0.3.15](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.3.14...@zaaksysteem/pip@0.3.15) (2020-02-07)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.3.14](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.3.13...@zaaksysteem/pip@0.3.14) (2020-02-06)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.3.13](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.3.12...@zaaksysteem/pip@0.3.13) (2020-02-06)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.3.12](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.3.11...@zaaksysteem/pip@0.3.12) (2020-02-06)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.3.11](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.3.10...@zaaksysteem/pip@0.3.11) (2020-01-30)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.3.10](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.3.9...@zaaksysteem/pip@0.3.10) (2020-01-30)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.3.9](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.3.8...@zaaksysteem/pip@0.3.9) (2020-01-23)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.3.8](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.3.7...@zaaksysteem/pip@0.3.8) (2020-01-23)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.3.7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.3.6...@zaaksysteem/pip@0.3.7) (2020-01-22)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.3.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.3.5...@zaaksysteem/pip@0.3.6) (2020-01-14)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.3.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.3.3...@zaaksysteem/pip@0.3.5) (2020-01-14)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.3.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.3.3...@zaaksysteem/pip@0.3.4) (2020-01-13)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.3.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.3.2...@zaaksysteem/pip@0.3.3) (2020-01-13)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.3.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.3.1...@zaaksysteem/pip@0.3.2) (2020-01-10)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.3.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.3.0...@zaaksysteem/pip@0.3.1) (2020-01-09)

**Note:** Version bump only for package @zaaksysteem/pip

# [0.3.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.2.6...@zaaksysteem/pip@0.3.0) (2020-01-09)

### Bug Fixes

- **Communication:** MINTY-2262 - allow e-mail addresses over multiple lines ([db44093](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/db44093))
- **UI:** Fix aria-labels on snackbar and dialogs ([775a287](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/775a287))

### Features

- **Communication:** MINTY-1798 Add `canImportMessage` capability ([c10c88e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c10c88e))
- **Communication:** MINTY-1925 Make deleting a message controllable via a capability ([704ee17](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/704ee17))
- **Communication:** MINTY-2034 Call `/get_thread_list` with correct params according to context of communication-module ([7fef0f2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7fef0f2))
- **i18n:** Move common locale namespace to `@zaaksysteem/common` package ([4a36e92](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4a36e92))
- **Message:** Allow source files of messages to be added to the case documents ([6819f60](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6819f60))
- **Thread:** Allow threads to be linked to cases from main view ([44a8a51](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/44a8a51))

## [0.2.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.2.5...@zaaksysteem/pip@0.2.6) (2019-12-19)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.2.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.2.4...@zaaksysteem/pip@0.2.5) (2019-11-21)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.2.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.2.3...@zaaksysteem/pip@0.2.4) (2019-10-29)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.2.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.2.2...@zaaksysteem/pip@0.2.3) (2019-10-25)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.2.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.2.1...@zaaksysteem/pip@0.2.2) (2019-10-22)

**Note:** Version bump only for package @zaaksysteem/pip

## [0.2.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/pip@0.2.0...@zaaksysteem/pip@0.2.1) (2019-10-17)

**Note:** Version bump only for package @zaaksysteem/pip

# 0.2.0 (2019-10-17)

### Features

- **Snackbar:** MINTY-1788 Implement snackbar in pip and main app ([dcbe4da](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/dcbe4da))
