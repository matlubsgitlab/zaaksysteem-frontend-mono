import React from 'react';
import { useTranslation } from 'react-i18next';
import CommonErrorDialog, {
  ErrorDialogPropsType as CommonErrorDialogPropsType,
} from '@zaaksysteem/common/src/components/ErrorDialog/ErrorDialog';
// @ts-ignore
import useMessages from '@zaaksysteem/common/src/library/useMessages';

type ErrorDialogPropsType = Pick<
  CommonErrorDialogPropsType,
  'onClose' | 'errorCode'
> & {
  [key: string]: any;
};

const ErrorDialog: React.FunctionComponent<ErrorDialogPropsType> = ({
  errorCode,
  onClose,
  ...restProps
}) => {
  const [t] = useTranslation('common');
  const [messages] = useMessages();

  const registeredMessage = messages[errorCode];
  const title = t('dialog.error.title');
  const body = registeredMessage ? registeredMessage : t('dialog.error.body');

  return (
    // @ts-ignore
    <CommonErrorDialog
      {...restProps}
      errorCode={errorCode}
      onClose={onClose}
      body={body}
      title={title}
      primaryButtonText={t('dialog.ok')}
    />
  );
};

export default ErrorDialog;
