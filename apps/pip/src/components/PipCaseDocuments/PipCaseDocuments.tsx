import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { APIDocument } from '@zaaksysteem/generated';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
// @ts-ignore
import locale from '@zaaksysteem/common/src/components/DocumentExplorer/locale/locale';
import CreateDocumentsDialog from '@zaaksysteem/common/src/components/dialogs/CreateDocumentsDialog/CreateDocumentsDialog';
import useDocumentExplorer from '@zaaksysteem/common/src/components/DocumentExplorer/useDocumentExplorer';
// @ts-ignore
import Button from '@mintlab/ui/App/Material/Button';
import FileExplorer from '@zaaksysteem/common/src/components/DocumentExplorer/components/FileExplorer/FileExplorer';
import Search from '@zaaksysteem/common/src/components/DocumentExplorer/components/DocumentExplorer/library/Search';
import {
  PresetLocations,
  HandleItemClickType,
  StoreShapeType,
} from '@zaaksysteem/common/src/components/DocumentExplorer/components/DocumentExplorer/types/types';
import { DirectoryItemType } from '@zaaksysteem/common/src/components/DocumentExplorer/components/FileExplorer/types/FileExplorerTypes';
import BreadCrumbs from '@zaaksysteem/common/src/components/DocumentExplorer/components/DocumentExplorer/library/BreadCrumbs';
import { useDocumentExplorerStyles } from '@zaaksysteem/common/src/components/DocumentExplorer/components/DocumentExplorer/DocumentExplorer.style';
import { getColumns } from '@zaaksysteem/common/src/components/DocumentExplorer/components/DocumentExplorer/utils/getColumns';
import { buildUrl, ENCODED_NULL_BYTE } from '@mintlab/kitchen-sink/source';

type PipCaseDocumentsPropsType = {
  caseUUID: string;
  capabilities: {
    canDownload: boolean;
    canUpload: boolean;
    canSearch: boolean;
  };
};

const CaseDocuments: React.ComponentType<PipCaseDocumentsPropsType> = ({
  caseUUID,
  capabilities,
}) => {
  const getURL = (state: StoreShapeType) => {
    const { search, location } = state;

    const getFilter = () =>
      location === PresetLocations.Search
        ? { fulltext: search }
        : {
            'relationships.parent.id':
              location === PresetLocations.Home ? ENCODED_NULL_BYTE : location,
          };

    return buildUrl<APIDocument.GetDirectoryEntriesForCaseRequestParams>(
      `/api/v2/document/get_directory_entries_for_case`,
      {
        case_uuid: caseUUID,
        filter: getFilter(),
      }
    );
  };

  const { canSearch, canUpload } = capabilities;
  const classes = useDocumentExplorerStyles();
  const [t] = useTranslation();
  const [state, actions, ServerErrorDialog] = useDocumentExplorer({
    t,
    getURL,
  });
  const {
    doRefreshAction,
    doSearchAction,
    doNavigateAction,
    doFileOpenAction,
    doSearchCloseAction,
  } = actions;
  const { items, location, loading, path } = state;
  const [createCaseDocumentsOpen, setCreateCaseDocumentsOpen] = useState(false);

  const handleItemClick: HandleItemClickType = (event, item) => {
    event.stopPropagation();
    event.preventDefault();
    doNavigateAction(item.id);
  };
  const handleFolderOpen = (folder: DirectoryItemType) =>
    doNavigateAction(folder.uuid);
  const handleOpenCreateDocuments = () => setCreateCaseDocumentsOpen(true);
  const handleCreateDocumentsConfirm = () => {
    doRefreshAction();
    setCreateCaseDocumentsOpen(false);
  };
  const handleCreateDocumentsClose = () => setCreateCaseDocumentsOpen(false);

  const { name, mimeType, modified, download } = getColumns({
    t,
    classes,
    handleFileOpen: doFileOpenAction,
  });

  const columns = [name({}), mimeType(), modified(), download()];

  const inFolderLocation = ![
    PresetLocations.Home as string,
    PresetLocations.Search as string,
  ].includes(location);

  return (
    <I18nResourceBundle resource={locale} namespace="DocumentExplorer">
      <div className={classes.wrapper}>
        {ServerErrorDialog}
        <div className={classes.search}>
          {canSearch ? (
            <Search onChange={doSearchAction} onClose={doSearchCloseAction} />
          ) : null}
        </div>
        <div className={classes.toolbar}>
          <BreadCrumbs
            path={path}
            location={location}
            handleItemClick={handleItemClick}
            classes={classes}
          />

          {canUpload && location !== PresetLocations.Search ? (
            <div className={classes.actionButtons}>
              <Button
                action={handleOpenCreateDocuments}
                presets={['text', 'primary', 'large']}
                classes={{
                  label: classes.actionButtonLabel,
                }}
              >
                {t('DocumentExplorer:addFiles.button')}
              </Button>
              <CreateDocumentsDialog
                onConfirm={handleCreateDocumentsConfirm}
                onClose={handleCreateDocumentsClose}
                open={createCaseDocumentsOpen}
                caseUUID={caseUUID}
                {...(inFolderLocation && { directoryUUID: location })}
              />
            </div>
          ) : null}
        </div>
        <div className={classes.table}>
          <FileExplorer
            items={items}
            columns={columns}
            onFolderOpen={handleFolderOpen}
            onFileOpen={doFileOpenAction}
            loading={loading}
            noRowsMessage={t('DocumentExplorer:table.noRows')}
            rowHeight={50}
            selectable={false}
          />
        </div>
      </div>
    </I18nResourceBundle>
  );
};

export default CaseDocuments;
