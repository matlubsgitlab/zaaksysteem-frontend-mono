import { RouterState } from 'connected-react-router';
import { UIState } from '@zaaksysteem/common/src/store/ui/ui.reducer';

export interface State {
  router: RouterState;
  ui: UIState;
}
