export default {
  nl: {
    modules: {
      dashboard: 'Dashboard',
      object: 'Object',
    },
    breadcrumbBar: {
      new: 'Nieuw',
    },
  },
};
