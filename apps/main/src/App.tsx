import React, { useEffect, Suspense } from 'react';
import { hot } from 'react-hot-loader/root';
import { ConnectedRouter } from 'connected-react-router';
import { Provider } from 'react-redux';
import { useTranslation } from 'react-i18next';
//@ts-ignore
import MaterialUiThemeProvider from '@mintlab/ui/App/Material/MaterialUiThemeProvider/MaterialUiThemeProvider';
//@ts-ignore
import Loader from '@mintlab/ui/App/Zaaksysteem/Loader';
//@ts-ignore
import ErrorBoundary from '@zaaksysteem/common/src/components/ErrorBoundary/ErrorBoundary';
import DialogRenderer from '@zaaksysteem/common/src/components/DialogRenderer/DialogRenderer';
import SnackbarRenderer from '@zaaksysteem/common/src/components/SnackbarRenderer/SnackbarRenderer';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import useDialogs from '@zaaksysteem/common/src/library/useDialogs';
import useMessages from '@zaaksysteem/common/src/library/useMessages';
import { configureStore, history } from './configureStore';
import Routes from './Routes';
import ErrorDialog from './components/ErrorDialog/ErrorDialog';
import { DIALOG_ERROR } from './constants/dialog.constants';
import locale from './locale/main.locale';

const initialState = {};
const store = configureStore(initialState);

function App() {
  const [t] = useTranslation('common');
  const [, addDialogs] = useDialogs();
  const [, addMessages] = useMessages();

  useEffect(() => {
    addMessages(
      t('serverErrors.status', {
        returnObjects: true,
      })
    );

    addDialogs({
      [DIALOG_ERROR]: ErrorDialog,
    });
  }, []);

  return (
    <Provider store={store}>
      <MaterialUiThemeProvider>
        <I18nResourceBundle namespace="main" resource={locale}>
          <Suspense fallback={<Loader delay={200} />}>
            <ConnectedRouter history={history}>
              <Routes prefix={process.env.APP_CONTEXT_ROOT || ''} />
            </ConnectedRouter>
            <ErrorBoundary>
              <DialogRenderer />
              <SnackbarRenderer />
            </ErrorBoundary>
          </Suspense>
        </I18nResourceBundle>
      </MaterialUiThemeProvider>
    </Provider>
  );
}

export default process.env.NODE_ENV === 'development' ? hot(App) : App;
