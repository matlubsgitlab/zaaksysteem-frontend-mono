import { createBrowserHistory } from 'history';
import { createStore } from 'redux-dynamic-modules-core';
import { getThunkExtension } from 'redux-dynamic-modules-thunk';
import { login } from '@zaaksysteem/common/src/library/auth';
import { getUrl } from '@zaaksysteem/common/src/library/url';
import getErrorModule from '@zaaksysteem/common/src/store/error/error.module';
import getRouterModule from './store/route/route.module';
import getSessionModule from './store/session/session.module';
import getUIModule from './store/ui/ui.module';
import { DIALOG_ERROR } from './constants/dialog.constants';

export const history = createBrowserHistory();

export const configureStore = (initialState: any) => {
  const store = createStore(
    { initialState, extensions: [getThunkExtension()] },
    getRouterModule(history),
    getSessionModule(),
    getUIModule(),
    getErrorModule({
      errorDialogType: DIALOG_ERROR,
      handleAuthenticationError: () => login(getUrl()),
    })
  );

  return store;
};
