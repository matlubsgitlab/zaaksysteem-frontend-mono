export default {
  nl: {
    navigation: {
      back: 'Terug',
    },
    note: 'Notitie',
    addNewTask: 'Toevoegen',
    deleteTask: 'Verwijder taak',
    serverErrors: {},
    fields: {
      title: {
        placeholder: 'Titel',
      },
      description: {
        placeholder: 'Notitie',
      },
      due_date: {
        label: 'Deadline',
        placeholder: 'Datum toevoegen',
      },
      assignee: {
        label: 'Toewijzing',
        placeholder: 'Zoek een medewerker...',
      },
    },
    form: {
      reOpen: 'Heropenen',
    },
  },
};
