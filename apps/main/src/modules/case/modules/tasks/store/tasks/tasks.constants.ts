import { createAjaxConstants } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';

export const TASKS_FETCH = createAjaxConstants('TASKS_FETCH');
export const TASK_CREATE = createAjaxConstants('TASK_CREATE');
export const TASK_EDIT = createAjaxConstants('TASK_EDIT');
export const TASK_DELETE = createAjaxConstants('TASK_DELETE');
export const TASK_SET_COMPLETE_STATUS = createAjaxConstants(
  'TASK_SET_COMPLETE_STATUS'
);
