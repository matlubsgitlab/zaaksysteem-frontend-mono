export interface TasksContextType {
  caseUuid: string;
  rootPath: string;
  phase: number;
}
