import React from 'react';
import { Link } from 'react-router-dom';
import i18next from 'i18next';
//@ts-ignore
import { Button } from '@mintlab/ui/App/Material/Button';
import Tooltip from '@mintlab/ui/App/Material/Tooltip/Tooltip';
import { CaseTask } from '../../../types/List.types';
import { useToolbarStyles } from './Toolbar.style';
//@ts-ignore

export type ToolbarPropsType = {
  setCompletionAction: (completed: boolean) => void;
  deleteAction: () => void;
  rootPath: string;
  isValid: boolean;
  t: i18next.TFunction;
} & Pick<CaseTask, 'can_set_completion' | 'completed' | 'is_editable'>;

/*eslint complexity: ["error", 10]*/
const Toolbar = ({
  deleteAction,
  rootPath,
  completed,
  is_editable,
  t,
}: ToolbarPropsType) => {
  const classes = useToolbarStyles();

  const LinkComponent = React.forwardRef<HTMLAnchorElement>((props, ref) => (
    <Link innerRef={ref} to={rootPath} {...props} />
  ));
  LinkComponent.displayName = 'BackButton';

  return (
    <div className={classes.wrapper}>
      <Tooltip
        title={t('tasks:navigation.back')}
        classes={{
          wrapper: classes.tooltipWrapper,
        }}
      >
        <Button component={LinkComponent} presets={['icon', 'medium']}>
          arrow_back
        </Button>
      </Tooltip>

      {is_editable && !completed && (
        <Tooltip
          title={t('tasks:deleteTask')}
          classes={{
            wrapper: classes.tooltipWrapper,
          }}
        >
          <Button action={deleteAction} presets={['icon', 'medium']}>
            delete
          </Button>
        </Tooltip>
      )}
    </div>
  );
};

export default Toolbar;
