import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { TasksRootStateType } from '../../../store/tasks.reducer';
import { fetchTasksAction } from '../../../store/tasks/tasks.actions';
import { setCompletion } from '../../../store/tasks/tasks.actions';
import { TaskListPropsType, TaskList } from './TaskList';

type PropsFromStateType = Pick<
  TaskListPropsType,
  'tasks' | 'initialBootOrPending' | 'tasksContext'
>;

const mapStateToProps = ({
  tasks: {
    context,
    list: { state, data },
  },
}: TasksRootStateType): PropsFromStateType => {
  const completed = data.filter(task => task.completed).reverse();
  const nonCompleted = data.filter(task => !task.completed).reverse();
  const tasks = [...nonCompleted, ...completed];

  return {
    initialBootOrPending: state === 'init' || state === 'pending',
    tasksContext: context,
    tasks,
  };
};

type PropsFromDispatchType = Pick<
  TaskListPropsType,
  'fetchTasks' | 'completeTask'
>;

const mapDispatchToProps = (dispatch: Dispatch): PropsFromDispatchType => ({
  fetchTasks(caseContext) {
    const action = fetchTasksAction(caseContext);
    if (action !== null) {
      dispatch(action as any);
    }
  },
  completeTask(task) {
    const action = setCompletion(task.task_uuid, true);
    if (action !== null) {
      dispatch(action as any);
    }
  },
});

export const TaskListContainer = connect<
  PropsFromStateType,
  PropsFromDispatchType,
  {},
  TasksRootStateType
>(
  mapStateToProps,
  mapDispatchToProps
)(TaskList);
