import { makeStyles } from '@material-ui/core';

export const useToolbarStyles = makeStyles(() => ({
  wrapper: {
    display: 'flex',
    justifyContent: 'space-between',
    marginBottom: 20,
    flex: 0,
  },
  tooltipWrapper: {
    width: 'auto',
  },
}));
