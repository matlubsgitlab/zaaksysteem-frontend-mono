import React from 'react';
import { useTranslation } from 'react-i18next';
// @ts-ignore
import Button from '@mintlab/ui/App/Material/Button';
// @ts-ignore
import { TextField } from '@mintlab/ui/App/Material/TextField';
import { TasksContextType } from '../../../types/Context.types';
import { useTaskCreatorStyles } from './TaskCreator.style';

const MAX_TITLE_LENGTH = 253;

export type TaskCreatorPropsType = {
  onCreateTask: (title: string, tasksContext: TasksContextType) => void;
  tasksContext: TasksContextType;
};

export const TaskCreator: React.ComponentType<TaskCreatorPropsType> = ({
  onCreateTask,
  tasksContext,
}) => {
  const [t] = useTranslation();
  const classes = useTaskCreatorStyles();
  const [creatingTask, setCreatingTask] = React.useState(false);
  const [taskTitle, setTaskTitle] = React.useState('');

  const submitTask = () => {
    if (taskTitle) {
      onCreateTask(taskTitle.slice(0, MAX_TITLE_LENGTH), tasksContext);
    }
    setTaskTitle('');
  };

  const handleKeyPress = (ev: any) => {
    if (ev.key.toLowerCase() === 'enter') {
      submitTask();
    } else if (ev.key.toLowerCase() === 'escape') {
      setTaskTitle('');
      setCreatingTask(false);
    }
  };

  const handleBlur = () => {
    setTaskTitle('');
    setCreatingTask(false);
  };

  return (
    <div className={classes.container}>
      <Button
        icon="add"
        iconSize="medium"
        classes={{ root: classes.button }}
        action={() => setCreatingTask(true)}
      >
        {t('tasks:addNewTask')}
      </Button>
      {creatingTask && (
        <TextField
          // eslint-disable-next-line jsx-a11y/no-autofocus
          autoFocus={true}
          onBlur={handleBlur}
          value={taskTitle}
          onChange={(ev: any) => setTaskTitle(ev.target.value)}
          onKeyDown={handleKeyPress}
        />
      )}
    </div>
  );
};
