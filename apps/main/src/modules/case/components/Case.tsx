import React from 'react';
import { Route, Switch } from 'react-router-dom';
import CommunicationModule from '@zaaksysteem/communication-module/src';
//@ts-ignore
import Loader from '@mintlab/ui/App/Zaaksysteem/Loader';
import TaskModule from './../modules/tasks';

export interface CasePropsType {
  caseUuid: string;
  contactUuid: string;
  contactName: string;
  busy: boolean;
  status: string | null;
  canCreatePipMessage: boolean;
}

/* eslint complexity: [2, 9] */
const Case: React.ComponentType<CasePropsType> = ({
  caseUuid,
  contactUuid,
  contactName,
  busy,
  status,
  canCreatePipMessage,
}) => {
  if (busy) {
    return <Loader />;
  }

  const caseOpen = status !== 'resolved';

  return (
    <React.Fragment>
      <Switch>
        <Route
          path={`/:prefix/case/:caseId/communication`}
          render={({ match }) => (
            <CommunicationModule
              capabilities={{
                allowSplitScreen: true,
                canAddAttachmentToCase: caseOpen,
                canAddSourceFileToCase: caseOpen,
                canAddThreadToCase: false,
                canCreateContactMoment: true,
                canCreatePipMessage: caseOpen && canCreatePipMessage,
                canCreateEmail:
                  process.env.WEBPACK_BUILD_TARGET !== 'production' && caseOpen,
                canCreateNote: true,
                canCreateMijnOverheid: false,
                canDeleteMessage: caseOpen,
                canImportMessage: caseOpen,
                canSelectCase: false,
                canSelectContact: true,
                canFilter: true,
                canOpenPDFPreview: true,
              }}
              context="case"
              caseUuid={caseUuid}
              contactUuid={contactUuid}
              contactName={contactName}
              rootPath={match.url}
            />
          )}
        />
        <Route
          path={`/:prefix/case/:caseId/phase/:phase/tasks`}
          render={({
            match: {
              params: { phase, caseId },
              url,
            },
          }) => <TaskModule phase={phase} caseUuid={caseId} rootPath={url} />}
        />
      </Switch>
    </React.Fragment>
  );
};

export default Case;
