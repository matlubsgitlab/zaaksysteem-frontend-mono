import { Reducer, AnyAction } from 'redux';
import { AjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import {
  AJAX_STATE_INIT,
  AjaxState,
} from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';
import handleAjaxStateChange from '@zaaksysteem/common/src/library/redux/ajax/handleAjaxStateChange';
import { DashboardCaseType } from '../types/Dashboard.types';
import { FETCH_OPEN_CASES } from './dashboard.contstants';

export interface DashboardStateType {
  cases: DashboardCaseType[];
  state: AjaxState;
}

export interface DashboardRootStateType {
  dashboard: DashboardStateType;
}

const initialState: DashboardStateType = {
  cases: [],
  state: AJAX_STATE_INIT,
};

type FetchDashboardCasesResponseBodyType = {
  result: {
    id: string;
    values: {
      'case.casetype.name': string;
      'case.number': number;
      'case.requestor.uuid': string;
      'case.requestor.name': string;
    };
  }[];
};

const normalizeCases = (
  response: FetchDashboardCasesResponseBodyType
): Omit<DashboardStateType, 'state'> => {
  const { result } = response;
  return {
    cases: result.map(item => ({
      id: item.id,
      caseType: item.values['case.casetype.name'],
      caseNumber: item.values['case.number'],
      requestor: {
        id: item.values['case.requestor.uuid'],
        name: item.values['case.requestor.name'],
      },
    })),
  };
};

const handleOpenCasesStateChange = handleAjaxStateChange(FETCH_OPEN_CASES);

export const dashboardReducer: Reducer<DashboardStateType, AnyAction> = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case FETCH_OPEN_CASES.PENDING:
    case FETCH_OPEN_CASES.ERROR:
      return {
        ...state,
        ...handleOpenCasesStateChange(state, action as AjaxAction<{}>),
      };

    case FETCH_OPEN_CASES.SUCCESS:
      return {
        ...handleOpenCasesStateChange(
          state,
          action as AjaxAction<FetchDashboardCasesResponseBodyType>
        ),
        ...normalizeCases(action.payload.response),
      };

    default:
      return state;
  }
};
