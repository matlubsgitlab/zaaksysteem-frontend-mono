import React, { useState } from 'react';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
//@ts-ignore
import Loader from '@mintlab/ui/App/Zaaksysteem/Loader';
//@ts-ignore
import DataProvider from '@mintlab/ui/App/Abstract/DataProvider';
import { request } from '@zaaksysteem/common/src/library/fetch';
import { DocumentPreviewModal } from '@zaaksysteem/common/src/components/DocumentPreview/DocumentPreviewModal';
import { DashboardCaseType } from '../../types/Dashboard.types';

interface CaseDocumentsPropsType {
  cases: DashboardCaseType[];
}

const fetchCaseDocuments = async (caseId: string) => {
  const response = await request('GET', `/api/case/${caseId}/directory/tree`);
  const data = await response.json();

  return data?.result?.[0]?.files || [];
};

export const CaseDocuments: React.ComponentType<CaseDocumentsPropsType> = ({
  cases,
}) => {
  const options = cases.map(item => ({
    value: item.caseNumber,
    label: `${item.caseNumber} - ${item.caseType}`,
  }));
  const [selectedCase, setSelectedCase] = useState<any>(null);
  const [selectedFile, setSelectedFile] = useState<any>(null);

  const renderList = (data: any) => {
    const items = data.map((item: any) => (
      <li key={item.id}>
        {item.filestore_id.original_name}{' '}
        <button
          type="button"
          onClick={() => {
            setSelectedFile(item);
          }}
        >
          Open preview
        </button>
      </li>
    ));

    return <ul>{items}</ul>;
  };

  return (
    <div>
      <div>
        <p>Select case</p>
        <Select
          options={options}
          onChange={(event: any) => setSelectedCase(event.target.value)}
        />
      </div>
      <div>
        {selectedCase !== null && (
          <DataProvider
            provider={fetchCaseDocuments}
            providerArguments={[selectedCase.value]}
          >
            {({ data, busy }: any) => {
              if (busy) {
                return <Loader />;
              }

              if (data) {
                return renderList(data);
              }
            }}
          </DataProvider>
        )}
      </div>

      {selectedCase && selectedFile && (
        <DocumentPreviewModal
          url={`/zaak/${selectedCase.value}/document/${selectedFile.id}/download/pdf`}
          onClose={() => setSelectedFile(null)}
          title={selectedFile.name}
          open={true}
        />
      )}
    </div>
  );
};
