import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AJAX_STATE_PENDING } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';
import { showSnackbar } from '@zaaksysteem/common/src/store/ui/snackbar/snackbar.actions';
import { DashboardRootStateType } from '../store/dashboard.reducer';
import Dashboard, { DashboardPropsType } from './Dashboard';

type PropsFromStateType = Pick<
  DashboardPropsType,
  'cases' | 'contacts' | 'loading'
>;

const mapStateToProps = (state: DashboardRootStateType): PropsFromStateType => {
  if (!state.dashboard) {
    return {
      cases: [],
      contacts: {},
      loading: true,
    };
  }

  const {
    dashboard: { cases, state: dashboardState },
  } = state;

  const contacts = cases.reduce(
    (acc, item) => ({
      ...acc,
      [item.requestor.id]: item.requestor.name,
    }),
    {}
  );

  return { cases, contacts, loading: dashboardState === AJAX_STATE_PENDING };
};

type PropsFromDispatchType = Pick<DashboardPropsType, 'showSnack'>;

const mapDispatchToProps = (dispatch: Dispatch): PropsFromDispatchType => ({
  showSnack() {
    dispatch(showSnackbar('test-message'));
  },
});

const CaseContainer = connect<
  PropsFromStateType,
  PropsFromDispatchType,
  {},
  DashboardRootStateType
>(
  mapStateToProps,
  mapDispatchToProps
)(Dashboard);

export default CaseContainer;
