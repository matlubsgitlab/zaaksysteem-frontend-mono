export default {
  nl: {
    attributes: {
      title: 'Kenmerken',
      customAttributes: {
        title: 'Specifieke kenmerken',
        description: 'De eigen kenmerken van dit object.',
      },
      systemAttributes: {
        title: 'Algemene kenmerken',
        description: 'De systeemkenmerken van dit object.',
        uuid: 'UUID',
        status: 'Status',
        registrationDate: 'Registratiedatum',
        lastModified: 'Gewijzigd op',
      },
    },
    status: {
      active: 'Actief',
      inactive: 'Inactief',
      draft: 'Concept',
    },
  },
};
