import { IModule } from 'redux-dynamic-modules';
import objectViewReducer, {
  ObjectViewRootStateType,
} from './objectView.reducer';
import { fetchObject } from './object/object.actions';

export function getObjectViewModule(
  uuid: string
): IModule<ObjectViewRootStateType> {
  return {
    id: 'objectView',
    reducerMap: {
      objectView: objectViewReducer as any,
    },
    initialActions: [fetchObject(uuid) as any],
  };
}
