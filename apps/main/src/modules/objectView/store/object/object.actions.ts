import { createAjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { FETCH_OBJECT } from './object.constants';

const fetchObjectAction = createAjaxAction(FETCH_OBJECT);

export type FetchObjectActionPayloadType = {
  uuid: string;
};

export const fetchObject = (uuid: string) =>
  fetchObjectAction<FetchObjectActionPayloadType>({
    url: buildUrl(`/api/v2/cm/custom_object/get_custom_object`, {
      uuid,
    }),
    method: 'GET',
  });
