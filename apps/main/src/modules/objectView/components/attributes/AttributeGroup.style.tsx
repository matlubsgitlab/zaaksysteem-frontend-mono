import { makeStyles } from '@material-ui/core';

export const useAttributeGroupStyles = makeStyles(
  ({ mintlab: { greyscale } }: any) => ({
    wrapper: { width: '100%', padding: '20 0' },
    attributeListHeader: {},
    attributeListTitle: {
      padding: '5px 20px',
    },
    attributeListDescription: {
      padding: '5px 20px',
      color: greyscale.evenDarker,
    },
    attributeListContent: {
      padding: 20,
      '&>*:first-child': {
        borderTop: `1px solid ${greyscale.darker}`,
      },
      '&>*': {
        borderBottom: `1px solid ${greyscale.darker}`,
      },
    },
  })
);
