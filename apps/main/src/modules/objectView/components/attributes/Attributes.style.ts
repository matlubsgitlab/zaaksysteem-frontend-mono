import { makeStyles } from '@material-ui/core';

export const useAttributesStyles = makeStyles(() => ({
  wrapper: {
    width: '100%',
    padding: 12,
  },
  header: {
    padding: 20,
  },
}));
