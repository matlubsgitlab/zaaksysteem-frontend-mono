import { makeStyles } from '@material-ui/core';

export const useObjectViewStyles = makeStyles(() => ({
  wrapper: {
    width: '100%',
  },
}));
