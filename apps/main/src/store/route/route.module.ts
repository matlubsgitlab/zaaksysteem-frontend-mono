import {
  connectRouter,
  routerMiddleware,
  RouterRootState,
} from 'connected-react-router';
import { IModule } from 'redux-dynamic-modules';
import { History } from 'history';
import { iframeMiddleware } from './iframe.middleware';

export const getRouterModule = (
  history: History
): IModule<RouterRootState> => ({
  id: 'router',
  reducerMap: {
    router: connectRouter(history) as any,
  },
  middlewares: [routerMiddleware(history), iframeMiddleware],
});

export default getRouterModule;
