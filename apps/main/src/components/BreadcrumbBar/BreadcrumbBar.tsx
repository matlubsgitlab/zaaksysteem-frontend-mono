import React from 'react';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import Breadcrumbs, {
  defaultItemRenderer,
} from '@mintlab/ui/App/Material/Breadcrumbs/Breadcrumbs';
import {
  BreadcrumbItemType,
  BreadcrumbRendererType,
} from '@mintlab/ui/App/Material/Breadcrumbs/Breadcrumbs.types';
//@ts-ignore
import Button from '@mintlab/ui/App/Material/Button';
//@ts-ignore
import Icon from '@mintlab/ui/App/Material/Icon';
//@ts-ignore
import { GenericTextField } from '@mintlab/ui/App/Material/TextField';
import { useBreadcrumbBarStyles } from './BreadcrumbBar.styles';

export type BreadcrumbBarPropsType = {
  breadcrumbs: BreadcrumbItemType[];
};

const itemRenderer: BreadcrumbRendererType = itemProps => {
  const { item, index, classes } = itemProps;

  return item.path.indexOf('/main') === -1 ? (
    defaultItemRenderer(itemProps)
  ) : (
    <Link key={index} to={item.path} className={classes.link}>
      {item.label}
    </Link>
  );
};

export const BreadcrumbBar: React.ComponentType<BreadcrumbBarPropsType> = ({
  breadcrumbs,
}) => {
  const [t] = useTranslation('main');
  const classes = useBreadcrumbBarStyles();

  return (
    <header className={classes.wrapper}>
      <div className={classes.breadCrumbs}>
        <Breadcrumbs items={breadcrumbs} itemRenderer={itemRenderer} />
      </div>
      <div className={classes.search}>
        <GenericTextField startAdornment={<Icon size="small">search</Icon>} />
      </div>
      <div className={classes.new}>
        <Button presets={['primary', 'contained']} icon="add">
          {t('breadcrumbBar.new')}
        </Button>
      </div>
    </header>
  );
};

export default BreadcrumbBar;
