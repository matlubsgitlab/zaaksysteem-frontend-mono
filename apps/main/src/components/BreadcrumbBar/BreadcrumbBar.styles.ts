import { makeStyles } from '@material-ui/core';

export const useBreadcrumbBarStyles = makeStyles(
  ({ mintlab: { greyscale } }: any) => ({
    wrapper: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      padding: '16px 32px',
      borderBottom: `1px solid ${greyscale.dark}`,
    },
    breadCrumbs: {
      flex: 1,
    },
    search: {
      flex: 0,
      minWidth: 320,
      paddingLeft: 20,
      paddingRight: 20,
    },
    new: {
      flex: 0,
    },
  })
);
