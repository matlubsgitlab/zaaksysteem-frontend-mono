import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import classNames from 'classnames';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
// @ts-ignore
import locale from '@zaaksysteem/common/src/components/DocumentExplorer/locale/locale';
import CreateDocumentsDialog from '@zaaksysteem/common/src/components/dialogs/CreateDocumentsDialog/CreateDocumentsDialog';
import useDocumentExplorer from '@zaaksysteem/common/src/components/DocumentExplorer/useDocumentExplorer';
import { getColumns } from '@zaaksysteem/common/src/components/DocumentExplorer/components/DocumentExplorer/utils/getColumns';
import { isDocument } from '@zaaksysteem/common/src/components/DocumentExplorer/components/DocumentExplorer/utils/isDocument';
// @ts-ignore
import Button from '@mintlab/ui/App/Material/Button';
import FileExplorer from '@zaaksysteem/common/src/components/DocumentExplorer/components/FileExplorer/FileExplorer';
import {
  PresetLocations,
  StoreShapeType,
} from '@zaaksysteem/common/src/components/DocumentExplorer/components/DocumentExplorer/types/types';
import { DirectoryItemType } from '@zaaksysteem/common/src/components/DocumentExplorer/components/FileExplorer/types/FileExplorerTypes';
import { useDocumentExplorerStyles } from '@zaaksysteem/common/src/components/DocumentExplorer/components/DocumentExplorer/DocumentExplorer.style';
import { DocumentPreviewModal } from '@zaaksysteem/common/src/components/DocumentPreview';
import Search from '@zaaksysteem/common/src/components/DocumentExplorer/components/DocumentExplorer/library/Search';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { APIDocument } from '@zaaksysteem/generated';
import Menu from './Menu/Menu';

const IntakeDocuments: React.ComponentType = () => {
  const classes = useDocumentExplorerStyles();
  const [t] = useTranslation();
  const getURL = (state: StoreShapeType) => {
    const { search, location } = state;

    return buildUrl<APIDocument.GetDirectoryEntriesForIntakeRequestParams>(
      '/api/v2/document/get_directory_entries_for_intake',
      {
        filter: location === PresetLocations.Search ? { fulltext: search } : {},
      }
    );
  };

  const [state, actions, ServerErrorDialog] = useDocumentExplorer({
    t,
    getURL,
  });

  const {
    doRefreshAction,
    doNavigateAction,
    doToggleSelectAction,
    doFileOpenAction,
    doSearchAction,
    doSearchCloseAction,
  } = actions;
  const { items, location, loading } = state;
  const [createCaseDocumentsOpen, setCreateCaseDocumentsOpen] = useState(false);
  const [previewOpen, setPreviewOpen] = useState(false);
  const [preview, setPreview] = useState('');

  const handleFolderOpen = (folder: DirectoryItemType) =>
    doNavigateAction(folder.uuid);
  const handleOpenCreateDocuments = () => setCreateCaseDocumentsOpen(true);
  const handleCreateDocumentsConfirm = () => {
    doRefreshAction();
    setCreateCaseDocumentsOpen(false);
  };
  const handleCreateDocumentsClose = () => setCreateCaseDocumentsOpen(false);
  const handleRowClick = ({ rowData }: { rowData: DirectoryItemType }) => {
    if (!isDocument(rowData)) return;
    doToggleSelectAction(rowData);
  };
  const { name, mimeType, modified, download } = getColumns({
    t,
    classes,
    handleFileOpen: doFileOpenAction,
  });
  const columns = [
    name({
      fileNameAction: item => {
        if (item.preview) {
          return (event: React.MouseEvent) => {
            event.preventDefault();
            event.stopPropagation();
            if (item.preview) {
              setPreview(item.preview);
              setPreviewOpen(true);
            }
          };
        }
      },
    }),
    mimeType(),
    modified(),
    download(),
  ];
  const inFolderLocation = ![PresetLocations.Home as string].includes(location);

  return (
    <I18nResourceBundle resource={locale} namespace="DocumentExplorer">
      <div className={classNames(classes.wrapper)}>
        <DocumentPreviewModal
          open={previewOpen}
          title={'Document preview'}
          url={preview}
          onClose={() => setPreviewOpen(false)}
        />
        {ServerErrorDialog}
        <div className={classes.search}>
          <Search onChange={doSearchAction} onClose={doSearchCloseAction} />
        </div>
        <div className={classes.toolbar}>
          <div className={classes.actionButtons}>
            <Menu state={state} actions={actions} />
            <Button
              action={handleOpenCreateDocuments}
              presets={['text', 'primary', 'large']}
              classes={{
                label: classes.actionButtonLabel,
              }}
            >
              {t('DocumentExplorer:addFiles.button')}
            </Button>
            <CreateDocumentsDialog
              onConfirm={handleCreateDocumentsConfirm}
              onClose={handleCreateDocumentsClose}
              open={createCaseDocumentsOpen}
              {...(inFolderLocation && { directoryUUID: location })}
            />
          </div>
        </div>
        <FileExplorer
          items={items}
          columns={columns}
          onFolderOpen={handleFolderOpen}
          onRowClick={handleRowClick}
          onFileOpen={doFileOpenAction}
          loading={loading}
          noRowsMessage={t('DocumentExplorer:table.noRows')}
          rowHeight={50}
          selectable={true}
        />
      </div>
    </I18nResourceBundle>
  );
};

export default IntakeDocuments;
