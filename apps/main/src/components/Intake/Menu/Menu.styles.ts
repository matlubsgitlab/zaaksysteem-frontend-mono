import { makeStyles } from '@material-ui/core';

export const useMenuStyles = makeStyles(({ palette: { elephant } }: any) => {
  return {
    buttons: {
      marginRight: 10,
      color: elephant.dark,
      display: 'flex',
    },
  };
});
