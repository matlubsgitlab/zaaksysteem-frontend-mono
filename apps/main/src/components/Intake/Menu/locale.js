const locale = {
  nl: {
    newCase: 'Nieuwe zaak aanmaken',
    addToCase: 'Aan zaak toevoegen',
    assign: 'Toewijzen',
    delete: 'Verwijderen',
    properties: 'Eigenschappen',
  },
};

export default locale;
