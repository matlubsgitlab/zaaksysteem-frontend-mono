import React, { Fragment } from 'react';
import { useTranslation } from 'react-i18next';
//@ts-ignore
import Icon from '@mintlab/ui/App/Material/Icon';
//@ts-ignore
import IconButton from '@material-ui/core/IconButton';
//@ts-ignore
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import { StoreShapeType } from '@zaaksysteem/common/src/components/DocumentExplorer/components/DocumentExplorer/types/types';
import { useMenuStyles } from './Menu.styles';
import locale from './locale';

type MenuPropsType = {
  state: StoreShapeType;
  actions: any;
};

const Menu = ({ state, actions }: MenuPropsType) => {
  const { items } = state;
  const classes = useMenuStyles();
  const [t] = useTranslation('IntakeMenu');

  const selected = items.reduce(
    (acc: number, current) => (current.selected == true ? acc + 1 : acc),
    0
  );

  const buttons = {
    newCase: (
      <Tooltip title={t('newCase')}>
        <IconButton onClick={() => {}} color="inherit">
          <Icon size="small">add</Icon>
        </IconButton>
      </Tooltip>
    ),
    addToCase: (
      <Tooltip title={t('addToCase')}>
        <IconButton onClick={() => {}} color="inherit">
          <Icon size="small">help</Icon>
        </IconButton>
      </Tooltip>
    ),
    assign: (
      <Tooltip title={t('assign')}>
        <IconButton onClick={() => {}} color="inherit">
          <Icon size="small">favorite</Icon>
        </IconButton>
      </Tooltip>
    ),
    delete: (
      <Tooltip title={t('delete')}>
        <IconButton onClick={() => {}} color="inherit">
          <Icon size="small">delete</Icon>
        </IconButton>
      </Tooltip>
    ),
    properties: (
      <Tooltip title={t('properties')}>
        <IconButton onClick={() => {}} color="inherit">
          <Icon size="small">help</Icon>
        </IconButton>
      </Tooltip>
    ),
  };

  if (selected === 0) return null;

  return (
    <div className={classes.buttons}>
      {selected === 1 ? (
        <Fragment>
          {buttons.newCase}
          {buttons.addToCase}
          {buttons.assign}
          {buttons.delete}
          {buttons.properties}
        </Fragment>
      ) : (
        <Fragment>
          {buttons.addToCase} {buttons.assign} {buttons.delete}
        </Fragment>
      )}
    </div>
  );
};

/* eslint-disable-next-line */
export default (props: MenuPropsType) => (
  <I18nResourceBundle resource={locale} namespace="IntakeMenu">
    <Menu {...props} />
  </I18nResourceBundle>
);
