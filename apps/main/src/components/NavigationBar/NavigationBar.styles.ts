import { makeStyles } from '@material-ui/core';

export const useNavigationBarStyles = makeStyles((theme: any) => {
  const {
    palette: { lizard },
  } = theme;

  return {
    wrapper: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'flex-end',
      height: '100%',
      overflowX: 'hidden',
      padding: '10px 0',
      width: 70,
      backgroundColor: lizard.dark,
    },
    open: {
      width: 260,
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    close: {
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
    },
    menuButtonWrapper: {
      flex: 0,
      width: 70,
      textAlign: 'center',
    },
    list: {
      width: '100%',
    },
    icon: {
      paddingLeft: 5,
    },
  };
});
