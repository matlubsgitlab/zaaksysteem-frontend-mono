import React, { useState } from 'react';
import classnames from 'classnames';
import { useTranslation } from 'react-i18next';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
//@ts-ignore
import Button from '@mintlab/ui/App/Material/Button';
import Icon from '@mintlab/ui/App/Material/Icon/Icon';
import { useNavigationBarStyles } from './NavigationBar.styles';

export const NavigationBar: React.ComponentType<{}> = () => {
  const classes = useNavigationBarStyles();
  const [t] = useTranslation('main');
  const [open, setOpen] = useState(false);

  const handleToggle = () => setOpen(!open);

  return (
    <nav
      className={classnames(
        classes.wrapper,
        open ? classes.open : classes.close
      )}
    >
      <div className={classes.menuButtonWrapper}>
        <Button action={handleToggle} presets={['icon']} color="inherit">
          {open ? 'close' : 'menu'}
        </Button>
      </div>

      <List disablePadding classes={{ root: classes.list }}>
        <ListItem button component="a" href="/intern">
          <ListItemIcon classes={{ root: classes.icon }}>
            <Icon>home</Icon>
          </ListItemIcon>
          <ListItemText>{t('modules.dashboard')}</ListItemText>
        </ListItem>
      </List>
    </nav>
  );
};

export default NavigationBar;
