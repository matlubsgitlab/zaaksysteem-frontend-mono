import React from 'react';
import NavigationBar from '../NavigationBar/NavigationBar';
import { useLayoutStyles } from './Layout.styles';

export const Layout: React.ComponentType<{}> = ({ children }) => {
  const classes = useLayoutStyles();

  return (
    <div className={classes.wrapper}>
      <div className={classes.navigation}>
        <NavigationBar />
      </div>
      <div className={classes.content}>{children}</div>
    </div>
  );
};
