import { makeStyles } from '@material-ui/core';

export const useLayoutStyles = makeStyles({
  wrapper: {
    display: 'flex',
    flexDirection: 'row',
    width: '100vw',
    height: '100vh',
  },
  navigation: {
    flex: 0,
  },
  content: {
    flex: 1,
    overflow: 'auto',
  },
});
