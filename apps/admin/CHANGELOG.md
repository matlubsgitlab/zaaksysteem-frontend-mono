# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.16.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.15.6...@zaaksysteem/admin@0.16.0) (2020-04-15)


### Features

* **ObjectTypeManagement:** MINTY-3627 Allow updating object types ([35f3998](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/35f3998))





## [0.15.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.15.5...@zaaksysteem/admin@0.15.6) (2020-04-14)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.15.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.15.4...@zaaksysteem/admin@0.15.5) (2020-04-14)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.15.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.15.3...@zaaksysteem/admin@0.15.4) (2020-04-10)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.15.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.15.2...@zaaksysteem/admin@0.15.3) (2020-04-09)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.15.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.15.1...@zaaksysteem/admin@0.15.2) (2020-04-06)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.15.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.15.0...@zaaksysteem/admin@0.15.1) (2020-04-03)

**Note:** Version bump only for package @zaaksysteem/admin





# [0.15.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.14.0...@zaaksysteem/admin@0.15.0) (2020-04-01)


### Features

* **ObjectTypeManagement:** Implement Dialog with warning of pending changes that will be lost when leaving the form ([d6ef897](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/d6ef897))





# [0.14.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.13.0...@zaaksysteem/admin@0.14.0) (2020-03-31)


### Features

* **ObjectTypeManagement:** MINTY-3473 Implement `afronden` step in wizard ([857b929](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/857b929))





# [0.13.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.12.0...@zaaksysteem/admin@0.13.0) (2020-03-31)


### Features

* **ObjectTypeManagement:** MINTY-3519 Implement add object type action ([a1df778](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a1df778))





# [0.12.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.11.1...@zaaksysteem/admin@0.12.0) (2020-03-30)


### Features

* **ObjectTypeManagement:** MINTY-3469 Create first page of ObjectTypeManagement wizard ([d1b8822](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/d1b8822))





## [0.11.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.11.0...@zaaksysteem/admin@0.11.1) (2020-03-30)

**Note:** Version bump only for package @zaaksysteem/admin





# [0.11.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.10.0...@zaaksysteem/admin@0.11.0) (2020-03-30)


### Features

* **FormRenderer:** Refactor formRenderer to hook and update Formik to latest version 2.1.4 ([c5d1e71](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c5d1e71))





# [0.10.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.9.2...@zaaksysteem/admin@0.10.0) (2020-03-27)


### Features

* **ObjectTypeManagement:** MINTY-3466 - Implement page for objecttype management ([375d69d](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/375d69d))





## [0.9.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.9.1...@zaaksysteem/admin@0.9.2) (2020-03-23)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.9.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.9.0...@zaaksysteem/admin@0.9.1) (2020-03-20)


### Bug Fixes

* **Catalog:** Rename objecttype v2 ([fb79e15](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/fb79e15))





# [0.9.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.8.2...@zaaksysteem/admin@0.9.0) (2020-03-19)


### Features

* **Catalog:** MINTY-3344 - Add support for custom object types ([55ee940](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/55ee940))





## [0.8.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.8.1...@zaaksysteem/admin@0.8.2) (2020-03-19)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.8.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.8.0...@zaaksysteem/admin@0.8.1) (2020-03-17)

**Note:** Version bump only for package @zaaksysteem/admin





# [0.8.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.7.10...@zaaksysteem/admin@0.8.0) (2020-03-05)


### Features

* **CaseDocuments:** add misc features ([8845760](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8845760))





## [0.7.10](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.7.9...@zaaksysteem/admin@0.7.10) (2020-03-03)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.7.9](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.7.8...@zaaksysteem/admin@0.7.9) (2020-02-24)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.7.8](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.7.7...@zaaksysteem/admin@0.7.8) (2020-02-21)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.7.7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.7.6...@zaaksysteem/admin@0.7.7) (2020-02-17)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.7.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.7.5...@zaaksysteem/admin@0.7.6) (2020-02-10)

**Note:** Version bump only for package @zaaksysteem/admin





## [0.7.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.7.4...@zaaksysteem/admin@0.7.5) (2020-02-07)

**Note:** Version bump only for package @zaaksysteem/admin

## [0.7.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.7.3...@zaaksysteem/admin@0.7.4) (2020-02-06)

**Note:** Version bump only for package @zaaksysteem/admin

## [0.7.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.7.2...@zaaksysteem/admin@0.7.3) (2020-02-06)

**Note:** Version bump only for package @zaaksysteem/admin

## [0.7.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.7.1...@zaaksysteem/admin@0.7.2) (2020-01-10)

**Note:** Version bump only for package @zaaksysteem/admin

## [0.7.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.7.0...@zaaksysteem/admin@0.7.1) (2020-01-09)

### Bug Fixes

- **Admin:** fix Suspense / loading error ([289b4b8](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/289b4b8))

# [0.7.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.6.7...@zaaksysteem/admin@0.7.0) (2020-01-09)

### Bug Fixes

- **Communication:** MINTY-2262 - allow e-mail addresses over multiple lines ([db44093](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/db44093))
- **UI:** Fix aria-labels on snackbar and dialogs ([775a287](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/775a287))

### Features

- **Forms:** MINTY-2323 - add generic FormDialog component and implement throughout ([7f46465](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7f46465))
- **ThreadList:** Add tooltip to message counter ([78965a4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/78965a4))

## [0.6.7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.6.6...@zaaksysteem/admin@0.6.7) (2019-12-19)

**Note:** Version bump only for package @zaaksysteem/admin

## [0.6.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.6.5...@zaaksysteem/admin@0.6.6) (2019-11-27)

**Note:** Version bump only for package @zaaksysteem/admin

## [0.6.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.6.4...@zaaksysteem/admin@0.6.5) (2019-11-21)

**Note:** Version bump only for package @zaaksysteem/admin

## [0.6.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.6.3...@zaaksysteem/admin@0.6.4) (2019-11-14)

**Note:** Version bump only for package @zaaksysteem/admin

## [0.6.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.6.2...@zaaksysteem/admin@0.6.3) (2019-11-05)

**Note:** Version bump only for package @zaaksysteem/admin

## [0.6.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.6.1...@zaaksysteem/admin@0.6.2) (2019-10-25)

**Note:** Version bump only for package @zaaksysteem/admin

## [0.6.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.6.0...@zaaksysteem/admin@0.6.1) (2019-10-17)

**Note:** Version bump only for package @zaaksysteem/admin

# [0.6.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.5.5...@zaaksysteem/admin@0.6.0) (2019-10-17)

### Bug Fixes

- change tag color, check for internalUrl in Details view ([89f58c3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/89f58c3))
- **Catalog:** Fix spelling on dutch copy translations ([15234ea](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/15234ea))
- **Catalog:** MINTY-1999: prevent an empty list of casetypes in 'used in case types', optional truncating ([943fcbb](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/943fcbb))

### Features

- **Catalog:** MINTY-1551, MINTY-1548: add clickable foldername and url to internal registrationform to details, refactor large parts of the view code ([031bfff](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/031bfff))
- **Theme:** adjust some theming for better contrast ([33d10c6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/33d10c6))

## [0.5.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.5.4...@zaaksysteem/admin@0.5.5) (2019-10-11)

**Note:** Version bump only for package @zaaksysteem/admin

## [0.5.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.5.3...@zaaksysteem/admin@0.5.4) (2019-09-30)

**Note:** Version bump only for package @zaaksysteem/admin

## [0.5.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.5.2...@zaaksysteem/admin@0.5.3) (2019-09-26)

**Note:** Version bump only for package @zaaksysteem/admin

## [0.5.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.5.1...@zaaksysteem/admin@0.5.2) (2019-09-09)

**Note:** Version bump only for package @zaaksysteem/admin

## [0.5.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.5.0...@zaaksysteem/admin@0.5.1) (2019-09-06)

**Note:** Version bump only for package @zaaksysteem/admin

# [0.5.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.4.0...@zaaksysteem/admin@0.5.0) (2019-09-05)

### Features

- MINTY-1620: adjust presets for buttons in dialogs/forms ([81adac4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/81adac4))

# [0.4.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.3.2...@zaaksysteem/admin@0.4.0) (2019-08-29)

### Features

- **Alert:** MINTY-1126 Centralize `Alert` component so it can be used in multiple applications ([c44ec56](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c44ec56))
- **Communication:** MINTY-1115 Add `SearchField` to `Theads` ([5ec462a](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5ec462a))
- **Layout:** Let menu buttons be anchors to allow opening in new tab ([67f6910](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/67f6910))
- **UI:** Upgrade to MUI4 ([ac921f6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ac921f6))

## [0.3.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.3.1...@zaaksysteem/admin@0.3.2) (2019-08-27)

**Note:** Version bump only for package @zaaksysteem/admin

## [0.3.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.3.0...@zaaksysteem/admin@0.3.1) (2019-08-22)

### Bug Fixes

- **admin:** add trailing slash to homepage ([eb5c176](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/eb5c176))

# [0.3.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.2.0...@zaaksysteem/admin@0.3.0) (2019-08-22)

### Features

- **admin:** Always start from homepage ([0375a5d](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/0375a5d))
- **Admin:** Activate new catalog for trial and master ([7e3a731](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7e3a731))
- **apps:** Prevent automatic opening of browser tabs on start ([4740421](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4740421))

# [0.2.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.1.1...@zaaksysteem/admin@0.2.0) (2019-08-12)

### Features

- **ProximaNova:** MINTY-1306 Change font family to Proxima Nova ([2e3009f](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/2e3009f))

## [0.1.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/admin@0.1.0...@zaaksysteem/admin@0.1.1) (2019-08-12)

**Note:** Version bump only for package @zaaksysteem/admin

# 0.1.0 (2019-08-06)

### Features

- **Admin:** MINTY-1281 Add admin build to nginx ([2b58ef2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/2b58ef2))
- **Admin:** MINTY-1281 Move admin app to mono repo ([f6a1d35](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/f6a1d35))
