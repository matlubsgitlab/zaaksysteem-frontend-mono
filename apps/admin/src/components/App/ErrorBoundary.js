import { Component, createElement } from 'react';
import Exception from '../Exception';

export default class ErrorBoundary extends Component {
  state = {
    exception: null,
  };

  componentDidCatch({ message: reason }, { componentStack }) {
    this.setState({
      exception: {
        reason,
        trace: this.format(componentStack),
      },
    });
  }

  render() {
    const {
      props: { children },
      state: { exception },
    } = this;

    if (exception) {
      return createElement(Exception, exception);
    }

    return children;
  }

  format(stacktrace) {
    return stacktrace.trim().replace(/\n\s+/gm, '\n');
  }
}
