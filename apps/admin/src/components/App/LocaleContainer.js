import { connect } from 'react-redux';
import { default as locale } from '../../locale/index';
import Locale from './Locale';

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
  locale,
});
const LocaleContainer = connect(null, null, mergeProps)(Locale);

export default LocaleContainer;
