import React from 'react';
import { withStyles } from '@material-ui/styles';
import { subAppHeaderStylesheet } from './SubAppHeader.style';

/**
 * @reactProps {*} children
 * @reactProps {Object} classes
 * @return {ReactElement}
 */
export const SubAppHeader = ({ children, classes }) => (
  <header className={classes.header}>{children}</header>
);

export default withStyles(subAppHeaderStylesheet)(SubAppHeader);
