import React from 'react';
import Render from '@mintlab/ui/App/Abstract/Render';
import ColoredIcon from './../../Icons/ColoredIcon';

/**
 * @param {Object} props
 * @param {string} props.value
 * @return {ReactElement}
 */
export const IconCell = ({ value }) => (
  <Render condition={value}>
    <ColoredIcon size="large" value={value} />
  </Render>
);

export default IconCell;
