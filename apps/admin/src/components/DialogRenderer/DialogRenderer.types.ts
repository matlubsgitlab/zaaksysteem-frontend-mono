import i18next from 'i18next';

export type DialogPropsType = {
  hide: () => void;
  invoke: (options: { path: string; force?: boolean }) => void;
  t: i18next.TFunction;
  options: any;
};
