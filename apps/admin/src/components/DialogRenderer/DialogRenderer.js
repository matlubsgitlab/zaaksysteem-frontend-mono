import React from 'react';
import { withStyles } from '@material-ui/styles';
import { DiscardChangesDialog } from '../Dialogs/DiscardChangesDialog';
import * as SystemConfigDialogs from '../../modules/systemConfiguration/components/Dialogs';
import * as CatalogDialogs from '../../modules/catalog/components/Dialogs';
import * as SharedDialogs from '../Dialogs';
import { dialogRendererStylesheet } from './DialogRenderer.style';

const allDialogs = {
  DiscardChangesDialog,
  ...SystemConfigDialogs,
  ...CatalogDialogs,
  ...SharedDialogs,
};

/**
 * Renders a specific dialog, based on the type defined in `ui.dialog.type`
 *
 * @param {Object} props
 * @return {ReactElement}
 */
const DialogRenderer = props => (
  <React.Fragment>
    {props.dialogs.map((dialog, index) => {
      const { dialogType, options } = dialog;

      if (!dialogType) return null;

      const Dialog = allDialogs[dialogType];

      return (
        <Dialog key={index} {...props} dialog={dialog} options={options} />
      );
    })}
  </React.Fragment>
);

export default withStyles(dialogRendererStylesheet)(DialogRenderer);
