export const CHECKBOX = 'checkbox';
export const EMAIL = 'email';
export const LOCATION_SELECT = 'locationSelect';
export const MAGICSTRING = 'magicString';
export const NAME = 'name';
export const OPTIONS = 'options';
export const PRODUCT_SELECT = 'productSelect';
export const SELECT = 'select';
export const TEXT = 'text';
export const TEXTAREA = 'textarea';
export const TYPES = 'types';
export const VERSIONS = 'versions';
export const FILE_SELECT = 'file';
export const FLATVALUE_SELECT = 'flatValueSelect';
export const ATTRIBUTE_SELECT = 'attributeSelect';
