import React from 'react';
import {
  getFormDefinitionAfterRules,
  getValuesDiff,
  getValuesFromDefinition,
  isFormDefinitionUpdated,
} from './helpers';

export const withRules = ({ Component, setFormDefinition }) => {
  return class RulesEngine extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        busy: true,
      };
    }

    componentDidMount() {
      this.update();
    }

    componentDidUpdate() {
      this.update();
    }

    render() {
      const { formDefinition } = this.props;

      if (this.state.busy) {
        return null;
      }
      return <Component {...this.props} formDefinition={formDefinition} />;
    }

    update() {
      const { setValues, rules, formDefinition, values } = this.props;
      const updatedFormDefinition = getFormDefinitionAfterRules(
        rules,
        formDefinition,
        values
      );

      const updatedValues = getValuesFromDefinition(updatedFormDefinition);
      const valueDiff = getValuesDiff(values, updatedValues);
      const hasUpdatedValues = Object.keys(valueDiff).length;
      const hasUpdatedFromDefinition = isFormDefinitionUpdated(
        this.props.formDefinition,
        updatedFormDefinition,
        values
      );

      if (hasUpdatedValues) {
        setValues({
          ...values,
          ...valueDiff,
        });
      }

      if (hasUpdatedFromDefinition) {
        setFormDefinition(updatedFormDefinition);
      } else if (this.state.busy) {
        this.setState({ busy: false });
      }
    }
  };
};

export default withRules;
