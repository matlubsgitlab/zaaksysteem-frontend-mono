const updateFields = (fieldNames, update) => field => ({
  ...field,
  ...(fieldNames.includes(field.name) ? update : {}),
});

export const hideFields = fieldNames => fields =>
  fields.map(updateFields(fieldNames, { hidden: true }));

export const showFields = fieldNames => fields =>
  fields.map(updateFields(fieldNames, { hidden: false }));

export const setFieldValue = (fieldName, value) => fields =>
  fields.map(updateFields([fieldName], { value }));

export const setRequired = fieldNames => fields =>
  fields.map(updateFields(fieldNames, { required: true }));

export const setOptional = fieldNames => fields =>
  fields.map(updateFields(fieldNames, { required: false }));

export const updateField = (fieldName, update) => fields =>
  fields.map(updateFields([fieldName], update));
