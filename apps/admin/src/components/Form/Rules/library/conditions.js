import deepEqual from 'fast-deep-equal';

const normalizeValue = value =>
  value &&
  typeof value === 'object' &&
  Object.prototype.hasOwnProperty.call(value, 'value')
    ? value.value
    : value;

export const valueEquals = equals => value =>
  deepEqual(equals, normalizeValue(value));

export const valueOneOf = equals => value => {
  return equals.includes(normalizeValue(value));
};

export const valueBiggerThan = biggerThan => value => value > biggerThan;

export const valueSmallerThan = smallerThan => value => value < smallerThan;
