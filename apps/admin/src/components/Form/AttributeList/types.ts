export type AttributeItemType = {
  title: string;
  name: string;
  id: string;
};
