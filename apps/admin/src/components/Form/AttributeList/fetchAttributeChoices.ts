import { buildUrl } from '@mintlab/kitchen-sink/source';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import { APICatalog } from '@zaaksysteem/generated';
import { request } from '../../../library/fetch';
import { AttributeItemType } from './types';

export const fetchAttributeChoices = (
  search_string: string
): Promise<ValueType<AttributeItemType>[]> =>
  request(
    'GET',
    buildUrl<APICatalog.SearchAttributeByNameRequestParams>(
      '/api/v2/admin/catalog/attribute_search',
      {
        search_string,
      }
    )
  )
    .then((requestPromise: any) => requestPromise.json())
    .then((response: APICatalog.SearchAttributeByNameResponseBody) => {
      return response.data.map(({ attributes: { name }, id }) => {
        return { value: { id: id || '', name, title: '' }, label: name };
      });
    })
    .catch(response => Promise.reject(response));
