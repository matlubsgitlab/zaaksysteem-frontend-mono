import { makeStyles } from '@material-ui/core/styles';
import { Theme } from '@mintlab/ui/types/Theme';

export const useAttributeListItemStyle = makeStyles(
  ({ typography, palette: { primary } }: Theme) => ({
    mainContainer: {
      padding: '0 5px',
      '&>.MuiTextField-root': {
        borderRadius: 8,
        padding: 0,
        '&>div>input': {
          marginLeft: 9,
        },
      },
    },
    headerContainer: {
      display: 'flex',
      justifyContent: 'space-between',
      paddingBottom: 15,
    },
    actionsContainer: {
      display: 'flex',
      '&>.MuiButton-text': {
        padding: '0 5px',
        borderRadius: 4,
        color: primary.main,
      },
      '&>.MuiIconButton-root': {
        padding: 0,
        marginLeft: 5,
        borderRadius: 4,
      },
    },
    attributeName: {
      ...typography.h5,
      margin: 0,
      display: 'flex',
      alignItems: 'center',
    },
  })
);
