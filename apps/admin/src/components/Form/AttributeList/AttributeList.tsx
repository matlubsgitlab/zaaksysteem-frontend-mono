import React from 'react';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
//@ts-ignore
import { DataProvider } from '@mintlab/ui/App/Abstract/DataProvider';
//@ts-ignore
import Icon from '@mintlab/ui/App/Material/Icon';
import {
  SortableList,
  ListContainer,
  useList,
} from '@mintlab/ui/App/Zaaksysteem/List';
import { createSelectStyleSheet } from './selectOverride.style';
import { fetchAttributeChoices } from './fetchAttributeChoices';
import { AttributeItem } from './AttributeItem';
import { AttributeItemType } from './types';

type AttributeSelectorPropsType = {
  value: AttributeItemType[];
  name: string;
  placeholder: string;
  onChange: any;
};

const extractSelectedValue = (event: any) => event.target.value.value;

export const AttributeList: React.ComponentType<AttributeSelectorPropsType> = ({
  value,
  name,
  placeholder,
  onChange,
}) => {
  const [input, setInput] = React.useState('');
  const callOnChange = React.useCallback(
    (nextValue: any) =>
      onChange({
        target: {
          value: nextValue,
          type: 'select',
          name,
        },
      }),
    [value, name]
  );

  const { addItem, updateItem, removeItem } = useList(value, callOnChange);

  return (
    <ListContainer>
      <SortableList
        value={value}
        onReorder={callOnChange}
        renderItem={(item, index) => (
          <AttributeItem
            {...item}
            index={index}
            onUpdate={updateItem}
            onRemove={removeItem}
          />
        )}
      />
      <DataProvider
        autoProvide={input !== ''}
        provider={fetchAttributeChoices}
        providerArguments={[input]}
      >
        {({ data, busy }: { data: any[]; busy: boolean; provide: any }) => {
          return (
            <Select
              name={name}
              generic={true}
              loading={busy}
              onChange={event => addItem(extractSelectedValue(event))}
              startAdornment={<Icon size="small">search</Icon>}
              isMulti={false}
              value={null}
              isClearable={false}
              choices={
                input.length > 2 && data
                  ? data.filter(option =>
                      Boolean(
                        (value || []).every(item => item.id !== option.value.id)
                      )
                    )
                  : []
              }
              getChoices={setInput}
              createStyleSheet={createSelectStyleSheet}
              placeholder={placeholder}
            />
          );
        }}
      </DataProvider>
    </ListContainer>
  );
};
