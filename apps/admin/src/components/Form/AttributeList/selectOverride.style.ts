import { StyleSheetCreatorType } from '@mintlab/ui/App/Zaaksysteem/Select/types/SelectStyleSheetType';

const radius = 50;

export const createSelectStyleSheet: StyleSheetCreatorType = ({
  theme: {
    palette: { primary, common },
    typography: { fontFamily },
    mintlab: { greyscale },
    typography,
  },
  focus,
}) => {
  const color = focus ? primary.dark : greyscale.offBlack;
  const colorRule = {
    color,
    '&:hover': {
      color,
    },
  };

  return {
    container(base) {
      return {
        ...base,
        width: '100%',
        fontFamily,
        fontWeight: typography.fontWeightLight,
        border: focus
          ? `2px solid ${primary.dark}`
          : `2px dashed ${greyscale.evenDarker}`,
        borderRadius: radius,
      };
    },
    indicatorSeparator(base) {
      return {
        ...base,
        display: 'none',
      };
    },
    control(base) {
      return {
        ...base,
        height: '32px',
        minHeight: '32px',
        boxShadow: 'none',
        border: 'none',
        borderRadius: radius,
        borderBottom: 'none',
        backgroundColor: focus ? 'white' : greyscale.dark,
        paddingLeft: '16px',
        '& .startAdornment': {
          ...colorRule,
          margin: '3px 8px 0px 0px',
        },
      };
    },
    valueContainer(base) {
      return {
        ...base,
      };
    },
    placeholder(base) {
      return {
        ...base,
        color: greyscale.offBlack,
        opacity: 0.5,
      };
    },
    dropdownIndicator(base) {
      return {
        display: 'none',
      };
    },
    clearIndicator(base) {
      return {
        ...base,
        ...colorRule,
        padding: 0,
      };
    },
    input(base) {
      return {
        ...base,
        input: {
          fontWeight: typography.fontWeightLight,
          fontFamily,
        },
      };
    },
    option(base, state) {
      return {
        ...base,
        backgroundColor: state.isSelected
          ? greyscale.lighter
          : state.isFocused
          ? greyscale.dark
          : common.white,
        color: greyscale.offBlack,
      };
    },
  };
};
