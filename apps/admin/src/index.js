import '@mintlab/ie11';
import React from 'react';
import ReactDOM from 'react-dom';
import AdminApp from './App';
import { setupI18n } from './i18n';

window.onerror = null;

setupI18n().then(() => {
  ReactDOM.render(<AdminApp />, document.getElementById('zs-app'));
});
