export const changeOnlineStatus = {
  changeOnlineStatus: {
    fields: {
      reason: {
        label:
          'Geef hieronder de reden op voor het {{ type }} zetten van dit zaaktype.',
        placeholder: 'Reden',
      },
    },
  },
};
