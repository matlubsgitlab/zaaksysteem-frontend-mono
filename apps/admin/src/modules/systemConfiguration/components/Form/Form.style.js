import config from './config.svg';

/**
 * @param {Object} theme
 * @return {JSS}
 */
export const formStylesheet = ({ breakpoints }) => ({
  sheet: {
    flex: 1,
    padding: '30px',
    'background-image': `url(${config})`,
    'background-size': '18px',
  },
  form: {
    width: '100%',
    [breakpoints.up('lg')]: {
      maxWidth: breakpoints.values.lg,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  formRow: {
    display: 'flex',
    'flex-wrap': 'wrap',
    [breakpoints.up('lg')]: {
      flex: 'none',
      width: '75%',
    },
    '& + $formRow': {
      marginTop: '28px',
    },
  },
  cardHeader: {
    marginBottom: '20px',
  },
});
