import { createElement } from 'react';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';

const CreatableSelect = props =>
  createElement(Select, {
    creatable: true,
    ...props,
  });

export default CreatableSelect;
