import { connect } from 'react-redux';
import { withTranslation } from 'react-i18next';
import { get, getSegment } from '@mintlab/kitchen-sink/source';
import { showBanner, hideBanner } from '../../../store/ui/ui.actions';
import { invoke } from '../../../store/route/route.actions';
import { getUrl } from '../../../library/url';
import { systemConfiguration as formDefinition } from '../fixtures/systemConfiguration';
import {
  updateChoices,
  choicesMap,
  saveSystemConfiguration,
  discardDone,
} from '../store/systemconfiguration.actions';
import { AJAX_STATE_PENDING } from '../../../library/redux/ajax/createAjaxConstants';
import SystemConfiguration from './SystemConfiguration';

const { assign } = Object;
const TEXTFIELD_ROWS = 10;

/* eslint-disable getter-return */
/**
 * @type {Object}
 */
const categoryIcons = {
  cases: 'folder_shared',
  users: 'supervised_user_circle',
  pip: 'web',
  documents: 'insert_drive_file',
  about: 'fingerprint',
  premium: 'star',
  other: 'card_giftcard',
  deprecated: 'save',
};

/**
 * @param {Object} state
 * @param state.route
 * @param state.ui
 * @return {Object}
 */
const mapStateToProps = ({ route, ui, systemConfiguration }) => ({
  route,
  banners: ui.banners,
  discard: get(systemConfiguration, 'discard'),
  items: systemConfiguration.items,
});

/**
 * @param {Function} dispatch
 * @return {Object}
 */
const mapDispatchToProps = dispatch => ({
  invoke: payload => dispatch(invoke(payload)),
  showBanner: payload => dispatch(showBanner(payload)),
  hideBanner: payload => dispatch(hideBanner(payload)),
  updateChoices: input => dispatch(updateChoices(input)),
  save: items => dispatch(saveSystemConfiguration(items)),
  onDiscard: () => dispatch(discardDone()),
});

/**
 * @param {Object} stateProps
 * @param {Object} dispatchProps
 * @param {Object} ownProps
 * @return {Object}
 */
const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { items } = stateProps;

  if (items.length === 0) {
    return stateProps;
  }
  /**
   * Replace the field name from the configuration file with the actual definition.
   *
   * @param {Array} accumulator
   * @param {string} name
   * @return {Array}
   */
  const reduceField = (accumulator, name) => {
    const item = items.find(resourceItem => resourceItem.name === name);

    if (item) {
      const mergedItem = assign({}, item, {
        get value() {
          return get(item, 'value');
        },
        get getChoices() {
          if (choicesMap[name]) {
            return input => {
              dispatchProps.updateChoices({
                input,
                name,
              });
            };
          }
        },
        get translations() {
          if (['select', 'creatable'].includes(item.type)) {
            return [
              'loading',
              'choose',
              'beginTyping',
              'creatable',
              'create',
            ].reduce((translationAccumulator, entry) => {
              translationAccumulator[`form:${entry}`] = t(`form:${entry}`);
              return translationAccumulator;
            }, {});
          }
        },
        get rows() {
          return TEXTFIELD_ROWS;
        },
        get help() {
          return t(`systemConfiguration:help:${name}`);
        },
        get loading() {
          return item.state === AJAX_STATE_PENDING;
        },
      });

      accumulator.push(mergedItem);
    }

    return accumulator;
  };

  const { segments, t } = ownProps;
  const [routeSegment] = segments;

  /**
   * @param {string} title
   * @param {string} description
   * @param {Object} fields
   * @return {Object}
   */
  const mapFieldSet = ({ title, description, fields }) => ({
    ...(title && { title: t(`systemConfiguration:titles:${title}`) }),
    ...(description && {
      description: t(`systemConfiguration:descriptions:${description}`),
    }),
    fields: fields.reduce(reduceField, []),
  });

  /**
   * @type Array
   */
  const fullMap = formDefinition.map(({ slug, fieldSets }) => ({
    current: routeSegment === slug,
    fieldSets: fieldSets.map(mapFieldSet),
    slug,
  }));

  /**
   * @type Array
   */
  const categories = fullMap.map(({ slug, current }) => ({
    label: t(`systemConfiguration:categories:${slug}`),
    icon: categoryIcons[slug],
    slug,
    current,
  }));

  const [currentCategory] = fullMap.filter(({ current }) => current);
  const fieldSets = get(currentCategory, 'fieldSets');

  if (!currentCategory) {
    const [{ slug: defaultSlug }] = fullMap;
    const segment = getSegment(getUrl());

    ownProps.route({
      force: true,
      path: `/admin/${segment}/${defaultSlug}`,
      replace: true,
    });
  }

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    categories,
    fieldSets,
  };
};

/**
 * Connects {@link SystemConfiguration} with {@link i18next} and the store.
 *
 * @return {ReactElement}
 */
export default withTranslation()(
  connect(mapStateToProps, mapDispatchToProps, mergeProps)(SystemConfiguration)
);
