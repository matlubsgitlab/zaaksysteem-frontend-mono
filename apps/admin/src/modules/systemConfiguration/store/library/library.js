import { asArray, get, reduceMap } from '@mintlab/kitchen-sink/source';

// Helper functions

const { isArray } = Array;

/**
 * Determine if a given value is either falsy or an array without elements.
 *
 * @param {*} value
 * @return {boolean}
 */
export const isEmpty = value => !value || (isArray(value) && !value.length);

/**
 * Determine if a given value is an array of strings.
 *
 * @param {*} value
 * @return {boolean}
 */
export const isArrayOfStrings = value =>
  isArray(value) &&
  value.length &&
  value.every(element => typeof element === 'string');

/**
 * Determine if a given value is made up of other
 * (possibly complex) values
 *
 * @param {*} value
 * @return {boolean}
 */
export const isCollection = value => {
  const rows = get(value, 'instance.rows');
  return rows && isArray(rows);
};

/**
 * Determine if a given value is a database object.
 *
 * @param {*} value
 * @return {boolean}
 */
export const isDbObject = value =>
  value &&
  // ZS-FIXME: accepts arrays (assuming only JSON, more otherwise)
  typeof value === 'object' &&
  Object.prototype.hasOwnProperty.call(value, 'reference') &&
  get(value, 'instance') !== undefined &&
  !get(value, 'instance.rows');

/**
 * Determine if a given value is a select configuration.
 *
 * @param {*} value
 * @return {boolean}
 */
export const isSelect = value =>
  value &&
  typeof value === 'object' &&
  get(value, 'label') !== undefined &&
  get(value, 'value') !== undefined;

/**
 * Determine if a given value is an array of select configurations.
 *
 * @param {*} value
 * @return {boolean}
 */
export const isArrayOfSelects = value =>
  !isEmpty(value) && isArray(value) && value.every(row => isSelect(row));

/**
 * @param {Object} value
 * @return {*}
 */
export const selectToDb = value => value.value;

/**
 * @param {Array} value
 * @return {*}
 */
export const arrayToDb = value => value.map(thisValue => thisValue.value);

/**
 * @param {Object} value
 * @return {*}
 */
export const objectToSelect = value => {
  const {
    instance: { label, alternative_name },
    preview,
    reference,
  } = value;

  if (!reference) {
    return value;
  }

  return {
    value: reference,
    label: label ? label : preview,
    alternativeLabels: asArray(alternative_name),
  };
};

/**
 * @param {Array} value
 * @return {Array}
 */
export const arrayToSelect = value => {
  if (!isArrayOfStrings(value)) {
    return value;
  }

  return value.map(thisValue => ({
    label: thisValue,
    value: thisValue,
  }));
};

/**
 * Transforms field value(s) from app state values to valid backend values
 * @param {*} value
 * @return {*}
 */
export const appToDb = value => {
  const map = new Map([
    [keyValue => isSelect(keyValue), valueValue => selectToDb(valueValue)],
    [
      keyValue => isArrayOfSelects(keyValue),
      valueValue => valueValue.map(thisValue => selectToDb(thisValue)),
    ],
    [
      keyValue => isArrayOfStrings(keyValue),
      valueValue => arrayToDb(valueValue),
    ],
  ]);

  return reduceMap({
    map,
    keyArguments: [value],
    valueArguments: [value],
    fallback: value,
  });
};
