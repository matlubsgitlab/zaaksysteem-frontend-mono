import { asArray, get } from '@mintlab/kitchen-sink/source';
import { handleAjaxStateChange } from '../../../../library/redux/ajax/handleAjaxStateChange';
import { AJAX_STATE_INIT } from '../../../../library/redux/ajax/createAjaxConstants';
import { CATALOG_CHANGE_ONLINE_STATUS } from '../changeOnlineStatus/changeOnlineStatus.constants';
import {
  CATALOG_FETCH,
  CATALOG_TOGGLE_ITEM,
  CATALOG_CLEAR_SELECTED,
} from './items.constants';

const getBreadcrumbItem = (response, path) => {
  const link = get(response, path);
  return link
    ? {
        name: link.name,
        id: link.id,
      }
    : null;
};

export const fetchCatalogSuccess = (state, action) => {
  const { response } = action.payload;
  const data = get(response, 'data');
  const itemsByUuid = data.map(item => item.id);
  const selectedItems = state.selectedItems.filter(uuid =>
    asArray(itemsByUuid).includes(uuid)
  );

  return {
    ...state,
    currentFolderName: get(response, 'links.self.name'),
    currentFolderUUID: get(response, 'links.self.id'),
    items: response.data.map(element => ({
      active: element.attributes.active,
      type: element.attributes.type,
      name: element.attributes.name,
      id: element.id,
    })),
    selectedItems,
    breadcrumbs: {
      current: getBreadcrumbItem(response, 'links.self'),
      parent: getBreadcrumbItem(response, 'links.parent'),
      grandparent: getBreadcrumbItem(response, 'links.grandparent'),
    },
  };
};

export const selectItem = (state, id, multiSelect = false) => ({
  ...state,
  selectedItems: [...(multiSelect ? state.selectedItems : []), id],
});

export const deselectItem = (state, id) => ({
  ...state,
  selectedItems: state.selectedItems.filter(item => item !== id),
});

export const toggleSelectedItem = (state, action) => {
  const { id, multiSelect } = action.payload;
  const shouldDeselectItem = state.selectedItems.includes(id) && multiSelect;

  return shouldDeselectItem
    ? deselectItem(state, id)
    : selectItem(state, id, multiSelect);
};

export const clearItemsWhenSearch = (state, action) => ({
  ...state,
  items: action.payload.query ? [] : state.items,
});

/** Update the item's active state after a succesfull request */
/**
 *
 * @param {Object} state
 * @param {Object} action
 * @returns {Object}
 */
export const handleChangeOnlineStatusSuccess = (state, action) => {
  const { active, id } = action.payload;

  const newItems = state.items.map(item => {
    if (item.id === id) {
      return {
        ...item,
        active,
      };
    }

    return item;
  });

  return {
    ...state,
    items: newItems,
  };
};

export const initialState = {
  state: AJAX_STATE_INIT,
  selectedItems: [],
  items: [],
};

/* eslint complexity: [2, 7] */
export function items(state = initialState, action) {
  const handleAjaxState = handleAjaxStateChange(CATALOG_FETCH);

  switch (action.type) {
    case CATALOG_FETCH.PENDING:
      return handleAjaxState(clearItemsWhenSearch(state, action), action);

    case CATALOG_FETCH.ERROR:
      return handleAjaxState(state, action);

    case CATALOG_FETCH.SUCCESS:
      return fetchCatalogSuccess(handleAjaxState(state, action), action);

    case CATALOG_TOGGLE_ITEM:
      return toggleSelectedItem(state, action);

    case CATALOG_CLEAR_SELECTED:
      return {
        ...state,
        selectedItems: [],
      };

    case CATALOG_CHANGE_ONLINE_STATUS.SUCCESS:
      return handleChangeOnlineStatusSuccess(state, action);

    default:
      return state;
  }
}

export default items;
