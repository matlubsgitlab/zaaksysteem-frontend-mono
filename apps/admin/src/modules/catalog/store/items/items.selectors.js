export const getSelectedItems = state => {
  const {
    catalog: {
      items: { items, selectedItems },
    },
  } = state;

  return selectedItems.map(id => items.find(item => item.id === id));
};

export const getSelectedItem = state => {
  const [first] = getSelectedItems(state);

  return first || null;
};
