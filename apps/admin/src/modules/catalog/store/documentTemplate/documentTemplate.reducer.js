import { get } from '@mintlab/kitchen-sink/source';
import { handleAjaxStateChange } from '../../../../library/redux/ajax/handleAjaxStateChange';
import { AJAX_STATE_INIT } from '../../../../library/redux/ajax/createAjaxConstants';
import {
  CATALOG_DOCUMENT_TEMPLATE_INIT,
  CATALOG_DOCUMENT_TEMPLATE_FETCH,
  CATALOG_DOCUMENT_TEMPLATE_INTEGRATIONS_FETCH,
  CATALOG_DOCUMENT_TEMPLATE_SAVE,
} from './documentTemplate.constants';

export const initialState = {
  state: AJAX_STATE_INIT,
  savingState: AJAX_STATE_INIT,
  id: null,
  integrations: [],
};

const handleInitDocumentTemplate = (state, action) => {
  const id = get(action, 'payload.options.id', null);
  return {
    ...initialState,
    id,
  };
};

const handleFetchIntegrationsSuccess = (state, action) => {
  const payload = get(action, 'payload.response.data');
  return {
    ...state,
    integrations: payload.map(integration => ({
      value: get(integration, 'id'),
      label: get(integration, 'attributes.name'),
      module: get(integration, 'attributes.module'),
    })),
  };
};

const handleFetchSuccess = (state, action) => {
  return {
    ...state,
    values: get(action, 'payload.response.data.attributes'),
  };
};

/* eslint complexity: [2, 11] */
export function documentTemplate(state = initialState, action) {
  const handleFetchAjaxState = handleAjaxStateChange(
    CATALOG_DOCUMENT_TEMPLATE_FETCH
  );
  const handleSaveAjaxState = handleAjaxStateChange(
    CATALOG_DOCUMENT_TEMPLATE_SAVE,
    'savingState'
  );
  const handleFetchIntegrationsAjaxState = handleAjaxStateChange(
    CATALOG_DOCUMENT_TEMPLATE_INTEGRATIONS_FETCH,
    'integrationsState'
  );

  switch (action.type) {
    case CATALOG_DOCUMENT_TEMPLATE_INIT:
      return handleInitDocumentTemplate(state, action);
    case CATALOG_DOCUMENT_TEMPLATE_FETCH.SUCCESS:
      return handleFetchSuccess(handleFetchAjaxState(state, action), action);
    case CATALOG_DOCUMENT_TEMPLATE_FETCH.PENDING:
    case CATALOG_DOCUMENT_TEMPLATE_FETCH.ERROR:
      return handleFetchAjaxState(state, action);
    case CATALOG_DOCUMENT_TEMPLATE_INTEGRATIONS_FETCH.PENDING:
    case CATALOG_DOCUMENT_TEMPLATE_INTEGRATIONS_FETCH.ERROR:
      return handleFetchIntegrationsAjaxState(state, action);
    case CATALOG_DOCUMENT_TEMPLATE_INTEGRATIONS_FETCH.SUCCESS:
      return handleFetchIntegrationsSuccess(
        handleFetchIntegrationsAjaxState(state, action),
        action
      );
    case CATALOG_DOCUMENT_TEMPLATE_SAVE.PENDING:
    case CATALOG_DOCUMENT_TEMPLATE_SAVE.ERROR:
    case CATALOG_DOCUMENT_TEMPLATE_SAVE.SUCCESS:
      return handleSaveAjaxState(state, action);

    default:
      return state;
  }
}

export default documentTemplate;
