import { get } from '@mintlab/kitchen-sink/source';
import { handleAjaxStateChange } from '../../../../library/redux/ajax/handleAjaxStateChange';
import { AJAX_STATE_INIT } from '../../../../library/redux/ajax/createAjaxConstants';
import {
  CATALOG_EMAIL_TEMPLATE_INIT,
  CATALOG_EMAIL_TEMPLATE_FETCH,
  CATALOG_EMAIL_TEMPLATE_SAVE,
} from './emailTemplate.constants';

export const initialState = {
  state: AJAX_STATE_INIT,
  savingState: AJAX_STATE_INIT,
  id: null,
};

const handleInitEmailTemplate = (state, action) => ({
  ...initialState,
  id: get(action, 'payload.options.id', null),
});

/**
 * Process incoming data from backend
 *
 * @param {Object} state
 * @param {Object} action
 * @return {*}
 */
const handleFetchSuccess = (state, action) => ({
  ...state,
  values: get(action, 'payload.response.data.attributes'),
});

/* eslint complexity: [2, 8] */
export function emailTemplate(state = initialState, action) {
  const handleFetchAjaxState = handleAjaxStateChange(
    CATALOG_EMAIL_TEMPLATE_FETCH
  );
  const handleSaveAjaxState = handleAjaxStateChange(
    CATALOG_EMAIL_TEMPLATE_SAVE,
    'savingState'
  );

  switch (action.type) {
    case CATALOG_EMAIL_TEMPLATE_INIT:
      return handleInitEmailTemplate(state, action);
    case CATALOG_EMAIL_TEMPLATE_FETCH.SUCCESS:
      return handleFetchSuccess(handleFetchAjaxState(state, action), action);
    case CATALOG_EMAIL_TEMPLATE_FETCH.PENDING:
    case CATALOG_EMAIL_TEMPLATE_FETCH.ERROR:
      return handleFetchAjaxState(state, action);
    case CATALOG_EMAIL_TEMPLATE_SAVE.PENDING:
    case CATALOG_EMAIL_TEMPLATE_SAVE.ERROR:
    case CATALOG_EMAIL_TEMPLATE_SAVE.SUCCESS:
      return handleSaveAjaxState(state, action);
    default:
      return state;
  }
}

export default emailTemplate;
