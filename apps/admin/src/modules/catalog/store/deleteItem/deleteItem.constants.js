import { createAjaxConstants } from '../../../../library/redux/ajax/createAjaxConstants';

export const CATALOG_REQUEST_DELETE_ITEM = 'CATALOG:REQUEST_DELETE_ITEM';
export const CATALOG_DELETE_ITEM = createAjaxConstants('CATALOG:DELETE_ITEM');
