import { getBreadcrumbs } from './CatalogHeaderContainer';

describe('The `CatalogHeaderContainer` module', () => {
  describe('The `getBreadcrumbs` function', () => {
    test('filters `null` values', () => {
      const breadcrumbs = {
        parent: null,
        current: {
          name: 'Test',
          id: '1',
        },
      };

      expect(getBreadcrumbs(breadcrumbs)).toHaveLength(1);
    });
  });
});
