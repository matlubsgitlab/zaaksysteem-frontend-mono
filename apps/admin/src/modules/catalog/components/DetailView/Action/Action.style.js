/**
 * @return {JSS}
 */
export const actionStylesheet = () => ({
  valueActionButton: {
    padding: 4,
    marginLeft: 6,
  },
});
