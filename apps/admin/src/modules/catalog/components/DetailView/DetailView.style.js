/**
 * @param {Object} theme
 * @return {JSS}
 */
export const detailViewStyleSheet = ({
  mintlab: { greyscale },
  typography,
}) => ({
  detailView: {
    height: '100%',
    width: '460px',
    margin: '20px 0px 20px 20px',
    display: 'flex',
    flexFlow: 'column',
    alignItems: 'stretch',
    fontFamily: typography.fontFamily,
  },
  header: {
    borderTop: `1px solid ${greyscale.darker}`,
    display: 'flex',
    alignItems: 'center',
    padding: '12px 0px',
  },
  headerIcon: {
    padding: '12px',
  },
  title: {
    flexGrow: '1',
  },
  close: {
    height: '60px',
  },
  subHeader: {
    display: 'flex',
    alignItems: 'center',
  },
  version: {
    margin: '0px 12px 12px 12px',
    padding: '0px 12px',
    border: `1px solid ${greyscale.darkest}`,
    borderRadius: '13px',
    color: greyscale.darkest,
    fontSize: typography.subtitle2.fontSize,
  },
  content: {
    borderTop: `1px solid ${greyscale.darker}`,
    padding: '0px 10px',
    height: '100%',
    overflow: 'auto',
    overflowY: 'scroll',
  },
});
