import React from 'react';
import { withStyles } from '@material-ui/styles';
import { withFormatDate } from '../../../../../components/App/withFormatDate';
import { valuesStyleSheet } from './Values.style';

/**
 * @reactProps {Object} classes
 * @reactProps {Object} value
 * @reactProps {Function} formatDate
 * @return {ReactElement}
 */
const DateTime = ({ classes, value, formatDate }) => {
  const dateValue = new Date(value);

  return (
    <div>
      <span>{formatDate(dateValue, 'Do MMMM YYYY')}</span>
      <span className={classes.time}>{formatDate(dateValue, 'shortTime')}</span>
    </div>
  );
};

export default withStyles(valuesStyleSheet)(withFormatDate(DateTime));
