import React from 'react';
import { withStyles } from '@material-ui/styles';
import classNames from 'classnames';
import { optionsListStylesheet } from './OptionsList.style';

const OptionsList = ({ provided, snapshot, children, classes }) => {
  const [items] = children;

  return (
    <div
      {...provided.droppableProps}
      ref={provided.innerRef}
      className={classNames(classes.list, {
        [classes.hidden]: !items.length,
        [classes.draggingOver]: snapshot.isDraggingOver,
      })}
    >
      {children}
    </div>
  );
};

export default withStyles(optionsListStylesheet)(OptionsList);
