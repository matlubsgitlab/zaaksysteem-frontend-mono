import React from 'react';
import DataProvider from '@mintlab/ui/App/Abstract/DataProvider';
import { asArray, isPopulatedArray } from '@mintlab/kitchen-sink/source';
import { getLocationList } from '../../../../../../library/requests/Attribute';
import FlatValueSelect from '../../../../../../components/Form/FlatValueSelect';

const fetchLocations = uuid =>
  getLocationList(uuid)
    .then(locations =>
      asArray(locations).map(location => ({
        value: location.id,
        label: location.label,
      }))
    )
    .catch(() => {});

const LocationSelect = ({
  value,
  appointment_interface_uuid,
  ...restProps
}) => (
  <DataProvider
    autoProvide={true}
    provider={fetchLocations}
    providerArguments={[appointment_interface_uuid]}
  >
    {({ data, busy, provide }) => {
      const normalizedChoices = data || [];
      return (
        <FlatValueSelect
          {...restProps}
          choices={normalizedChoices}
          value={value}
          isClearable={true}
          loading={busy}
          getChoices={provide}
          isDisabled={!isPopulatedArray(normalizedChoices)}
        />
      );
    }}
  </DataProvider>
);

export default LocationSelect;
