import React, { Component } from 'react';
import deepEqual from 'fast-deep-equal';
import DataProvider from '@mintlab/ui/App/Abstract/DataProvider';
import { asArray, isPopulatedArray } from '@mintlab/kitchen-sink/source';
import { getProductList } from '../../../../../../library/requests/Attribute';
import FlatValueSelect from '../../../../../../components/Form/FlatValueSelect';

const fetchProducts = (uuid, location) => {
  if (!uuid || !location) {
    return Promise.resolve([]);
  }

  return getProductList({ uuid, location_id: location.value }).then(products =>
    asArray(products).map(product => ({
      value: product.id,
      label: product.label,
    }))
  );
};

class ProductSelect extends Component {
  constructor(props) {
    super(props);
  }

  // When the user changes or removes the refValue (Location Select),
  // set this field's value to null.
  componentDidUpdate(prevProps) {
    const { refValue, setFieldValue, name } = this.props;
    if (
      (!refValue && prevProps.refValue) ||
      (refValue &&
        prevProps.refValue &&
        !deepEqual(refValue.value, prevProps.refValue.value))
    ) {
      setFieldValue(name, null);
    }
  }

  render() {
    const {
      value,
      refValue,
      appointment_interface_uuid,
      ...restProps
    } = this.props;
    return (
      <DataProvider
        autoProvide={refValue !== null}
        provider={fetchProducts}
        key={refValue}
        providerArguments={[appointment_interface_uuid, refValue]}
      >
        {({ data, busy, provide }) => {
          const normalizedChoices = data || [];
          return (
            <FlatValueSelect
              {...restProps}
              choices={normalizedChoices}
              value={value}
              isClearable={true}
              loading={busy}
              getChoices={provide}
              isDisabled={!isPopulatedArray(normalizedChoices)}
            />
          );
        }}
      </DataProvider>
    );
  }
}

export default ProductSelect;
