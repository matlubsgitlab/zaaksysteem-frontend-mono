import React from 'react';
import { callOrNothingAtAll, get } from '@mintlab/kitchen-sink/source';
import { FormFields } from '@zaaksysteem/common/src/components/form/fields';
import { TEXT } from '@zaaksysteem/common/src/components/form/constants/fieldTypes';

const TextField = FormFields[TEXT];

const Name = props => {
  const handleBlur = event => {
    const {
      setFieldValue,
      value = '',
      requests: { generateMagicString },
      onBlur,
    } = props;

    if (!get(props, 'refValue')) {
      generateMagicString(value).then(magicstring =>
        setFieldValue('magic_string', magicstring)
      );
    }

    callOrNothingAtAll(onBlur, [event]);
  };

  return <TextField {...props} onBlur={handleBlur} />;
};

export default Name;
