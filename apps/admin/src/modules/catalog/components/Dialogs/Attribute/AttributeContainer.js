import { connect } from 'react-redux';
import { withTranslation } from 'react-i18next';
import uuidv4 from 'uuid/v4';
import { get, asArray, cloneWithout } from '@mintlab/kitchen-sink/source';
import { AJAX_STATE_PENDING } from '../../../../../library/redux/ajax/createAjaxConstants';
import { generateMagicString } from '../../../../../library/requests/Attribute';
import { saveAttribute } from '../../../store/attribute/attribute.actions';
import formDefinition from '../../../fixtures/attribute';
import Attribute from './Attribute';

const { keys } = Object;

/* eslint-disable getter-return */
const mangleValuesForSave = ({
  values,
  id,
  currentFolderUUID,
  appointment_interface_uuid,
}) => {
  const isNew = !id;
  const attribute_uuid = isNew ? uuidv4() : id;

  // Modify values where needed, before they get
  // sent to the save action, and to the API endpoint.
  /* eslint complexity: [2, 11] */
  const fields = keys(values).reduce((accumulator, key) => {
    switch (key) {
      case 'attribute_values':
        if (!values.attribute_values) return [];
        accumulator[key] = asArray(values.attribute_values).map(
          (item, index) => ({
            ...cloneWithout(item, 'id', 'isNew'),
            sort_order: index,
          })
        );
        break;
      case 'attribute_versions':
        break;
      default:
        accumulator[key] = values[key];
    }
    return accumulator;
  }, {});

  fields.category_uuid = currentFolderUUID;
  fields.appointment_interface_uuid = appointment_interface_uuid;

  return {
    fields,
    attribute_uuid,
    isNew,
  };
};

const mapStateToProps = (
  {
    catalog: {
      attribute,
      attribute: { id, values },
      items: { currentFolderUUID },
    },
  },
  { t }
) => {
  // Insert translations, requests and other values into FormDefinition
  const { appointment_interface_uuid } = attribute;
  const defaultValues = {
    commit_message: id ? t('catalog:defaultEdit') : t('catalog:defaultCreate'),
  };
  const valuesWithDefault = {
    ...defaultValues,
    ...values,
  };
  const mapFormDefinition = field => {
    const mode = id ? 'edit' : 'create';
    const getChoiceTranslation = key => choice => ({
      value: choice,
      label: t(`attribute:fields.${key}.choices.${choice}`),
    });
    /* eslint complexity: [2, 12] */
    const getValue = name => {
      const value = get(valuesWithDefault, name);
      if (!value) return field.value;

      switch (name) {
        case 'attribute_values':
          return asArray(get(values, name)).map((option, index) => ({
            ...option,
            id: index + 1,
          }));
        case 'help':
        case 'value_default':
          return get(values, name) || '';
        default:
          return value;
      }
    };

    return {
      ...field,
      label: t(field.label),
      placeholder: t(field.placeholder),
      hint: t(field.hint),
      help: t(field.help),
      loadingMessage: t(field.loadingMessage),
      value: getValue(field.name),
      disabled:
        mode === 'edit' &&
        ['attribute_type', 'magic_string'].includes(field.name),
      get requests() {
        switch (field.name) {
          case 'name':
            return {
              generateMagicString,
            };
          default:
            break;
        }
      },
      get choices() {
        switch (field.name) {
          case 'document_category':
          case 'document_trust_level':
          case 'document_origin':
            return field.choices.map(getChoiceTranslation(field.name));
          case 'attribute_type':
            return field.choices
              .filter(choice =>
                choice === 'appointment' ? appointment_interface_uuid : true
              )
              .map(getChoiceTranslation(field.name));
          default:
            break;
        }
      },
      get appointment_interface_uuid() {
        if (
          ['appointment_location_id', 'appointment_product_id'].includes(
            field.name
          )
        ) {
          return appointment_interface_uuid;
        }
      },
    };
  };

  const newProps = {
    ...attribute,
    formDefinition: formDefinition.map(mapFormDefinition),
    currentFolderUUID,
    appointment_interface_uuid,
  };

  return {
    loading: attribute.state === AJAX_STATE_PENDING,
    saving: attribute.savingState === AJAX_STATE_PENDING,
    fetchingIntegrations: attribute.integrationsState === AJAX_STATE_PENDING,
    ...newProps,
  };
};

const mapDispatchToProps = dispatch => ({
  saveAction: ({ values, id, currentFolderUUID, appointment_interface_uuid }) =>
    dispatch(
      saveAttribute(
        mangleValuesForSave({
          values,
          id,
          currentFolderUUID,
          appointment_interface_uuid,
        })
      )
    ),
});

const connectedDialog = withTranslation()(
  connect(mapStateToProps, mapDispatchToProps)(Attribute)
);

export default connectedDialog;
