import React from 'react';
import { withStyles } from '@material-ui/styles';
import Button from '@mintlab/ui/App/Material/Button';
import Icon from '@mintlab/ui/App/Material/Icon';
import Render from '@mintlab/ui/App/Abstract/Render';
import Switch from '@mintlab/ui/App/Material/Switch';
import classNames from 'classnames';
import { optionStylesheet } from './Option.style';

const getItemStyle = (snapshot, style) => {
  const transition = `all 0.03s`;

  if (snapshot.isDropAnimating) {
    return {
      ...style,
      transition,
    };
  } else if (snapshot.isDragging) {
    return {
      ...style,
      transform: `rotate(3deg) ${style.transform}`,
      transition,
    };
  }

  return {
    ...style,
  };
};

const Option = ({
  classes,
  item,
  provided,
  snapshot,
  onDelete,
  onSwitchActive,
  t,
}) => {
  return (
    <div
      ref={provided.innerRef}
      {...provided.draggableProps}
      {...provided.dragHandleProps}
      className={classNames(classes.wrapper, {
        [classes.dragging]: snapshot.isDragging,
      })}
      style={getItemStyle(snapshot, provided.draggableProps.style)}
    >
      <Icon
        classes={{
          root: classes.handle,
        }}
      >
        drag_indicator
      </Icon>
      <Switch
        checked={item.active}
        onChange={() => onSwitchActive(item)}
        variant="iOS"
      />

      <div className={classes.label}>{item.value}</div>
      <Render condition={item.isNew}>
        <Button
          action={() => onDelete(item)}
          label={t('attribute:dialog.deleteOption')}
          presets={['primary', 'medium', 'icon']}
          classes={{
            root: classes.delete,
          }}
        >
          close
        </Button>
      </Render>
    </div>
  );
};

export default withStyles(optionStylesheet)(Option);
