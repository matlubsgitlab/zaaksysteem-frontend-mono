/**
 * @param {Object} theme
 * @return {JSS}
 */
export const formStylesheet = ({ mintlab: { greyscale } }) => ({
  wrapper: {
    '&:not(:last-child)': {
      marginBottom: '20px',
    },
  },
  dialogTitle: {
    '&>*:nth-child(1)': {
      color: greyscale.darkest,
    },
  },
});
