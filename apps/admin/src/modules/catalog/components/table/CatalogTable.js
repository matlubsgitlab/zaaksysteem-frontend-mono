import React from 'react';
import classNames from 'classnames';
import { AutoSizer, Column, Table } from 'react-virtualized';
import { withStyles } from '@material-ui/styles';
import Checkbox from '@mintlab/ui/App/Material/Checkbox';
import Render from '@mintlab/ui/App/Abstract/Render';
import { get, preventDefaultAndCall } from '@mintlab/kitchen-sink/source';
import { IconCell, LinkCell } from './../../../../components/Table/cells';
import { catalogTableStyleSheet } from './CatalogTable.style';

/**
 * @reactProps {Object} classes
 * @reactProps {Array<Object>} columns
 * @reactProps {Array<Object>} rows
 * @reactProps {Function} t
 * @reactProps {Function} toggleItem
 * @reactProps {Function} onRowNavigate
 *
 * @returns {ReactElement}
 */
const CatalogTable = ({
  classes,
  columns,
  rows = [],
  t,
  toggleItem,
  onRowNavigate,
}) => {
  const catalogItemScope = 'catalog-item';
  const cellContent = {
    selected(rowData) {
      return (
        <Checkbox
          scope={catalogItemScope}
          disabled={rowData.beingMoved}
          checked={rowData.selected}
        />
      );
    },
    icon(rowData) {
      return <IconCell value={rowData.icon} />;
    },
    name({ active, beingMoved, isNavigable, name, path, type }) {
      const shouldBeLink = isNavigable && !beingMoved;

      return (
        <div data-scope={`${catalogItemScope}:name-wrapper`}>
          <Render condition={!shouldBeLink}>
            <span>{name}</span>
          </Render>
          <Render condition={shouldBeLink}>
            <LinkCell
              handleNavigate={preventDefaultAndCall(event => {
                const tagName = get(event, 'nativeEvent.target.tagName');
                if (tagName && tagName.toLowerCase() === 'a') {
                  event.stopPropagation();
                }
                onRowNavigate({ path, isNavigable });
              })}
              scope={`${catalogItemScope}:${type}`}
              path={path}
              value={name}
            />
          </Render>
          <Render condition={type === 'case_type' && !active}>
            <span className={classes.activeLabel}>
              {t('catalog:items:offline')}
            </span>
          </Render>
        </div>
      );
    },
    type(rowData) {
      return t(`catalog:type:${rowData.type}`);
    },
  };

  const rowClassName = ({ index }) =>
    classNames(classes.flexContainer, {
      [classes.tableHeader]: index === -1,
      [classes.tableRow]: index >= 0,
      [classes.tableRowHover]: index >= 0 && !rows[index].beingMoved,
      [classes.tableRowDisabled]: index >= 0 && rows[index].beingMoved,
    });

  const onRowClick = ({ event, rowData }) => {
    if (rowData.beingMoved) {
      return;
    }
    const multiSelect = event.target.type === 'checkbox';
    toggleItem(rowData.id, multiSelect);
  };

  const onRowDoubleClick = ({ rowData: { path, isNavigable } }) =>
    onRowNavigate({ path, isNavigable });

  const headerRenderer = ({ label }) => (
    <div className={classes.tableCell}>{label}</div>
  );

  const cellRenderer = ({ dataKey, rowData }) => (
    <div className={classes.tableCell}>{cellContent[dataKey](rowData)}</div>
  );

  return (
    <AutoSizer>
      {table => (
        <Table
          height={table.height}
          width={table.width}
          headerHeight={61}
          rowHeight={82}
          rowCount={rows.length}
          rowGetter={({ index }) => rows[index]}
          rowClassName={rowClassName}
          onRowClick={onRowClick}
          onRowDoubleClick={onRowDoubleClick}
        >
          {columns.map(({ label, name, width }) => (
            <Column
              key={name}
              width={width}
              flexGrow={width === 1 ? 1 : 0}
              dataKey={name}
              label={label}
              headerRenderer={headerRenderer}
              cellRenderer={cellRenderer}
            />
          ))}
        </Table>
      )}
    </AutoSizer>
  );
};

export default withStyles(catalogTableStyleSheet)(CatalogTable);
