import {
  SECTION_CATALOG,
  SECTION_USERS,
  SECTION_LOG,
  SECTION_TRANSACTIONS,
  SECTION_CONNECTORS,
  SECTION_DATA_WAREHOUSE,
  SECTION_SYSTEM_CONFIG,
} from '../../../constants/section';

/**
 * Array of navigation link configurations with the signature
 * - `{string}` capability
 * - `{string}` icon
 * - `{string}` label
 * - `{string}` path
 *
 * @type {Array<Object>}
 */
export const navigation = [
  {
    capability: 'beheer_zaaktype_admin',
    icon: 'chrome_reader_mode',
    label: 'Catalogus',
    path: '/admin/catalogus',
    section: SECTION_CATALOG,
  },
  {
    capability: 'useradmin',
    icon: 'people',
    label: 'Gebruikers',
    path: '/admin/gebruikers',
    section: SECTION_USERS,
  },
  {
    capability: 'admin',
    icon: 'import_contacts',
    label: 'Logboek',
    path: '/admin/logboek',
    section: SECTION_LOG,
  },
  {
    capability: 'admin',
    icon: 'poll',
    label: 'Transacties',
    path: '/admin/transacties',
    section: SECTION_TRANSACTIONS,
  },
  {
    capability: 'admin',
    icon: 'all_inclusive',
    label: 'Koppelingen',
    path: '/admin/koppelingen',
    section: SECTION_CONNECTORS,
  },
  {
    capability: 'admin',
    icon: 'view_comfy',
    label: 'Gegevens',
    path: '/admin/gegevens',
    section: SECTION_DATA_WAREHOUSE,
  },
  {
    capability: 'admin',
    icon: 'settings',
    label: 'Configuratie',
    path: '/admin/configuratie',
    section: SECTION_SYSTEM_CONFIG,
  },
];
