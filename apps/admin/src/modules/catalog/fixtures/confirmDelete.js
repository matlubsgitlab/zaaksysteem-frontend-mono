import { TEXT } from '@zaaksysteem/common/src/components/form/constants/fieldTypes';

export const formDefinition = [
  {
    name: 'reason',
    type: TEXT,
    value: '',
    required: true,
    label: 'catalog:delete:confirm:label',
    hint: 'catalog:delete:confirm:description',
    placeholder: 'catalog:delete:confirm:placeholder',
  },
];
