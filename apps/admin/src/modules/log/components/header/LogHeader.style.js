const filterHeight = '50px';
/**
 * @param {Object} theme
 * @return {JSS}
 */
export const logHeaderStyleSheet = ({
  mintlab: { shadows, greyscale },
  breakpoints,
}) => ({
  headerWrapper: {
    display: 'flex',
    alignItems: 'center',
    width: '100%',
    '&>*:nth-child(1)': {
      display: 'flex',
      alignItems: 'center',
      flexGrow: '1',
    },
    '&>*:not(:first-child):not(:last-child)': {
      marginRight: '10px',
    },
    '& .exportButton': {
      display: 'none',
    },
    [breakpoints.up('md')]: {
      '& .exportButton': {
        display: 'flex',
      },
      '& .moreButton': {
        display: 'none',
      },
    },
  },
  filterOverlay: {
    position: 'absolute',
    top: '11px',
    left: '0',
    height: filterHeight,
    width: '100%',
    backgroundColor: greyscale.lighter,
    borderRadius: `calc(${filterHeight} / 2)`,
    boxShadow: shadows.flat,
  },
  hideFilterOverlay: {
    display: 'none',
  },
});
