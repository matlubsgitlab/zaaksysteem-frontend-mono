import React, { createElement } from 'react';
import classNames from 'classnames';
import { withStyles } from '@material-ui/styles';
import Button from '@mintlab/ui/App/Material/Button';
import DropdownMenu from '@mintlab/ui/App/Zaaksysteem/DropdownMenu';
import SubAppHeader from '../../../../components/Header/SubAppHeader';
import Title from '../../../../components/Header/Title';
import { logHeaderStyleSheet } from './LogHeader.style';
import ToggleFilterButton from './ToggleFilterButton';
import ExportButton from './ExportButton';
import TextFieldFilter from './Filters/TextFieldFilter';
import SelectFilter from './Filters/SelectFilter';

/**
 * @param {Object} props
 * @param {string} props.caseNumberTranslation
 * @param {Object} props.classes
 * @param {Function} props.clearFilter
 * @param {string} props.exportButtonTitle
 * @param {Function} props.exportLog
 * @param {Function} props.fetchUsersList
 * @param {Object} props.filters
 * @param {string} props.filters.keyword
 * @param {string} props.filters.caseNumber
 * @param {Object} props.filters.user
 * @param {string} props.headerTitle
 * @param {string} props.keywordTranslation
 * @param {Function} props.onTextFieldKeyDown
 * @param {boolean} props.showFilters
 * @param {Function} props.toggleFilters
 * @param {Array<Object>} props.userOptions
 * @param {Object} props.userTranslations
 * @param {Object} props.values
 * @param {Function} props.handleChange
 * @param {boolean} props.userOptionsLoading
 * @return {ReactElement}
 **/
const LogHeader = ({
  caseNumberTranslation,
  classes,
  clearFilter,
  exportButtonTitle,
  exportLog,
  fetchUsersList,
  headerTitle,
  keywordTranslation,
  onTextFieldKeyDown,
  showFilters,
  toggleFilters,
  userOptions,
  userTranslations,
  values,
  handleChange,
  userOptionsLoading,
}) => {
  const triggerButton = createElement(
    Button,
    { presets: ['icon'] },
    'more_vert'
  );

  const exportButtonProps = {
    action: exportLog,
    value: exportButtonTitle,
  };

  const exportButton = <ExportButton {...exportButtonProps} />;
  const exportButtonToggle = (
    <ExportButton {...exportButtonProps} baseClassName="exportButton" />
  );

  return (
    <SubAppHeader>
      <div className={classes.headerWrapper}>
        <Title>{headerTitle}</Title>
        {exportButton}
        <ToggleFilterButton action={toggleFilters} type="search" />
      </div>

      <div
        className={classNames(classes.headerWrapper, classes.filterOverlay, {
          [classes.hideFilterOverlay]: !showFilters,
        })}
      >
        <div role="presentation">
          <TextFieldFilter
            name="keyword"
            value={values.keyword}
            onChange={handleChange}
            startAdornmentName="search"
            endAdornmentAction={clearFilter}
            placeholder={keywordTranslation}
            onKeyPress={onTextFieldKeyDown}
          />
          <TextFieldFilter
            name="caseNumber"
            value={values.caseNumber}
            onChange={handleChange}
            onKeyPress={onTextFieldKeyDown}
            startAdornmentName="folder"
            endAdornmentAction={clearFilter}
            placeholder={caseNumberTranslation}
          />

          <SelectFilter
            name="user"
            value={values.user}
            translations={userTranslations}
            onChange={handleChange}
            fetchUsersList={fetchUsersList}
            startAdornmentName="person"
            userOptions={userOptions}
            userTranslations={userTranslations}
            loading={userOptionsLoading}
          />
        </div>

        {exportButtonToggle}

        <div className="moreButton">
          <DropdownMenu trigger={triggerButton}>{exportButton}</DropdownMenu>
        </div>

        <ToggleFilterButton action={toggleFilters} type="close" />
      </div>
    </SubAppHeader>
  );
};
export default withStyles(logHeaderStyleSheet)(LogHeader);
