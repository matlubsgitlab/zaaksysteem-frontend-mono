import React from 'react';
import {
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
} from '@mintlab/ui/App/Material/Table';
import LogTableRow from './LogTableRow';

/**
 * @reactProps {Array<Object>} columns
 * @reactProps {Array<Object>} rows
 *
 * @returns {ReactElement}
 */
const LogTable = ({ columns, rows, t }) => (
  <Table>
    <TableHead>
      <TableRow heading={true}>
        {columns.map(({ name, label }) => (
          <TableCell key={name} heading={true}>
            {label}
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
    <TableBody>
      {rows &&
        rows.map((row, index) => <LogTableRow key={index} t={t} {...row} />)}
    </TableBody>
  </Table>
);

export default LogTable;
