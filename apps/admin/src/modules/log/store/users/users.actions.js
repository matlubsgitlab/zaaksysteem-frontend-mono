import { buildUrl } from '@mintlab/kitchen-sink/source';
import { createAjaxAction } from '../../../../library/redux/ajax/createAjaxAction';
import { USERS_FETCH_LIST } from './users.constants';

const listAction = createAjaxAction(USERS_FETCH_LIST);

export const fetchUsersList = query =>
  listAction({
    url: buildUrl('/objectsearch/contact/medewerker', {
      query,
      include_admin: '1',
    }),
    method: 'GET',
  });
