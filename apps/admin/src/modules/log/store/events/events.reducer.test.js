import { defaultInitialState, events } from './events.reducer';
import { eventsSetRowsPerPage } from './events.actions';
import { EVENTS_SET_ROWS_PER_PAGE } from './events.contants';

describe('Log events reducer', () => {
  describe(`When action '${EVENTS_SET_ROWS_PER_PAGE}' is dispatched`, () => {
    const getState = ({ page, rowsPerPage }) => ({
      ...defaultInitialState,
      page,
      rowsPerPage,
      count: rowsPerPage,
    });

    describe("and the 'rowsPerPage' in the payload is decreased", () => {
      test("the 'page' in the next state is increased", () => {
        const state = getState({
          page: 1,
          rowsPerPage: 50,
        });
        const action = eventsSetRowsPerPage(25);
        const result = events(state, action);
        const expected = 2;

        expect(result.page).toBe(expected);
      });
    });

    describe("and the 'rowsPerPage' in the payload is increased", () => {
      test("the 'page' in the next state is decreased", () => {
        const state = getState({
          page: 2,
          rowsPerPage: 25,
        });
        const action = eventsSetRowsPerPage(100);
        const result = events(state, action);
        const expected = 1;

        expect(result.page).toBe(expected);
      });
    });
  });
});
