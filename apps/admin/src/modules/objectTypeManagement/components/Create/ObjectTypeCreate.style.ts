import { makeStyles } from '@material-ui/core';

export const useObjectTypeCreateStyle = makeStyles(() => ({
  wrapper: {
    height: '100%',
    overflow: 'hidden',
  },
}));
