import { makeStyles } from '@material-ui/core';

export const useObjectTypeEditStyle = makeStyles(() => ({
  wrapper: {
    height: '100%',
    overflow: 'hidden',
  },
}));
