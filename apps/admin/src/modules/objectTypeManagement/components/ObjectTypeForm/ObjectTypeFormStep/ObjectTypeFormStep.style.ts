import { makeStyles } from '@material-ui/core';

export const useObjectTypeFormStepStyles = makeStyles(
  ({ mintlab: { greyscale } }: any) => ({
    stepIntro: {
      margin: '64px 0',
    },
    stepIntroSubTitle: {
      paddingTop: 16,
      maxWidth: 768,
    },
    fieldWrapper: {
      padding: '24px 0',
      borderTop: `1px solid ${greyscale.dark}`,

      '&:first-child': {
        border: 0,
        paddingTop: 0,
      },
    },
  })
);
