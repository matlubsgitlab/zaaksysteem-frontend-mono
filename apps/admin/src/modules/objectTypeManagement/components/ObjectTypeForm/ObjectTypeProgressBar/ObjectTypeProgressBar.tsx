import React from 'react';
import { FormStepType } from '@zaaksysteem/common/src/components/form/hooks/useFormSteps';
import { Stepper, StepType } from '@mintlab/ui/App/Material/Stepper/Stepper';

export type ObjectTypeProgressBarPropsType = {
  steps: FormStepType[];
};

export const ObjectTypeProgressBar: React.ComponentType<ObjectTypeProgressBarPropsType> = ({
  steps,
}) => {
  const stepperSteps: StepType[] = steps.map<StepType>(step => ({
    name: step.title,
    active: step.isActive,
    completed: step.isValid,
    disabled: !step.isTouched,
    error: step.isTouched && !step.isValid,
  }));

  return (
    <div>
      <Stepper steps={stepperSteps} />
    </div>
  );
};
