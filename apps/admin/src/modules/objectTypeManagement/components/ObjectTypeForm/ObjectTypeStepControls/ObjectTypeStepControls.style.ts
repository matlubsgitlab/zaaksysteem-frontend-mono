import { makeStyles } from '@material-ui/core';

export const useObjectTypeStepControlsStyles = makeStyles(
  ({ mintlab: { greyscale, radius } }: any) => ({
    wrapper: {
      display: 'flex',
      flexDirection: 'row',
      flexWrap: 'wrap',
      width: '100%',
      justifyContent: 'space-between',
    },

    nextStepWrapper: {
      alignSelf: 'flex-end',
    },

    previousStepWrapper: {
      alignSelf: 'flex-start',
    },
  })
);
