import React from 'react';
import { useTranslation } from 'react-i18next';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import {
  FormValuesType,
  PartialFormValuesType,
} from '@zaaksysteem/common/src/components/form/types';
import mapValuesToFormDefinition from '@zaaksysteem/common/src/components/form/library/mapValuesToFormDefinition';
import { ATTRIBUTE_SELECT } from '../../../../components/Form/Constants/fieldTypes';
import { AttributeList } from '../../../../components/Form/AttributeList/AttributeList';
import { getObjectTypeFormDefinition } from './formDefinition/ObjectTypeForm.formDefinition';
import { useObjectTypeFormStyles } from './ObjectTypeForm.style';
import ObjectTypeStepControls from './ObjectTypeStepControls/ObjectTypeStepControls';
import { ObjectTypeFormShapeType } from './ObjectTypeForm.types';
import { ObjectTypeProgressBar } from './ObjectTypeProgressBar/ObjectTypeProgressBar';
import { ObjectTypeFormStep } from './ObjectTypeFormStep/ObjectTypeFormStep';

export type ObjectTypeFormPropsType = {
  initialValues?: PartialFormValuesType<ObjectTypeFormShapeType>;
  isInitialValid?: boolean;
  busy?: boolean;
  onChange?: (values: FormValuesType<ObjectTypeFormShapeType>) => {};
  onSubmit: (values: FormValuesType<ObjectTypeFormShapeType>) => void;
};

export const ObjectTypeForm: React.ComponentType<ObjectTypeFormPropsType> = ({
  onSubmit,
  onChange,
  busy = false,
  isInitialValid = false,
  initialValues = {},
}) => {
  const [t] = useTranslation('objectTypeManagement');
  const classes = useObjectTypeFormStyles();
  const formDefinition = getObjectTypeFormDefinition({ t });
  const formDefinitionWithValues = mapValuesToFormDefinition(
    initialValues,
    formDefinition
  );
  const {
    fields,
    activeStep,
    activeStepValid,
    handleNextStep,
    handlePreviousStep,
    hasNextStep,
    hasPreviousStep,
    steps,
    formik: { submitForm },
  } = useForm<ObjectTypeFormShapeType>({
    formDefinition: formDefinitionWithValues,
    isInitialValid,
    onSubmit,
    onChange,
    fieldComponents: { [ATTRIBUTE_SELECT]: AttributeList },
  });

  return (
    <div className={classes.wrapper}>
      <div className={classes.formWrapper}>
        <div className={classes.innerWrapper}>
          <div>
            <ObjectTypeProgressBar steps={steps} />
          </div>

          <ObjectTypeFormStep step={activeStep} fields={fields} />
        </div>
      </div>

      <div className={classes.buttonWrapper}>
        <div className={classes.innerWrapper}>
          <ObjectTypeStepControls
            activeStepValid={activeStepValid}
            handleNextStep={handleNextStep}
            handlePreviousStep={handlePreviousStep}
            hasNextStep={hasNextStep}
            hasPreviousStep={hasPreviousStep}
            submitForm={submitForm}
            busy={busy}
          />
        </div>
      </div>
    </div>
  );
};

export default ObjectTypeForm;
