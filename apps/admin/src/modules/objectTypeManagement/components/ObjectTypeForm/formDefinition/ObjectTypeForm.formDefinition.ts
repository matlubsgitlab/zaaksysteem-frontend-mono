import i18next from 'i18next';
import { FormDefinition } from '@zaaksysteem/common/src/components/form/types';
import { ObjectTypeFormShapeType } from '../ObjectTypeForm.types';
import { getStep1 } from './steps/getStep1';
import { getStep2 } from './steps/getStep2';
import { getStep5 } from './steps/getStep5';

export const getObjectTypeFormDefinition = ({
  t,
}: {
  t: i18next.TFunction;
}): FormDefinition<ObjectTypeFormShapeType> => [
  getStep1(t),
  getStep2(t),
  getStep5(t),
];
