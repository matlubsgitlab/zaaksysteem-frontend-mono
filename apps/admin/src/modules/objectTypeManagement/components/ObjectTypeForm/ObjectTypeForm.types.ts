export type ObjectTypeFormShapeType = {
  name: string;
  title: string;
  external_reference: string;
  components_changed: string[];
  changes: string;
  custom_fields: { label: string; attribute_uuid: string }[];
};
