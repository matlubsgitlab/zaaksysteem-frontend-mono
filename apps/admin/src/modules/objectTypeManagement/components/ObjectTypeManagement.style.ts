import { makeStyles } from '@material-ui/core';

export const useObjectTypeStyle = makeStyles(() => ({
  wrapper: {
    height: '100%',
    overflow: 'hidden',
  },
  header: {},
}));
