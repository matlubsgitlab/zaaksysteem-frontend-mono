export default {
  nl: {
    title: 'Objecttypebeheer',
    objecttype: 'Objecttype',
    add: {
      title: 'Voeg toe',
    },
    edit: {},
    form: {
      controls: {
        next: 'Volgende',
        previous: 'Terug',
        submit: 'Opslaan',
      },
      steps: {
        1: {
          title: 'Algemeen',
          description:
            'Voordat je naar de volgende stap kunt gaan, moeten alle verplichte velden die aangegeven zijn met een asterix ingevuld zijn.',
        },
        2: {
          title: 'Kenmerken',
        },
        5: {
          title: 'Afronden',
        },
      },
      fields: {
        name: {
          label: 'Objecttype',
          help: `Geef hier een naam voor het objecttype zoals dit binnen de oplossing wordt getoond. Deze naam wordt o.a. getoond in de catalogus en voor objecten van dit objecttype (objectbeeld). Bijvoorbeeld 'Contract'`,
          placeholder: 'Geef een herkenbare naam voor het objecttype',
        },
        title: {
          label: 'Objecttitel',
          placeholder: 'Geef een titel voor de objecten van dit objecttype',
          help:
            'Hier kan een titel worden samengesteld voor de objecten van dit objecttype. Een titel kan bestaan uit reguliere teksten, maar ook met magic strings die zich in het object bevinden. Bijvoorbeeld [[ztc_contract_soort]] | [[ztc_contract_nummer]].',
        },
        external_reference: {
          label: 'Externe referentie',
          placeholder: 'Referentie voor bron buiten Zaaksysteem.nl',
          help:
            'Hier kan een locatie worden samengesteld voor een object wanneer de bron zich buiten het zaaksysteem bevindt. Bijvoorbeeld https://zaaksysteem.nl/contract/[[ztc_contract_nummer]].',
        },
        components_changed: {
          label: 'Componenten gewijzigd',
          choices: {
            generic: 'Algemeen',
            attributes: 'Kenmerken',
            relations: 'Relaties',
            rights: 'Rechten',
          },
        },
        changes: {
          label: 'Wijziging',
          help: 'Geef omschrijving op',
          placeholder: 'Geef omschrijving op',
        },
        custom_fields: {
          placeholder: 'Zoek en voeg toe',
          titlePlaceholder: 'Titel',
        },
      },
    },
  },
};
