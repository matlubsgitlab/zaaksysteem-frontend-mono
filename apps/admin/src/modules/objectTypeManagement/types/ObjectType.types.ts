import { APICaseManagement } from '@zaaksysteem/generated';

type ObjectTypeAttributes = APICaseManagement.ACustomObject['attributes'];

export interface ObjectTypeType {
  uuid: ObjectTypeAttributes['uuid'];
  title: ObjectTypeAttributes['title'];
  name: ObjectTypeAttributes['name'];
  status: ObjectTypeAttributes['status'];
}
