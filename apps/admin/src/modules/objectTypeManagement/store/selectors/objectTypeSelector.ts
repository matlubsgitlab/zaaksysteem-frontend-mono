import { ObjectTypeManagementRootStateType } from '../objectTypeManagement.reducer';
import { ObjectTypeStateType } from '../objectType/objectType.reducer';

export const objectTypeSelector = (
  state: ObjectTypeManagementRootStateType
): ObjectTypeStateType => state.objectTypeManagement.objectType;
