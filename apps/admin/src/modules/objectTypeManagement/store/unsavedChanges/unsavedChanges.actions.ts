import { ActionWithPayload } from '@zaaksysteem/common/src/types/ActionWithPayload';
import { SET_OBJECT_TYPE_UNSAVED_CHANGES } from './unsavedChanges.constants';

export type SetObjectTypeUnsavedChangesActionPayloadType = boolean;

export const setObjectTypeUnsavedChangesAction = (
  value: boolean
): ActionWithPayload<SetObjectTypeUnsavedChangesActionPayloadType, string> => ({
  type: SET_OBJECT_TYPE_UNSAVED_CHANGES,
  payload: value,
});
