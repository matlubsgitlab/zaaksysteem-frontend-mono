import { Reducer } from 'redux';
import {
  AJAX_STATE_INIT,
  AjaxState,
} from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';
import { handleAjaxStateChange } from '@zaaksysteem/common/src/library/redux/ajax/handleAjaxStateChange';
import { AjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import { APICaseManagement } from '@zaaksysteem/generated';
import { ObjectTypeType } from '../../types/ObjectType.types';
import { OBJECT_TYPE_FETCH } from './objectType.constants';

export interface ObjectTypeStateType {
  state: AjaxState;
  objectType?: ObjectTypeType;
}

const initialState: ObjectTypeStateType = {
  state: AJAX_STATE_INIT,
};

const handleFetchSuccess = (
  state: ObjectTypeStateType,
  action: AjaxAction<APICaseManagement.GetCustomObjectTypeResponseBody>
): ObjectTypeStateType => {
  const { response } = action.payload;

  if (!response || !response.data) {
    return state;
  }

  const objectType = response.data;
  const {
    id,
    attributes: { title, status, name },
  } = objectType;

  return {
    ...state,
    objectType: {
      uuid: id,
      title,
      status,
      name,
    },
  };
};

export const objectType: Reducer<ObjectTypeStateType, AjaxAction> = (
  state = initialState,
  action
) => {
  const { type } = action;
  const handleFetchObjectTypeStateChange = handleAjaxStateChange(
    OBJECT_TYPE_FETCH
  );

  switch (type) {
    case OBJECT_TYPE_FETCH.PENDING:
    case OBJECT_TYPE_FETCH.ERROR:
      return handleFetchObjectTypeStateChange(state, action);
    case OBJECT_TYPE_FETCH.SUCCESS:
      return handleFetchObjectTypeStateChange(
        handleFetchSuccess(
          state,
          action as AjaxAction<
            APICaseManagement.GetCustomObjectTypeResponseBody
          >
        ),
        action
      );
    default:
      return state;
  }
};

export default objectType;
