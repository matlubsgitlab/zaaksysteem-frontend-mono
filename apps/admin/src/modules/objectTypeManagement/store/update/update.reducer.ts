import { Reducer } from 'redux';
import {
  AJAX_STATE_INIT,
  AjaxState,
} from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';
import { handleAjaxStateChange } from '@zaaksysteem/common/src/library/redux/ajax/handleAjaxStateChange';
import { AjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import { OBJECT_TYPE_UPDATE } from './update.constants';

export interface UpdateStateType {
  state: AjaxState;
}

const initialState: UpdateStateType = {
  state: AJAX_STATE_INIT,
};

const ajaxStateHandler = handleAjaxStateChange(OBJECT_TYPE_UPDATE);

export const update: Reducer<UpdateStateType, AjaxAction> = (
  state = initialState,
  action
) => {
  return ajaxStateHandler(state, action);
};

export default update;
