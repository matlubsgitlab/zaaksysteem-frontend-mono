import createUIReducer from '@zaaksysteem/common/src/store/ui/ui.reducer';
import { iframe } from './iframe/iframe.reducer';
import { banners } from './banners/banners.reducer';
import { drawer } from './drawer/drawer.reducer';

export const ui = createUIReducer({
  iframe,
  banners,
  drawer,
});
