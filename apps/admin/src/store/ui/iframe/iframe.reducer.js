import {
  UI_OVERLAY_OPEN,
  UI_OVERLAY_CLOSE,
  UI_WINDOW_LOAD,
  UI_WINDOW_UNLOAD,
} from '../ui.constants';

const initialState = {
  overlay: false,
  loading: true,
};

/* eslint-disable complexity */
/**
 * @param {Object} state
 * @param {Object} action
 * @return {Object}
 */
export function iframe(state = initialState, action) {
  const { type } = action;

  switch (type) {
    case UI_OVERLAY_OPEN:
      return {
        ...state,
        overlay: true,
      };
    case UI_OVERLAY_CLOSE:
      return {
        ...state,
        overlay: false,
      };
    case UI_WINDOW_LOAD:
      return {
        ...state,
        loading: false,
      };
    case UI_WINDOW_UNLOAD:
      return {
        ...state,
        loading: true,
      };
    default:
      return state;
  }
}
