import { app } from './app.reducer';
import { bootstrapMiddleware } from './bootstrap.middleware';
import { errorMiddleware } from './error.middleware';

export const getAppModule = () => ({
  id: 'router',
  reducerMap: {
    app: app as any,
  },
  middlewares: [bootstrapMiddleware as any, errorMiddleware as any],
});

export default getAppModule;
