import { ROUTE_INVOKE, ROUTE_RESOLVE } from './route.constants';

export const invoke = payload => ({
  type: ROUTE_INVOKE,
  payload,
});

export const resolve = payload => ({
  type: ROUTE_RESOLVE,
  payload,
});
