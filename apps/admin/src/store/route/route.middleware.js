import { pushState, replaceState } from '../../library/dom/history';
import { getSectionFromPath } from '../../modules/catalog/library/section';
import { sectionChange } from '../app/app.actions';
import { resolve } from './route.actions';
import { ROUTE_INVOKE, ROUTE_RESOLVE } from './route.constants';

const handleInvoke = (store, next, action) => {
  const cancelledAction = next(action);
  const {
    cancel,
    payload: { force },
  } = cancelledAction;

  const { replace, path } = action.payload;
  const updateHistory = replace ? replaceState : pushState;

  if (force || !cancel) {
    updateHistory(path);
    store.dispatch(resolve(action.payload));
  }

  return cancelledAction;
};

const handleRouteResolve = (store, next, action) => {
  const currentSection = getSectionFromPath(store.getState().route);
  const nextSection = getSectionFromPath(action.payload.path);

  next(action);

  if (currentSection !== nextSection) {
    store.dispatch(sectionChange(nextSection));
  }
};

export const routeMiddleware = store => next => action => {
  switch (action.type) {
    case ROUTE_INVOKE:
      return handleInvoke(store, next, action);
    case ROUTE_RESOLVE:
      return handleRouteResolve(store, next, action);
    default:
      return next(action);
  }
};

export default routeMiddleware;
