import { uri } from './uri';

describe('The `uri` function', () => {
  test('accepts an empty string', () => {
    expect(uri('')).toBe(true);
  });

  test('accepts just the "file"-protocol', () => {
    expect(uri('file://')).toBe(true);
  });

  test('does not accept just the protocol when it\'s not the "file"-protocol', () => {
    expect(uri('test://')).toBe(false);
  });

  test('accepts a url without a protocol', () => {
    expect(uri('www.zaaksysteem.nl')).toBe(true);
  });

  test('accepts a url without a protocol or subdomain', () => {
    expect(uri('zaaksysteem.nl')).toBe(true);
  });

  test('accepts a url with a protocol and without a subdomain', () => {
    expect(uri('https://zaaksysteem.nl')).toBe(true);
  });

  test('accepts a url with a protocol and with a subdomain', () => {
    expect(uri('https://www.zaaksysteem.nl')).toBe(true);
  });

  test('accepts a url with digits, dots and dashes in the protocol', () => {
    expect(uri('htt-3.14s://www.zaaksysteem.nl')).toBe(true);
  });

  test('does not accept a url whose protocol does not start with a letter', () => {
    [
      '-https://zaaksysteem',
      '3https://zaaksysteem',
      '.https://zaaksysteem',
    ].forEach(value => {
      expect(uri(value)).toBe(false);
    });
  });

  test('accepts a url to just a top-level domain', () => {
    ['zaaksysteem', 'https://zaaksysteem'].forEach(value => {
      expect(uri(value)).toBe(true);
    });
  });

  test('accepts an internal url', () => {
    expect(uri('c:\\windows\\is\\awesome')).toBe(true);
  });

  test('accepts an internal url where the drive is capitalised', () => {
    expect(uri('C:\\windows\\is\\awesome')).toBe(true);
  });

  test('accepts an internal url to just a drive', () => {
    expect(uri('c:\\')).toBe(true);
  });

  test("does not accept an interal url when it's not prefixed by a drive reference", () => {
    ['1:\\', 'cd:\\', 'windows\\is\\awesome'].forEach(value => {
      expect(uri(value)).toBe(false);
    });
  });

  test('does not accept a url where the domain ends with a dot', () => {
    ['zaaksysteem.', 'zaaksysteem.nl.'].forEach(value => {
      expect(uri(value)).toBe(false);
    });
  });

  test('accept a url where the path ends with a dot', () => {
    expect(uri('zaaksysteem.nl/team.')).toBe(true);
  });

  test('accept a url where the domain ends with a slash', () => {
    expect(uri('zaaksysteem.nl/')).toBe(true);
  });

  test('accept a url where the path ends with a slash', () => {
    expect(uri('zaaksysteem.nl/team/')).toBe(true);
  });

  test('accept a url with any of its letters capitalised', () => {
    expect(uri('hTtPs://wWw.ZaAkSyStEeM.nL/TeAm/')).toBe(true);
  });

  test('accept a url with digits, underscores and dashes in the domain', () => {
    expect(uri('zaak-6_teem.nl/team.')).toBe(true);
  });

  test('accept a url with digits, underscores and dashes in the path', () => {
    expect(uri('zaaksysteem.nl/te-6_am.')).toBe(true);
  });

  test('does not accept a url with special characters in the domain', () => {
    ['zaak%teem.nl/team', 'zaak?teem.nl/team', 'zaak$teem.nl/team'].forEach(
      value => {
        expect(uri(value)).toBe(false);
      }
    );
  });

  test('accepts a url with special characters in the path', () => {
    expect(uri('zaaksysteem.nl/te%?$am.')).toBe(true);
  });

  // Allowed protocols
  test('accepts valid configurations', () => {
    expect(
      uri('HTTPS://www.zaaksysteem.nl', {
        allowed_protocols: ['https', 'http'],
      })
    ).toBe(true);

    expect(
      uri('http://www.zaaksysteem.nl', {
        allowed_protocols: ['https', 'http'],
      })
    ).toBe(true);

    expect(
      uri('http://www.zaaksysteem.nl', {
        allowed_protocols: ['http'],
      })
    ).toBe(true);

    expect(
      uri('https://www.zaaksysteem.nl', {
        allowed_protocols: ['https'],
      })
    ).toBe(true);
  });

  test('rejects protocols that are not allowed', () => {
    expect(
      uri('https://www.zaaksysteem.nl', {
        allowed_protocols: ['http'],
      })
    ).toBe(false);

    expect(
      uri('http://www.zaaksysteem.nl', {
        allowed_protocols: ['https'],
      })
    ).toBe(false);

    expect(
      uri('ftp://www.zaaksysteem.nl', {
        allowed_protocols: ['http'],
      })
    ).toBe(false);
  });

  test('allows only http when no protocol is given', () => {
    expect(
      uri('www.zaaksysteem.nl', {
        allowed_protocols: ['http'],
      })
    ).toBe(true);

    expect(
      uri('www.zaaksysteem.nl', {
        allowed_protocols: ['https'],
      })
    ).toBe(false);
  });
});
