const createReactAppOverrides = require('@zaaksysteem/common/src/createReactAppOverrides');
const packageJson = require('./package.json');

module.exports = createReactAppOverrides(packageJson.homepage);
