# Installation Docker

## 🏃 Get it up and running in a container

> Don't want to install `Node`? Try the `Docker` way instead.

## Docker usage

This image is designed to be used with an nginx proxy from the main Zaaksysteem.nl repository. It
is also possible to run the image without a proxy.

### Zaaksysteem docker-compose development override

Cf. `../zaaksysteem/docker-compose.override.yml`

```yaml
services:
  frontend-mono:
    build:
      context: '../zaaksysteem-frontend-mono'
      target: 'development'
    environment:
      - DOCKER=true
    volumes:
      - ../zaaksysteem-frontend-mono:/opt/zaaksysteem-frontend-mono
      - /opt/zaaksysteem-frontend-mono/node_modules
    depends_on:
      - frontend
    command: nginx -g 'daemon off;'
```

### Build

```bash
$ cd /path/to/zaaksysteem
$ docker-compose build frontend-mono

```

### Starting

```bash
$ cd /path/to/zaaksysteem
$ docker-compose up -d frontend-mono

```

Because we want to allow starting a specific app instead of all the apps, we don't automatically start
the development process. You will have to start this manually.

```bash
$ docker-compose exec frontend-mono bash
(frontend-mono)$ yarn lerna:bootstrap
(frontend-mono)$ yarn lerna:start

```
