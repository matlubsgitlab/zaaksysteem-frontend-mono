# Modular App structure

## 🚀 Performant and clean

> Why bundle and load what you will not visit?

The concept behind this modular structure is that it is repeatable and identifiable.
Having the same structure over and over again, you'll always know where to look.

## What defines a module?

Add documentation

## Routing

A module can have it's own routes. **A module should only care about a part of the path, never the entire path!**

## Module structure

A module is structured in the following way:

```tree
module-root
  └── components
  └── locale
  └── modules
  └── store
  index.tsx
```

### components

Components and compositions specific to this module

### locale

Translations specific to this module, common translations should be added to `@zaaksysteem/common` package.

### modules

Since this is a repeatable pattern, modules can be added to modules.

### store

The `redux` store, containing:

- reducers
- actions
- constants
- module entrypoint

See the Redux modules documentation.

### index.tsx

The module entrypoint, this is where all the parts of the module come together.

```javascript
import React from 'react';
import { DynamicModuleLoader } from 'redux-dynamic-modules-react';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import { getMyModule } from './store/my.module';
import locale from './locale/my.locale';

export const MyModule: React.FunctionComponent<{}> = () => (
  <DynamicModuleLoader modules={[getMyModule()]}>
    <I18nResourceBundle resource={locale} namespace="my">
      <h1>Hello world</h1>
    </I18nResourceBundle>
  </DynamicModuleLoader>
);

export default MyModule;
```

## Submodules

This pattern is meant to be repeatable. Submodules are therefore allowed.
