# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [10.24.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.24.1...@mintlab/ui@10.24.2) (2020-04-14)

**Note:** Version bump only for package @mintlab/ui





## [10.24.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.24.0...@mintlab/ui@10.24.1) (2020-04-10)

**Note:** Version bump only for package @mintlab/ui





# [10.24.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.23.3...@mintlab/ui@10.24.0) (2020-04-09)


### Features

* **BreadcrumbBar:** MINTY-3628 Implement header containing breadcrumb component which can be used in modules ([4a85e72](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4a85e72))





## [10.23.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.23.2...@mintlab/ui@10.23.3) (2020-04-06)

**Note:** Version bump only for package @mintlab/ui





## [10.23.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.23.1...@mintlab/ui@10.23.2) (2020-04-03)


### Bug Fixes

* **PipDocuments:** MINTY-3619 - fix PipDocuments not showing all rows ([67cf987](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/67cf987))





## [10.23.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.23.0...@mintlab/ui@10.23.1) (2020-03-30)

**Note:** Version bump only for package @mintlab/ui





# [10.23.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.22.0...@mintlab/ui@10.23.0) (2020-03-30)


### Features

* **Stepper:** MINTY-3485 Add Stepper UI component ([d0f77b6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/d0f77b6))





# [10.22.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.21.1...@mintlab/ui@10.22.0) (2020-03-19)


### Features

* **DocumentExplorer:** MINTY-3300 - add initial PDF preview features, misc. ([a131814](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a131814))
* **DocumentExplorer:** MINTY-3300 - change Loader, exports ([b25d770](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/b25d770))
* **DocumentExplorer:** MINTY-3300 - split up DocumentExplorer functionality, create new Intake entrypoint, add Selectable state to table ([5bae0d3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5bae0d3))





## [10.21.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.21.0...@mintlab/ui@10.21.1) (2020-03-17)

**Note:** Version bump only for package @mintlab/ui





# [10.21.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.20.3...@mintlab/ui@10.21.0) (2020-03-05)


### Bug Fixes

* **CaseDocuments:** apply MR comments ([778fd18](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/778fd18))


### Features

* **CaseDocuments:** add misc features ([8845760](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8845760))
* **CaseDocuments:** MINTY-2884: add file icons to FileBrowser based on mime type ([4223ab5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4223ab5))
* **CaseDocuments:** MINTY-2995 - enable creating of documents from file uploads ([2792d26](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/2792d26))
* **CaseDocuments:** MINTY-3043, MINTY-3045: get documents from server, implement breadcrumb, error handling ([9519313](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/9519313))
* **CaseDocuments:** MINTY-3191 - add misc. UX tweaks, code layout ([11658c1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/11658c1))
* **CaseDocuments:** MINTY-3191 - implement misc. UX/Design changes ([a2c44c3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a2c44c3))
* **DocumentPreview:** MINTY-2960 Add DocumentPreview component ([ef352ac](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ef352ac))
* **Documents:** MINTY-2861 - add initial CaseDocuments ([237e411](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/237e411))
* **SortableTable:** MINTY-3266 - add storybook story for Sortable Table ([42be482](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/42be482))





## [10.20.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.20.2...@mintlab/ui@10.20.3) (2020-03-03)

**Note:** Version bump only for package @mintlab/ui





## [10.20.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.20.1...@mintlab/ui@10.20.2) (2020-02-24)

**Note:** Version bump only for package @mintlab/ui





## [10.20.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.20.0...@mintlab/ui@10.20.1) (2020-02-21)

**Note:** Version bump only for package @mintlab/ui





# [10.20.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.19.4...@mintlab/ui@10.20.0) (2020-02-17)


### Features

* **MultiValueText:** MINTY-3076 Add entered value on blur ([5c098f8](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5c098f8))





## [10.19.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.19.3...@mintlab/ui@10.19.4) (2020-02-10)

**Note:** Version bump only for package @mintlab/ui





## [10.19.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.19.2...@mintlab/ui@10.19.3) (2020-02-07)

**Note:** Version bump only for package @mintlab/ui

## [10.19.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.19.1...@mintlab/ui@10.19.2) (2020-02-06)

**Note:** Version bump only for package @mintlab/ui

## [10.19.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.19.0...@mintlab/ui@10.19.1) (2020-02-06)

**Note:** Version bump only for package @mintlab/ui

# [10.19.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.18.0...@mintlab/ui@10.19.0) (2020-01-10)

### Features

- **Communication:** MINTY-2692 Add role selection to email recipient form ([2122179](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/2122179))
- **Communication:** MINTY-2693 Add EmailRecipient selection component ([c67d8e9](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c67d8e9))

# [10.18.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.17.0...@mintlab/ui@10.18.0) (2020-01-09)

### Bug Fixes

- **Tasks:** fix npm error, date-fns ([6f2a1a3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6f2a1a3))
- **UI:** fix component imports ([797f9a9](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/797f9a9))

### Features

- **CaseManagement:** add task creator ([939fbbb](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/939fbbb))
- **DatePicker:** MINTY-2601 - add onClose ability to DatePicker ([732401e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/732401e))
- **Tasks:** add misc. style/UX improvements ([b619af4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/b619af4))
- **Tasks:** MINTY-1575 - add basic form fields, DatePicker ([07ed5e5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/07ed5e5))
- **Tasks:** MINTY-1575 - add edit, delete, unlock functionality ([8dca70b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8dca70b))
- **Tasks:** MINTY-1575 - fix build/import problems ([23c0700](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/23c0700))
- **Tasks:** MINTY-2695 - rewrite internal logic to immediate responding to user actions ([c0f74c2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c0f74c2))

# [10.17.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.16.3...@mintlab/ui@10.17.0) (2020-01-09)

### Bug Fixes

- **Communication:** MINTY-2262 - allow e-mail addresses over multiple lines ([db44093](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/db44093))
- **UI:** Fix aria-labels on snackbar and dialogs ([775a287](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/775a287))

### Features

- **CaseAttributes:** add Numeric field ([61d9b6e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/61d9b6e))
- **Communication:** Allow reply form to scale up with content ([0d34d84](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/0d34d84))
- **Communication:** MINTY-2664 Implement icon which indicates mulitple messages in the thread ([4a50cdb](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4a50cdb))
- **Forms:** MINTY-2323 - add generic FormDialog component and implement throughout ([7f46465](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7f46465))

## [10.16.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.16.2...@mintlab/ui@10.16.3) (2019-12-19)

**Note:** Version bump only for package @mintlab/ui

## [10.16.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.16.1...@mintlab/ui@10.16.2) (2019-11-21)

**Note:** Version bump only for package @mintlab/ui

## [10.16.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.16.0...@mintlab/ui@10.16.1) (2019-10-25)

**Note:** Version bump only for package @mintlab/ui

# [10.16.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.15.2...@mintlab/ui@10.16.0) (2019-10-17)

### Features

- **Catalog:** MINTY-1551, MINTY-1548: add clickable foldername and url to internal registrationform to details, refactor large parts of the view code ([031bfff](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/031bfff))
- **Communication:** Add actionMenus to threadList, thread and message ([665d4b7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/665d4b7))
- **Theme:** adjust some theming for better contrast ([33d10c6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/33d10c6))
- **Thread:** Add attachments view with empty click ([2b613ed](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/2b613ed))

## [10.15.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.15.1...@mintlab/ui@10.15.2) (2019-09-30)

**Note:** Version bump only for package @mintlab/ui

## [10.15.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.15.0...@mintlab/ui@10.15.1) (2019-09-26)

### Bug Fixes

- **FileUpload:** Handle filename overflow in file uploader ([90feeb5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/90feeb5))

# [10.15.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.14.0...@mintlab/ui@10.15.0) (2019-09-09)

### Bug Fixes

- **Typescript:** fix misc. errors ([a5eb790](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a5eb790))

### Features

- **TypeScript:** Add tsconfig.json, which is needed to enable typescript ([da89a6b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/da89a6b))

# [10.14.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.13.0...@mintlab/ui@10.14.0) (2019-09-06)

### Features

- **Communication:** MINTY-1294 Implement `CaseFinder` component in add `ContactMoment` form ([27af541](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/27af541))

# [10.13.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.12.0...@mintlab/ui@10.13.0) (2019-08-29)

### Bug Fixes

- **Communication:** Make icons with background white ([72610f1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/72610f1))

### Features

- **Communication:** add person/company icons to Contactfinder, some styling ([a2f2da2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a2f2da2))
- **Communication:** Add thread views for contactmoment and note ([e60867e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/e60867e))
- **Communication:** MINTY-1115 Add `SearchField` to `Theads` ([5ec462a](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5ec462a))
- **Communication:** MINTY-1115 Add `Threads` filter component ([409738f](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/409738f))
- **Communication:** MINTY-1115 Add direction icon to indicate incoming or outgoing messages ([6b8e4fa](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6b8e4fa))
- **Communication:** MINTY-1399 - add (temporary) colors for entitytypes ([4a0d023](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4a0d023))
- **Communication:** Process MR feedback ([8b17f80](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8b17f80))
- **Communication:** Tweak some styling ([bb1158d](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/bb1158d))
- **Layout:** Let menu buttons be anchors to allow opening in new tab ([67f6910](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/67f6910))
- **UI:** Add IconRounded and ZsIcon ([c042b11](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c042b11))
- **UI:** Upgrade to MUI4 ([ac921f6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ac921f6))

# [10.12.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.11.0...@mintlab/ui@10.12.0) (2019-08-27)

### Features

- **UI:** MINTY-1472 - add stories for viewing / generating elements of the theme ([0c9db5f](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/0c9db5f))

# [10.11.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.10.0...@mintlab/ui@10.11.0) (2019-08-12)

### Features

- **ProximaNova:** MINTY-1306 Change font family to Proxima Nova ([2e3009f](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/2e3009f))

# [10.10.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.9.1...@mintlab/ui@10.10.0) (2019-08-06)

### Features

- **Admin:** MINTY-1281 Move admin app to mono repo ([f6a1d35](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/f6a1d35))

## [10.9.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.9.0...@mintlab/ui@10.9.1) (2019-07-31)

**Note:** Version bump only for package @mintlab/ui

# [10.9.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.8.3...@mintlab/ui@10.9.0) (2019-07-31)

### Features

- **Communication:** add general Communication setup and Thread list ([7cb92e6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7cb92e6))

## [10.8.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.8.2...@mintlab/ui@10.8.3) (2019-07-25)

**Note:** Version bump only for package @mintlab/ui

## 10.8.2 (2019-07-23)

**Note:** Version bump only for package @mintlab/ui
