module.exports = {
  presets: [
    '@babel/preset-env',
    '@babel/preset-typescript',
    '@babel/preset-react',
  ],
  plugins: [
    '@babel/plugin-syntax-object-rest-spread',
    'babel-plugin-syntax-dynamic-import',
    'transform-class-properties',
    '@babel/plugin-proposal-optional-chaining',
  ],
  env: {
    test: {
      plugins: ['dynamic-import-node'],
    },
  },
};
