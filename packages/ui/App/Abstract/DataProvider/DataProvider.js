import { useState, useEffect, useRef, memo } from 'react';
import deepEquals from 'fast-deep-equal';

const getDataFromProvider = (state, setState, provider, providerArguments) => {
  const argumentsArray = Array.isArray(providerArguments)
    ? providerArguments
    : [providerArguments];

  setState({
    ...state,
    busy: true,
  });

  provider(...argumentsArray)
    .then(providerData =>
      setState({
        data: providerData,
        error: null,
        busy: false,
      })
    )
    .catch(error => {
      setState({
        ...state,
        error: error.toString(),
        busy: false,
      });
    });
};

/* eslint complexity: [2, 5] */
/**
 * @param {Object} props
 * @param {Function} props.children
 * @param {boolean} [props.autoProvide=true]
 * @param {Function} props.provider
 * @param {any} [props.providerArguments]
 * @returns {ReactElement}
 */
export const DataProvider = memo(
  ({ children, autoProvide = true, provider, providerArguments = [] }) => {
    const [state, setState] = useState({
      busy: false,
      data: null,
      error: null,
    });
    const { busy, data, error } = state;
    const previousProviderArguments = useRef(providerArguments);

    const getData = (...args) => {
      if (data === null && !busy) {
        getDataFromProvider(state, setState, provider, [
          ...providerArguments,
          ...args,
        ]);
      }
    };

    const getDataDelayed = (...args) => {
      return setTimeout(() => getData(...args), 100);
    };

    useEffect(() => {
      if (!deepEquals(previousProviderArguments.current, providerArguments)) {
        setState({
          data: null,
          busy: false,
          error: null,
        });
      }

      previousProviderArguments.current = providerArguments;
    }, [providerArguments]);

    useEffect(() => {
      let timeoutId;
      if (autoProvide && typeof provider === 'function' && data === null) {
        timeoutId = getDataDelayed();
      }

      return () => clearTimeout(timeoutId);
    }, [autoProvide, provider, data]);

    return children({
      data,
      busy,
      error,
      provide: getData,
    });
  }
);

export default DataProvider;
