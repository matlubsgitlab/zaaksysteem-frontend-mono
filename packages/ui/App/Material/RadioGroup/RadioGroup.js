import React, { Component } from 'react';
import MuiRadioGroup from '@material-ui/core/RadioGroup';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import { bind } from '@mintlab/kitchen-sink/source';
import { addScopeAttribute, addScopeProp } from '../../library/addScope';
import { Choice } from './library/Choice';

/**
 * *Material Design* **Radio button** selection control.
 * - facade for *Material-UI* `RadioGroup`, `Radio,`
 *   `FormLabel`, `FormControl`, `FormControlLabel`
 *   and `FormHelperText`
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Material/RadioGroup
 * @see /npm-mintlab-ui/documentation/consumer/manual/RadioGroup.html
 *
 * @reactProps {Array} choices
 * @reactProps {boolean} [disabled=false]
 * @reactProps {string} [error='']
 * @reactProps {string} [scope]
 * @reactProps {string} label
 * @reactProps {string} value
 */
export class RadioGroup extends Component {
  /**
   * @ignore
   */
  static get defaultProps() {
    return {
      disabled: false,
      error: '',
    };
  }

  /**
   * @param {Object} props
   */
  constructor(props) {
    super(props);
    this.state = {
      // ZS-FIXME: prefix with 'default' in all components
      value: props.value,
    };
    bind(this, 'handleChange');
  }

  /**
   * @return {ReactElement}
   */
  render() {
    const {
      props: { choices, error, label, name, required, scope },
      state: { value },
    } = this;

    return (
      <FormControl
        component="fieldset"
        error={Boolean(error)}
        required={required}
      >
        <FormLabel component="legend">{label}</FormLabel>
        <MuiRadioGroup
          name={name}
          value={value}
          onChange={this.handleChange}
          {...addScopeAttribute(scope, 'radio-group')}
        >
          {choices.map((choice, index) => (
            <Choice
              key={index}
              {...choice}
              {...addScopeProp(scope, 'radio-group')}
            />
          ))}
        </MuiRadioGroup>
        <FormHelperText>{error}</FormHelperText>
      </FormControl>
    );
  }

  handleChange({ target: { value } }) {
    this.setState({ value });
  }
}

export default RadioGroup;
