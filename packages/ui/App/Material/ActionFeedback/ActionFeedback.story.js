import { React, stories, text, select } from '../../story';
import ActionFeedback from './ActionFeedback';

stories(module, __dirname, {
  Default() {
    return (
      <div
        style={{
          width: '300px',
        }}
      >
        <ActionFeedback
          title={text('Title', 'Hello, world!')}
          type={select('Type', ['default', 'error', 'info'])}
        >
          {({ handleOpen }) => (
            <button type="button" onClick={handleOpen}>
              Open Feedback
            </button>
          )}
        </ActionFeedback>
      </div>
    );
  },
});
