import React from 'react';
import toJson from 'enzyme-to-json';
import { shallow } from 'enzyme';
import Checkbox from './Checkbox';

/**
 * @test {Checkbox}
 */
describe('The `Checkbox` component', () => {
  test('renders correctly', () => {
    const component = shallow(
      <Checkbox
        color="secondary"
        defaultChecked={true}
        disabled={true}
        name="MyFancyCheckbox"
      />
    );

    expect(toJson(component)).toMatchSnapshot();
  });
});
