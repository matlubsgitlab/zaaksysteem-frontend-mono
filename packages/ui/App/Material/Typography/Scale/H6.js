import React from 'react';
import Typography from '@material-ui/core/Typography';

/**
 * @see /npm-mintlab-ui/storybook/?selectedKind=Material/Typography&selectedStory=H6
 *
 * @param {Object} props
 * @param {*} props.children
 * @param {Object} props.classes
 * @return {ReactElement}
 */
export const H6 = ({ children, classes }) => (
  <Typography variant="h6" classes={classes}>
    {children}
  </Typography>
);
