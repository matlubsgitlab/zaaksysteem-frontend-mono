/**
 * Generates classnames through the withStyles HOC.
 * This gets injected as the `classes` prop into the TextField component
 * @param {Object} theme
 * @return {JSS}
 */
export const textFieldStyleSheet = ({
  palette: { common },
  mintlab: { greyscale, shadows },
  typography,
}) => ({
  // Form Control
  formControl: {
    width: '100%',
    padding: '0px 6px 0px 12px',
    'background-color': greyscale.light,
    'border-radius': '23px',
    'justify-content': 'center',
    height: '46px',
    fontWeight: typography.fontWeightLight,
  },
  // Input
  input: {
    color: greyscale.offBlack,
    marginRight: '6px',
  },
  // all focus
  focus: {
    color: greyscale.offBlack,
    backgroundColor: common.white,
  },
  // root focus
  rootFocus: {
    boxShadow: shadows.flat,
  },
  // end adornment
  endAdornment: {
    marginRight: '4px',
  },
});
