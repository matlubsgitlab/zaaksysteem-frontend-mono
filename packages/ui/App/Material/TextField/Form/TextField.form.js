import React, { Component } from 'react';
import classNames from 'classnames';
import MuiTextField from '@material-ui/core/TextField';
import { withStyles } from '@material-ui/styles';
import { bind, callOrNothingAtAll, unique } from '@mintlab/kitchen-sink/source';
import { addScopeAttribute } from '../../../library/addScope';
import { textFieldStyleSheet } from './TextField.form.style';

const DEFAULT_ROWS = 3;

/**
 * *Material Design* **Text field**.
 * - facade for *Material-UI* `TextField`
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Material/TextField
 * @see /npm-mintlab-ui/documentation/consumer/manual/TextField.html
 * @see https://material-ui.com/api/text-field/
 *
 * @reactProps {Object} classes
 * @reactProps {boolean} [disabled=false]
 * @reactProps {string} [error]
 * @reactProps {string} [info]
 * @reactProps {boolean} [required=false]
 * @reactProps {string} name
 * @reactProps {number} rows
 * @reactProps {string} value
 * @reactProps {function} onChange
 * @reactProps {function} onBlur
 * @preactProps {string} [scope]
 * @reactProps {function} onKeyPress
 * @reactProps {function} submit
 * @reactProps {boolean} [isMultiline=false]
 * @reactProps {boolean} [autoFocus=false]
 */
export class TextField extends Component {
  /**
   * @param {Object} props
   */
  constructor(props) {
    super(props);
    this.state = {
      focus: false,
    };
    bind(this, 'handleFocus', 'handleBlur', 'handleKeyPress');
  }

  /**
   * @see https://reactjs.org/docs/react-component.html#render
   *
   * @return {ReactElement}
   */
  /* eslint-disable jsx-a11y/no-autofocus */
  render() {
    const {
      props: {
        classes: {
          input,
          inputError,
          inputRoot,
          inputRootError,
          inputRootErrorFocus,
          formControl,
        },
        disabled = false,
        type = 'text',
        error,
        placeholder,
        name,
        required = false,
        rows = DEFAULT_ROWS,
        rowsMax,
        value,
        onChange,
        onKeyDown,
        scope,
        InputProps,
        inputProps,
        isMultiline = false,
        autoFocus,
      },
      state: { focus },
    } = this;

    return (
      <MuiTextField
        error={Boolean(error)}
        type={type}
        value={value}
        onChange={onChange}
        onFocus={this.handleFocus}
        onBlur={this.handleBlur}
        onKeyPress={this.handleKeyPress}
        onKeyDown={onKeyDown}
        disabled={disabled}
        id={unique()}
        placeholder={placeholder}
        name={name}
        required={required}
        multiline={isMultiline}
        rows={rows}
        rowsMax={rowsMax}
        autoFocus={autoFocus}
        classes={{
          root: formControl,
        }}
        InputProps={{
          classes: {
            root: classNames(inputRoot, {
              [inputRootError]: Boolean(error),
              [inputRootErrorFocus]: Boolean(error) && focus,
            }),
            input: classNames(input, { [inputError]: Boolean(error) }),
          },
          ...InputProps,
        }}
        inputProps={{
          ...addScopeAttribute(scope, 'text-field-input'),
          ...(inputProps || {}),
        }}
        {...addScopeAttribute(scope, 'text-field')}
      />
    );
  }

  handleFocus() {
    this.setState({ focus: true });
  }

  handleBlur(event) {
    const { onBlur } = this.props;
    this.setState({ focus: false });
    callOrNothingAtAll(onBlur, [event]);
  }

  handleKeyPress(event) {
    const { onKeyPress, submit, isMultiline } = this.props;

    if (onKeyPress) return onKeyPress(event);

    if (submit && event.key.toLowerCase() === 'enter' && !isMultiline) {
      submit(event);
    }
  }
}

export default withStyles(textFieldStyleSheet)(TextField);
