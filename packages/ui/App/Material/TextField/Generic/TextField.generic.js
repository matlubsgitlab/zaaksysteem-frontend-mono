import React, { Component, createElement } from 'react';
import classNames from 'classnames';
import MuiTextField from '@material-ui/core/TextField';
import { withStyles } from '@material-ui/styles';
import { callOrNothingAtAll, unique } from '@mintlab/kitchen-sink/source';
import { bind } from '@mintlab/kitchen-sink/source';
import InputAdornment from '@material-ui/core/InputAdornment';
import CloseIndicator from '../../../Shared/CloseIndicator/CloseIndicator';
import { addScopeAttribute } from '../../../library/addScope';
import { textFieldStyleSheet } from './TextField.generic.style';

/**
 *  *Material Design* **Text field**.
 * - facade for *Material-UI* `TextField`
 *
 * This is a simple TextField with generic styling,
 * meant to be used as a general GUI component.
 *
 * @see https://material-ui.com/api/text-field/
 *
 * @reactProps {Object} classes
 * @reactProps {string} placeholder
 * @reactProps {string} name
 * @reactProps {ReactElement} [startAdornment]
 * @reactProps {Function} [closeAction]
 * @reactProps {string} value
 * @reactProps {function} onChange
 * @reactProps {function} onKeyPress
 * @reactProps {function} onKeyDown
 * @reactProps {function} onBlur
 * @preactProps {string} [scope]
 */
export class TextField extends Component {
  /**
   * @param {Object} props
   */
  constructor(props) {
    super(props);
    this.state = {
      focus: false,
    };
    bind(this, 'EndAdornment', 'StartAdornment', 'handleFocus', 'handleBlur');
  }

  /**
   * @see https://reactjs.org/docs/react-component.html#render
   * @return {ReactElement}
   */
  render() {
    const {
      props: {
        classes,
        name,
        value,
        startAdornment,
        placeholder,
        closeAction: action,
        onChange,
        onKeyPress,
        onKeyDown,
        scope,
      },
      state: { focus },
      StartAdornment,
      EndAdornment,
    } = this;

    return (
      <MuiTextField
        value={value}
        id={unique()}
        name={name}
        placeholder={placeholder}
        classes={{
          root: classNames(classes.formControl, { [classes.focus]: focus }),
        }}
        InputProps={{
          classes: {
            input: classNames(classes.input, { [classes.focus]: focus }),
          },
          disableUnderline: true,
          ...(startAdornment && { startAdornment: StartAdornment() }),
          ...(action && { endAdornment: EndAdornment() }),
        }}
        inputProps={{
          ...addScopeAttribute(scope, 'text-field-input'),
        }}
        onChange={onChange}
        onKeyPress={onKeyPress}
        onKeyDown={onKeyDown}
        onFocus={this.handleFocus}
        onBlur={this.handleBlur}
        {...addScopeAttribute(scope, 'text-field')}
      />
    );
  }

  handleFocus() {
    this.setState({ focus: true });
  }

  handleBlur(event) {
    const { onBlur } = this.props;
    this.setState({ focus: false });
    callOrNothingAtAll(onBlur, [event]);
  }

  /**
   * @return {ReactElement}
   */
  StartAdornment() {
    const {
      props: { classes, startAdornment },
      state: { focus },
    } = this;

    const props = {
      position: 'start',
      classes: {
        root: classNames({ [classes.focus]: focus }),
      },
    };

    return createElement(InputAdornment, props, startAdornment);
  }

  /**
   * @return {ReactElement}
   */
  EndAdornment() {
    const {
      props: { closeAction: action, classes },
      state: { focus },
    } = this;

    const closeButton = createElement(CloseIndicator, { action });
    const props = {
      position: 'end',
      classes: {
        root: classNames({ [classes.focus]: focus }),
      },
    };

    return createElement(InputAdornment, props, closeButton);
  }
}

export default withStyles(textFieldStyleSheet)(TextField);
