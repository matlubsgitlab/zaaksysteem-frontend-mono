import React from 'react';
import MuiFab from '@material-ui/core/Fab';
import { withStyles } from '@material-ui/styles';
import { addScopeAttribute } from '../../library/addScope';
import Icon from '../Icon';
import { fabStylesheet } from './Fab.style';

/**
 * *Material Design* **Fab**.
 * - facade for *Material-UI* `Fab`
 *   additional props are passed through
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Material/Fab
 * @see /npm-mintlab-ui/documentation/consumer/manual/Fab.html
 *
 * @param {Object} props
 * @param {Object} props.classes
 * @param {string} props.size
 * @param {ReactElement} props.children
 * @param {string} [props.scope]
 * @return {ReactElement}
 */
export const Fab = props => {
  const { classes, action, children, color, label, scope, size } = props;

  return (
    <MuiFab
      classes={classes}
      color={color}
      aria-label={label}
      onClick={action}
      size={size}
      {...addScopeAttribute(scope, 'button')}
    >
      <Icon>{children}</Icon>
    </MuiFab>
  );
};

export default withStyles(fabStylesheet)(Fab);
