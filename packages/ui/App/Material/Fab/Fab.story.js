import { React, stories } from '../../story';
import Fab from '.';

stories(module, __dirname, {
  Default() {
    return (
      <Fab
        aria-label={'ariaLabel'}
        color="primary"
        action={() => {}}
        scope={'fab'}
      >
        add
      </Fab>
    );
  },
});
