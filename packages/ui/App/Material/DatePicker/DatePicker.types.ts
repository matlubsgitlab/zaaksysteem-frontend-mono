export type DatePickerPropsType = {
  value: Pick<DatePickerProps, 'value'>;
  onChange: (date: Date) => void;
  name: string;
  outputFormat: string;
  displayFormat: string;
  [key: string]: any;
};
