import React, { Fragment } from 'react';
import nlLocale from 'date-fns/locale/nl';
import DateFnsUtils from '@date-io/date-fns';
import {
  DatePicker as MUIDatePicker,
  MuiPickersUtilsProvider,
} from '@material-ui/pickers';
//@ts-ignore
import { callOrNothingAtAll } from '@mintlab/kitchen-sink/source';
import InputAdornment from '@material-ui/core/InputAdornment';
//@ts-ignore
import Icon from '@mintlab/ui/App/Material/Icon';
import CloseIndicator from '../../Shared/CloseIndicator';
import { useDatePickerStyleSheet } from './DatePicker.style';

const utils = new DateFnsUtils();

const DatePicker = ({
  value,
  onChange,
  onClose,
  name,
  outputFormat = 'yyyy-MM-dd',
  displayFormat = 'EEEE, d MMMM yyyy',
  clearable = true,
  disabled = false,
  ...rest
}) => {
  const classes = useDatePickerStyleSheet();

  const handleClose = event => {
    event.stopPropagation();
    event.preventDefault();
    callOrNothingAtAll(onClose);
  };

  const handleChange = newValue => {
    callOrNothingAtAll(onChange, () => [
      {
        target: {
          name,
          value: utils.format(newValue, outputFormat),
        },
      },
    ]);
  };

  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils} locale={nlLocale}>
      <MUIDatePicker
        name={name}
        disabled={disabled}
        value={value}
        onChange={handleChange}
        onAccept={handleChange}
        format={displayFormat}
        autoOk={true}
        InputProps={{
          endAdornment: (
            <Fragment>
              {value && clearable && (
                <CloseIndicator disabled={disabled} action={handleClose} />
              )}
              <InputAdornment
                position="end"
                classes={{
                  root: classes.adornment,
                }}
              >
                <Icon size="small">calendar</Icon>
              </InputAdornment>
            </Fragment>
          ),
          disableUnderline: true,
          classes: {
            root: classes.root,
          },
        }}
        {...rest}
        label=""
      />
    </MuiPickersUtilsProvider>
  );
};

export default DatePicker;
