import React from 'react';
import classNames from 'classnames';
import MuiTooltip from '@material-ui/core/Tooltip';
import { withStyles } from '@material-ui/styles';
import { tooltipStyleSheet } from './Tooltip.style';

/**
 * *Material Design* **Tooltip**.
 * - facade for *Material-UI* `Tooltip`
 * - additional props are passed through to that component
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Material/Tooltip
 * @see /npm-mintlab-ui/documentation/consumer/manual/Tooltip.html
 * @see https://material-ui.com/api/tooltip/
 *
 * @param {Object} props
 * @param {Array} props.children
 * @param {Object} props.classes
 * @param {string} [props.placement='top']
 * @param {string} [props.type='default']
 * @param {string} props.title
 * @param {boolean} [props.disabled=false]
 * @return {ReactElement}
 */
export const Tooltip = ({
  children,
  classes,
  title,
  placement = 'top',
  type = 'default',
  disabled = false,
  ...rest
}) => (
  <MuiTooltip
    title={disabled ? '' : title}
    placement={placement}
    classes={{
      tooltip: classNames(classes.all, classes[type]),
      popper: classes.popper,
    }}
    {...rest}
    disableFocusListener={disabled}
    disableHoverListener={disabled}
    disableTouchListener={disabled}
  >
    <div className={classes.wrapper}>{children}</div>
  </MuiTooltip>
);

export default withStyles(tooltipStyleSheet)(Tooltip);
