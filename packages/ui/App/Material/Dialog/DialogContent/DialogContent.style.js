/**
 * @return {JSS}
 */
export const dialogContentStyleSheet = ({ typography }) => ({
  root: {
    ...typography.body2,
    padding: 0,
  },
  padded: {
    ...typography.body2,
    padding: '0 24px 16px',
  },
});
