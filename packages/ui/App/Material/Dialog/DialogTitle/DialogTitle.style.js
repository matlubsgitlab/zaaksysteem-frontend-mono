/**
 * @return {JSS}
 */
const defaultRoot = {
  display: 'flex',
  alignItems: 'center',
  paddingTop: 0,
  paddingBottom: 0,
  height: 70,
  zIndex: 1,
};

export const dialogTitleStyleSheet = () => ({
  rootNormal: defaultRoot,
  rootElevated: {
    ...defaultRoot,
    boxShadow: '0px 0px 5px 0px rgba(0,0,0,0.3)',
    '& + *': {
      paddingTop: 16,
    },
  },
  typography: {
    flex: '1 0 auto',
  },
  icon: {
    paddingRight: 8,
  },
  closeButton: {
    paddingLeft: 8,
  },
});
