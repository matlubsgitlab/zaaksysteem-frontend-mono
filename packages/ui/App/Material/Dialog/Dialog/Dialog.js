import React from 'react';
import MuiDialog from '@material-ui/core/Dialog';
import { withStyles } from '@material-ui/styles';
import { addScopeAttribute } from '../../../library/addScope';
import { dialogStyleSheet } from './Dialog.style';

/**
 * *Material Design* **Dialog**.
 * - facade for *Material-UI* `Dialog`
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Material/Dialog
 * @see /npm-mintlab-ui/documentation/consumer/manual/Dialog.html
 *
 * @param {Object} props
 * @param {boolean} [props.open=false]
 * @param {string} [props.children]
 * @param {string} [props.scope]
 * @return {ReactElement}
 */
export const Dialog = React.forwardRef(
  ({ open = false, classes, scope, children, ...rest }, ref) => (
    <MuiDialog
      ref={ref}
      open={open}
      classes={classes}
      {...addScopeAttribute(scope, 'dialog')}
      {...rest}
    >
      {children}
    </MuiDialog>
  )
);

Dialog.displayName = 'Dialog';

export default withStyles(dialogStyleSheet)(Dialog);
