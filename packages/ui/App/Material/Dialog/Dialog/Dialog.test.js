import React from 'react';
import toJson from 'enzyme-to-json';
import { shallow } from 'enzyme';
import { Dialog } from './Dialog';

const classes = {
  content: '',
};

/**
 * @test {Dialog}
 */
describe('The `Dialog` component', () => {
  test('renders correctly', () => {
    const component = shallow(<Dialog classes={classes}>test</Dialog>);

    expect(toJson(component)).toMatchSnapshot();
  });
});
