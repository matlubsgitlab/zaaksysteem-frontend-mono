import { withStyles } from '@material-ui/styles';
import DialogActions from '@material-ui/core/DialogActions';
import { dialogActionsStyleSheet } from './DialogActions.style';

export default withStyles(dialogActionsStyleSheet)(DialogActions);
