import { withStyles } from '@material-ui/styles';
import Divider from '@material-ui/core/Divider';
import { dialogDividerStyleSheet } from './DialogDivider.style';

export default withStyles(dialogDividerStyleSheet)(Divider);
