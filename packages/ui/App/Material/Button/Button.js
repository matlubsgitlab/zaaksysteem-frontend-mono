import React, { Fragment } from 'react';
import IconButton from '@material-ui/core/IconButton';
import MuiButton from '@material-ui/core/Button';
import { withStyles } from '@material-ui/styles';
import { addScopeAttribute } from '../../library/addScope';
import { CustomPalette } from '../MaterialUiThemeProvider/library/CustomPalette';
import Icon from '../Icon';
import { parseAction, parsePresets } from './library/utilities';
import { buttonSemiContained } from './Button.style';

const SemiContainedButton = properties => (
  <MuiButton {...properties} variant="contained" />
);

const EnhancedSemiContainedButton = withStyles(buttonSemiContained)(
  SemiContainedButton
);

/**
 * *Material Design* **Button**.
 * - facade for *Material-UI* `Button` and `IconButton`
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Material/Button
 * @see /npm-mintlab-ui/documentation/consumer/manual/Button.html
 * @see https://material-ui.com/api/button/
 * @see https://material-ui.com/api/icon-button/
 *
 * @param {Object} props
 * @param {Function|string} [props.action]
 * @param {*} props.children
 * @param {*} [props.component]
 * @param {*} [props.classes]
 * @param {boolean} [props.disabled=false]
 *   [Disabled State](https://html.spec.whatwg.org/multipage/interactive-elements.html#command-facet-disabledstate)
 * @param {string} [props.label]
 *   Assistive text for floating action and icon buttons
 * @param {Array} [props.presets=[]]
 *   color, fullWidth, mini, size and variant
 * @param {string} [props.scope]
 * @param {any} [props.icon]
 * @return {ReactElement}
 */
export function Button({
  action,
  children,
  classes,
  disabled = false,
  icon,
  iconSize = 'extraSmall',
  label,
  presets = [],
  scope,
  ...rest
}) {
  const props = {
    ...parsePresets(presets),
  };
  const { color, variant } = props;

  function createInstance(instanceProps) {
    const compact = {
      icon() {
        return (
          <IconButton
            aria-label={label}
            classes={classes}
            color={instanceProps.color}
            disabled={disabled}
            onClick={action}
            {...addScopeAttribute(scope, 'button')}
            {...rest}
          >
            <Icon size={instanceProps.size}>{children}</Icon>
          </IconButton>
        );
      },
    };

    if (Object.prototype.hasOwnProperty.call(compact, variant)) {
      return compact[variant]();
    }

    function getChildren() {
      if (icon) {
        return (
          <Fragment>
            <Icon size={iconSize}>{icon}</Icon>
            {children}
          </Fragment>
        );
      }

      return children;
    }

    const baseProps = {
      ...parseAction(action),
      ...instanceProps,
      children: getChildren(),
      classes,
      disabled,
      ...rest,
    };

    if (variant === 'semiContained') {
      return (
        <EnhancedSemiContainedButton
          {...baseProps}
          {...addScopeAttribute(scope, 'button')}
        />
      );
    }

    return <MuiButton {...baseProps} {...addScopeAttribute(scope, 'button')} />;
  }

  if (color === 'review') {
    return (
      <CustomPalette>
        {createInstance({
          ...props,
          color: 'primary',
        })}
      </CustomPalette>
    );
  }

  if (color === 'danger') {
    return (
      <CustomPalette>
        {createInstance({
          ...props,
          color: 'secondary',
        })}
      </CustomPalette>
    );
  }

  return createInstance(props);
}

export default Button;
