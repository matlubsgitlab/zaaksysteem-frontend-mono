// @see https://material-ui.com/api/button/
/**
 * @type {Array<string>}
 */
export const colors = [
  'default',
  'inherit',
  'primary',
  'secondary',
  'review',
  'danger',
];
/**
 * @type {Array<string>}
 */
export const sizes = ['small', 'medium', 'large'];
/**
 * @type {Array<string>}
 */
export const iconSizes = ['extraSmall', 'small', 'medium', 'large'];
/**
 * @type {Array<string>}
 */
export const variants = [
  'text',
  'icon',
  'outlined',
  'contained',
  'semiContained',
];

/**
 * Get a mutually exlcusive array intersection value.
 *
 * @param {Array<string>} haystack
 * @param {Array<string>} needles
 * @param {string} fallback
 * @return {string|undefined}
 */
export function oneOf(haystack, needles, fallback) {
  const [result, overflow] = needles.filter(name => haystack.includes(name));

  if (result && !overflow) {
    return result;
  }

  return fallback;
}

export function getSizes(presets) {
  if (presets.includes('icon')) {
    return iconSizes;
  }

  return sizes;
}

/**
 * @param {Array} presets
 * @return {Object}
 */
export const parsePresets = presets => ({
  color: oneOf(colors, presets, 'default'),
  fullWidth: presets.includes('fullWidth'),
  mini: presets.includes('mini') ? 'true' : 'false',
  size: oneOf(getSizes(presets), presets, 'medium'),
  variant: oneOf(variants, presets, 'text'),
});

/**
 * @param {*} value
 * @param {string} type
 * @return {*}
 */
function getByType(value, type) {
  if (typeof value == type) {
    return value;
  }
}

/**
 * Get an object with mutually exclusive `href` and `onClick` properties.
 *
 * @param {Function|string} action
 * @return {Object}
 */
export const parseAction = action => ({
  href: getByType(action, 'string'),
  onClick: getByType(action, 'function'),
});
