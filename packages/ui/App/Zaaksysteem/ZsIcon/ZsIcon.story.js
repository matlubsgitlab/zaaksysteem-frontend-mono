import { React, stories, select } from '../../story';
import getPresets from './library/presets';
import ZsIcon from '.';

const { keys } = Object;

stories(module, __dirname, {
  Default() {
    const size = select(
      'Size',
      ['extraSmall', 'small', 'medium', 'large'],
      'medium'
    );

    const presets = getPresets();
    const categories = keys(presets);

    return (
      <div>
        {categories.map(category => {
          const categoryIcons = keys(presets[category]);

          return (
            <div key={category}>
              <div>{category}:</div>
              {categoryIcons.map(icon => {
                const iconPath = `${category}.${icon}`;

                return (
                  <span
                    key={icon}
                    style={{
                      display: 'inline-flex',
                      alignItems: 'center',
                      margin: '0.2em 0.5em',
                      padding: '0.2em 0.5em',
                      border: '1px solid #ddd',
                      fontFamily: 'Menlo, Consolas',
                      fontSize: '0.8rem',
                    }}
                  >
                    <ZsIcon size={size}>{iconPath}</ZsIcon>
                    <span
                      style={{
                        marginLeft: '0.5em',
                        color: '#666',
                      }}
                    >
                      {icon}
                    </span>
                  </span>
                );
              })}
            </div>
          );
        })}
      </div>
    );
  },
});
