import { makeStyles } from '@material-ui/core/styles';
import { Theme } from '@mintlab/ui/types/Theme';

export const useMultilineOptionStyle = makeStyles(
  ({ mintlab: { greyscale } }: Theme) => ({
    optionWrapper: {
      padding: '0px 6px',
      position: 'relative',
      cursor: 'pointer',
      display: 'flex',
      flexDirection: 'row',
    },
    labelWrapper: {
      display: 'flex',
      flexDirection: 'column',
      minWidth: 0,
    },
    optionLabel: {
      marginBottom: '4px',
    },
    subLabel: {
      color: greyscale.darkest,

      '& > p': {
        whiteSpace: 'nowrap',
        overflow: 'hidden',
        textOverflow: 'ellipsis',
      },
    },
    icon: {
      paddingRight: 10,
    },
  })
);
