export type TranslationKeysType =
  | 'form:choose'
  | 'form:loading'
  | 'form:beginTyping'
  | 'form:creatable'
  | 'form:create';
export type TranslationsType = { [k in TranslationKeysType]: string };
