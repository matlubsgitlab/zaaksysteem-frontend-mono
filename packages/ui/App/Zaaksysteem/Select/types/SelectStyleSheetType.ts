import { Theme } from '@mintlab/ui/types/Theme';
import { ReactSelectSubcomponentNames } from '../types/ReactSelectSubcomponentNames';

export type StyleSheetCreatorType = (arg: {
  theme: Theme;
  focus?: boolean;
  error?: any;
}) => {
  [k in ReactSelectSubcomponentNames]?: (base: any, state: any) => any;
};
