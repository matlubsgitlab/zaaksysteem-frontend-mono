import { useState, useEffect } from 'react';
import { useDebouncedCallback } from 'use-debounce';
import { TranslationsType as Translations } from '../types/TranslationsType';

export const DELAY = 400;
export const MIN_CHARACTERS = 3;

const defaultTranslations: Translations = {
  'form:choose': 'Select…',
  'form:loading': 'Loading…',
  'form:beginTyping': 'Begin typing to search…',
  'form:creatable': 'Typ, en <ENTER> om te bevestigen.',
  'form:create': 'Aanmaken:',
};

const isValidInput = (input: string) => {
  return Boolean(input) && input.length >= MIN_CHARACTERS;
};

export const useSelectBase = ({
  autoLoad,
  hasInitialChoices,
  focus,
  name,
  translations,
  getChoices,
  eventType,
  onBlur,
  onChange,
  onKeyDown,
}: {
  autoLoad?: boolean;
  hasInitialChoices?: boolean;
  focus?: boolean;
  name?: string;
  translations?: Translations;
  getChoices?: (input: string) => void;
  eventType: string;
  onBlur?: (event: any) => void;
  onChange?: (event: any) => void;
  onKeyDown?: (event: any) => void;
}) => {
  const [focusState, setFocus] = useState(focus);

  const [getChoicesDebounced] = useDebouncedCallback((input: string) => {
    getChoices &&
      !autoLoad &&
      !hasInitialChoices &&
      isValidInput(input) &&
      getChoices(input);
  }, DELAY);

  useEffect(() => {
    autoLoad && getChoices && getChoices('');
  }, []);

  const defaultedTranslations = translations || defaultTranslations;

  return {
    focusState,
    defaultedTranslations: {
      choose: defaultedTranslations['form:choose'],
      loadingMessage: defaultedTranslations['form:loading'],
      beginTyping: defaultedTranslations['form:beginTyping'],
      creatable: defaultedTranslations['form:creatable'],
      create: defaultedTranslations['form:create'],
    },
    eventHandlers: {
      onInputChange: getChoicesDebounced,
      onKeyDown: (keyDownEvent: React.KeyboardEvent) => {
        const { key, keyCode, charCode } = keyDownEvent;
        onKeyDown &&
          onKeyDown({
            key,
            keyCode,
            charCode,
            name,
            type: eventType,
          });

        keyDownEvent.stopPropagation();
      },
      onChange: (value: any) => {
        onChange &&
          onChange({
            target: {
              name,
              value,
              type: eventType,
            },
          });
      },
      onBlur: () => {
        setFocus(false);
        onBlur &&
          onBlur({
            target: {
              name,
            },
          });
      },

      onFocus: () => {
        setFocus(true);
      },
    },
  };
};
