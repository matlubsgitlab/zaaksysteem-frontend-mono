import { createElement } from 'react';
import { FormSelect } from '../Form/FormSelect';
import { CreatableSelect } from '../Form/CreatableSelect';
import { GenericSelect } from '../Generic/GenericSelect';

export const SelectStrategy = ({
  creatable = false,
  generic = false,
  ...rest
}) => {
  let component: any;

  if (creatable) {
    component = CreatableSelect;
  } else if (generic) {
    component = GenericSelect;
  } else {
    component = FormSelect;
  }

  return createElement(component, rest as any);
};

export default SelectStrategy;
