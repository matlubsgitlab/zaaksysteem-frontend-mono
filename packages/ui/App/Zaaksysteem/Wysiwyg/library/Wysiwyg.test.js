import React from 'react';
import { shallow } from 'enzyme';
import { Wysiwyg } from './Wysiwyg';

/**
 * Editor state dummy.
 * @type {Object}
 */
const editorState = {
  getCurrentContent() {
    return {
      getBlockMap() {
        return {
          forEach() {},
        };
      },
    };
  },
};

/**
 * @test {Wysiwyg}
 */
describe('The `Wysiwyg` component', () => {
  describe('has a `handleChange` method', () => {
    test('that calls the `handleChange` prop', () => {
      const spy = jest.fn();
      const wrapper = shallow(
        <Wysiwyg classes={{}} name="richtext" onChange={spy} />
      );
      const instance = wrapper.instance();
      const assertedCallCount = 1;

      instance.handleChange(editorState);

      expect(spy).toHaveBeenCalledTimes(assertedCallCount);
    });
  });
});
