import Color from 'color';
import { React, stories, text, number } from '../../story';
import {
  palette,
  greyscale,
  shadows,
} from '../../Material/MaterialUiThemeProvider/library/theme';
import './Theme.css';

const generateColumns = columns => (
  <div className="wrapper">
    {columns.map((column, columnIndex) => (
      <div className="column-wrapper" key={columnIndex}>
        {column.label && <div className="name">{column.label}</div>}
        {column.colors.map((color, colorIndex) => (
          <div
            key={colorIndex}
            className="color"
            style={{ backgroundColor: color.color }}
          >
            <span className="label">{color.label}</span>
          </div>
        ))}
      </div>
    ))}
  </div>
);

stories(module, __dirname, {
  'Generate Palette': function GeneratePalette() {
    const rangeOptions = {
      range: true,
      min: 0,
      max: 100,
      step: 1,
    };

    const mainColors = text('Colors', '#357DF4,#23D17F,#FF9D00,#FF345B');
    const light = number('Light', 10, rangeOptions);
    const lighter = number('Lighter', 20, rangeOptions);
    const lightest = number('Lightest', 40, rangeOptions);
    const dark = number('Dark', 10, rangeOptions);
    const darker = number('Darker', 20, rangeOptions);
    const darkest = number('Darkest', 40, rangeOptions);

    const colorColumns = mainColors
      .split(',')
      .map(hex => hex.trim())
      .filter(hex => hex && /^#[a-zA-Z0-9]{6}/.test(hex))
      .map(mainColor => {
        const hexLightest = Color(mainColor)
          .lighten(lightest / 100)
          .hex();

        const hexLighter = Color(mainColor)
          .lighten(lighter / 100)
          .hex();
        const hexLight = Color(mainColor)
          .lighten(light / 100)
          .hex();
        const hexDark = Color(mainColor)
          .darken(dark / 100)
          .hex();
        const hexDarker = Color(mainColor)
          .darken(darker / 100)
          .hex();
        const hexDarkest = Color(mainColor)
          .darken(darkest / 100)
          .hex();

        return {
          label: mainColor,
          colors: [
            {
              label: `lightest ${hexLightest}`,
              color: hexLightest,
            },
            {
              label: `lighter ${hexLighter}`,
              color: hexLighter,
            },
            {
              label: `light ${hexLight}`,
              color: hexLight,
            },
            {
              label: `main ${mainColor}`,
              color: mainColor,
            },
            {
              label: `dark ${hexDark}`,
              color: hexDark,
            },
            {
              label: `darker ${hexDarker}`,
              color: hexDarker,
            },
            {
              label: `darkest ${hexDarkest}`,
              color: hexDarkest,
            },
          ],
        };
      });

    return generateColumns(colorColumns);
  },
  'Current Palette': function PaletteNow() {
    const hideColors = [
      'error',
      'review',
      'danger',
      'support',
      'common',
      'vivid',
    ];

    const colorColumns = Object.entries(palette)
      .filter(([name]) => hideColors.indexOf(name) === -1)
      .map(([name, value]) => ({
        label: name,
        colors: Object.entries(value).map(([colorName, color]) => ({
          label: `${colorName} ${color}`,
          color: color,
        })),
      }));

    return generateColumns(colorColumns);
  },
  Greyscales: function Greyscale() {
    const keys = Object.keys(greyscale);

    const colorColumns = [
      {
        label: 'greyscale',
        colors: keys.map(key => ({
          label: `${key} ${greyscale[key]}`,
          color: greyscale[key],
        })),
      },
    ];

    return generateColumns(colorColumns);
  },
  Shadows: function Shadows() {
    const keys = Object.keys(shadows);

    return (
      <div className="wrapper">
        {keys.map((key, index) => (
          <div
            key={index}
            className="shadow"
            style={{
              boxShadow: shadows[key],
            }}
          >
            <span className="label">{key}</span>
          </div>
        ))}
      </div>
    );
  },
});
