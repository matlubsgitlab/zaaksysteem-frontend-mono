/* eslint-disable no-magic-numbers */
import { parseSize } from './parseSize';

/**
 * @test {parseSize}
 */
describe('The `Svg` component’s `parseSize` utility function', () => {
  test('parses em lengths', () => {
    expect(parseSize('10em')).toEqual([10, 'em']);
  });

  test('parses ex lengths', () => {
    expect(parseSize('10ex')).toEqual([10, 'ex']);
  });

  test('parses rem lengths', () => {
    expect(parseSize('10rem')).toEqual([10, 'rem']);
  });

  test('parses px lengths', () => {
    expect(parseSize('10px')).toEqual([10, 'px']);
  });

  test('parses vh lengths', () => {
    expect(parseSize('10vh')).toEqual([10, 'vh']);
  });

  test('parses vw lengths', () => {
    expect(parseSize('10vw')).toEqual([10, 'vw']);
  });

  test('parses percentages', () => {
    expect(parseSize('10%')).toEqual([10, '%']);
  });

  test('parses decimal values with an integer part and a fractional part', () => {
    expect(parseSize('0.9%')).toEqual([0.9, '%']);
  });

  test('parses decimal values without an integer part', () => {
    expect(parseSize('.9%')).toEqual([0.9, '%']);
  });

  test('throws if a decimal value has no fractional part', () => {
    expect(() => parseSize('9.%')).toThrow();
  });

  test('throws on numeric integer values without a unit', () => {
    expect(() => parseSize('19')).toThrow();
  });

  test('throws on numeric decimal values has no supported unit', () => {
    expect(() => parseSize('19.5')).toThrow();
  });

  test('throws if the numeric is not numeric', () => {
    expect(() => parseSize('a1px')).toThrow();
  });

  test('throws if the unit is not supported', () => {
    expect(() => parseSize('12cm')).toThrow();
    expect(() => parseSize('12mm')).toThrow();
  });

  test('throws if the size is not a string', () => {
    expect(() => parseSize(42)).toThrow();
  });
});
