import React from 'react';
import { withStyles } from '@material-ui/styles';
import classnames from 'classnames';
import { Body2 } from '../../../Material/Typography';
import { chipStylesheet } from './Chip.style';

const Chip = ({
  label,
  renderIcon,
  classes,
  selected,
  error,
  onClick,
  onFocus,
  onBlur,
}) => (
  <button
    type="button"
    onClick={onClick}
    onFocus={onFocus}
    onBlur={onBlur}
    className={classnames(
      classes.chip,
      selected && classes.chipSelected,
      error && classes.chipError,
      renderIcon && classes.chipHasIcon
    )}
  >
    {renderIcon && typeof renderIcon === 'function' && (
      <span className={classes.icon}>{renderIcon()}</span>
    )}
    <Body2 classes={{ root: classes.label }}>{label}</Body2>
  </button>
);

export default withStyles(chipStylesheet)(Chip);
