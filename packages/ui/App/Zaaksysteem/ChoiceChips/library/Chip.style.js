/**
 * @param {Object} theme
 * @return {JSS}
 */
export const chipStylesheet = ({
  mintlab: { greyscale },
  palette: { primary, error },
}) => ({
  chip: {
    display: 'flex',
    border: `1px solid ${greyscale.darker}`,
    backgroundColor: 'inherit',
    alignItems: 'center',
    borderRadius: 15,
    paddingLeft: 10,
    paddingRight: 10,
    height: 30,
    outline: 'none',
    cursor: 'pointer',
    borderImage: 'none',

    '&:focus, &:hover': {
      borderColor: greyscale.darkest,
    },
    '&:active': {
      borderStyle: 'solid',
    },
  },
  chipHasIcon: {
    paddingLeft: 5,
  },
  chipSelected: {
    backgroundColor: primary.lighter,
  },
  chipError: {
    borderColor: error.main,
  },
  icon: {
    paddingRight: 5,
    display: 'inline-flex',
  },
  label: {
    color: greyscale.darkest,
  },
});
