export const moveItem: <T>(
  items: T[],
  sourceIndex: number,
  destinationIndex: number
) => T[] = (items, sourceIndex, destinationIndex) => {
  const nx = [...items];
  const [moved] = nx.splice(sourceIndex, 1);
  nx.splice(destinationIndex, 0, moved);

  return nx;
};
