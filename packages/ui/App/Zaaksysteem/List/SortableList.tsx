import React from 'react';
import { DragDropContext, Draggable, Droppable } from 'react-beautiful-dnd';
import classNames from 'classnames';
import { moveItem } from './moveItem';
import { useListVariantStyle } from './List.style';

export type SortableListPropsType<T> = {
  renderItem: (item: T, index: number) => React.ReactElement;
  onReorder: (value: T[]) => void;
  value: T[];
};

function SortableList<T extends { id: string }>({
  renderItem,
  onReorder,
  value,
}: SortableListPropsType<T>) {
  const classes = useListVariantStyle();

  const reorderItem = React.useCallback(
    (sourceIndex, destinationIndex) =>
      onReorder(moveItem(value, sourceIndex, destinationIndex)),
    [value]
  );

  return (
    <DragDropContext
      onDragEnd={({ destination, source }) =>
        destination && reorderItem(source.index, destination.index)
      }
    >
      <Droppable droppableId="attribute-options-droppable">
        {({ innerRef, droppableProps, placeholder }) => (
          <div ref={innerRef}>
            {value.map((item, index: number) => (
              <Draggable
                key={item.id}
                draggableId={item.id}
                index={index}
                {...droppableProps}
              >
                {(draggableProvided, draggableSnapshot) => {
                  const {
                    dragHandleProps,
                    draggableProps,
                    innerRef: draggableRef,
                  } = draggableProvided;
                  const { isDragging, isDropAnimating } = draggableSnapshot;
                  return (
                    <div
                      className={classNames({
                        [classes.itemContainer]: true,
                        [classes.itemContainerHover]: true,
                        [classes.itemContainerDragging]: isDragging,
                      })}
                      ref={draggableRef}
                      {...dragHandleProps}
                      {...draggableProps}
                      style={
                        isDragging && !isDropAnimating
                          ? {
                              ...draggableProps.style,
                              transform:
                                'rotate(-2deg) ' +
                                draggableProps.style?.transform,
                            }
                          : draggableProps.style
                      }
                    >
                      {renderItem(item, index)}
                    </div>
                  );
                }}
              </Draggable>
            ))}
            {placeholder}
          </div>
        )}
      </Droppable>
    </DragDropContext>
  );
}

export default SortableList;
