import { makeStyles } from '@material-ui/core/styles';
import { Theme } from '@mintlab/ui/types/Theme';

const mainPadding = 15;
const mainRadius = 4;

export const useListContainerStyle = makeStyles(
  ({ palette, mintlab: { greyscale, shadows } }: Theme) => ({
    listContainer: {
      padding: 15,
      backgroundColor: greyscale.dark,
      borderRadius: 4,
    },
  })
);

export const useListVariantStyle = makeStyles(
  ({ palette, mintlab: { greyscale, shadows } }: Theme) => ({
    itemContainer: {
      boxShadow: shadows.flat,
      padding: mainPadding,
      borderRadius: mainRadius,
      marginBottom: mainPadding,
      backgroundColor: 'white',
    },
    itemContainerHover: {
      '&:hover': {
        backgroundColor: greyscale.darker,
      },
    },
    itemContainerDragging: {
      backgroundColor: palette.sahara.lighter,
      transition: 'all 0.03s',
    },
  })
);
