import React, { Suspense } from 'react';
import Loader from '../Loader';
import { SortableListPropsType } from './SortableList';

const LazyList = React.lazy(() =>
  import(
    // https://webpack.js.org/api/module-methods/#import
    /* webpackChunkName: "ui.sortableeditablelist" */
    './SortableList'
  )
);

export { default as ListContainer } from './ListContainer';
export { default as CommonList } from './CommonList';
export { default as useList } from './useList';

export function SortableList<T extends { id: string }>(
  props: SortableListPropsType<T>
) {
  return (
    <Suspense fallback={<Loader />}>
      {/* 
        // @ts-ignore */}
      <LazyList {...props} />
    </Suspense>
  ) as React.ReactElement;
}
