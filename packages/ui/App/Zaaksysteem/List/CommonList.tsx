import React from 'react';
import { useListVariantStyle } from './List.style';

type CommonListPropsType<T> = {
  renderItem: (
    item: T,
    index: number,
    itemClassName: string
  ) => React.ReactElement;
  value: T[];
};

function CommonList<T>({ renderItem, value }: CommonListPropsType<T>) {
  const classes = useListVariantStyle();

  return value.map((item, index) => {
    return renderItem(item, index, classes.itemContainer);
  }) as React.ReactElement[];
}

export default CommonList;
