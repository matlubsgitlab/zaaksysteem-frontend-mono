import React from 'react';

type useListType = <T>(
  list: T[],
  onChange: (nextValue: T[]) => void
) => {
  addItem: (item: T) => ReturnType<typeof onChange>;
  removeItem: (targetIndex: number) => ReturnType<typeof onChange>;
  updateItem: (nextItem: T, targetIndex: number) => ReturnType<typeof onChange>;
};

const useList: useListType = (list, onChange) => {
  return {
    addItem: React.useCallback(item => onChange([...list, item]), [list]),
    removeItem: React.useCallback(
      targetIndex =>
        onChange(
          list
            .slice(0, targetIndex)
            .concat(list.slice(targetIndex + 1, list.length))
        ),
      [list]
    ),
    updateItem: React.useCallback(
      (nextItem, targetIndex) => {
        const nextList = [...list];
        nextList.splice(targetIndex, 1, nextItem);

        return onChange(nextList);
      },
      [list]
    ),
  };
};

export default useList;
