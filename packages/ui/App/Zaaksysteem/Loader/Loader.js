import React, { useState, useEffect } from 'react';
import { withTheme } from '@material-ui/styles';
import { addScopeProp } from '../../library/addScope';
import * as map from './Spinner';

/**
 * Loader component based on *SpinKit*.
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Zaaksysteem/Loader
 * @see /npm-mintlab-ui/documentation/consumer/manual/Loader.html
 *
 * @param {Object} props
 * @param {boolean} [props.active=true]
 * @param {ReactElement} [props.children]
 * @param {string} [props.color=props.theme.palette.common.black]
 * @param {Object} [props.theme]
 *   Material-UI theme
 * @param {string} [props.type='circle']
 *   Loader name; available loaders are
 *   - `'circle'` ({@link Circle})
 *   - `'cube'` ({@link Cube})
 *   - `'fold'` ({@link Fold})
 *   - `'pulse'` ({@link Pulse})
 *   - `'wave'` ({@link Wave})
 * @param {number} [props.delay]
 * @param {Object} [props.className]
 * @return {ReactElement}
 */
export const Loader = ({
  active = true,
  color,
  theme,
  type = 'circle',
  scope,
  delay = 0,
  className,
}) => {
  const [delayedActive, setDelayedActive] = useState(delay === 0);
  const visible = delayedActive && active;

  useEffect(() => {
    let timeoutId;

    if (delay > 0) {
      timeoutId = setTimeout(() => setDelayedActive(true), parseInt(delay, 10));
    }

    return () => clearTimeout(timeoutId);
  });

  if (visible) {
    const LoaderComponent = map[type];

    return (
      <LoaderComponent
        color={color || theme.palette.common.black}
        className={className}
        {...addScopeProp(scope, 'loader')}
      />
    );
  }

  return null;
};

export default withTheme(Loader);
