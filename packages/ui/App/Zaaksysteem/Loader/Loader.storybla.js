import { React, stories, boolean, select, number } from '../../story';
import Loader from '.';

stories(module, __dirname, {
  Default() {
    const delay = number('Delay visible', 300);

    return (
      <Loader
        active={boolean('Active', true)}
        type={select(
          'Type',
          {
            circle: 'Circle',
            cube: 'Cube',
            fold: 'Fold',
            pulse: 'Pulse',
            wave: 'Wave',
          },
          'circle'
        )}
        delay={delay}
        scope="story"
      />
    );
  },
});
