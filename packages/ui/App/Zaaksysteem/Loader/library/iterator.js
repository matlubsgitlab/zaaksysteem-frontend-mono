const INCREMENT = 1;

/**
 * Loader component utility function.
 *
 * @param {number} length
 * @return {Array<number>}
 */
export const iterator = length =>
  Array.from(Array(length).keys()).map(number => number + INCREMENT);
