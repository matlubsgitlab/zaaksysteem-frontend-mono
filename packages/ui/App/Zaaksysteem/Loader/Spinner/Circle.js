import React from 'react';
import classnames from 'classnames';
import { addScopeAttribute } from '../../../library/addScope';
import { iterator } from '../library/iterator';
import style from './Circle.module.css';

const LENGTH = 12;

/**
 * Spinner type for the {@link Loader} component
 * @param {Object} props
 * @param {string} props.color
 * @return {ReactElement}
 */
export const Circle = ({ color, scope, className }) => (
  <div
    className={classnames(style['circle'], className)}
    {...addScopeAttribute(scope)}
  >
    {iterator(LENGTH).map(item => (
      <div key={item} className={style['child']}>
        <span
          className={style['dot']}
          style={{
            backgroundColor: color,
          }}
        />
      </div>
    ))}
  </div>
);
