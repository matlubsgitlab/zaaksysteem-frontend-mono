import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { DropdownMenu } from './DropdownMenu';

/**
 * @test {DropdownMenu}
 */
describe('The `DropdownMenu` component', () => {
  test('renders correctly', () => {
    const items = [{ label: 'foo' }, { label: 'bar' }];
    const component = shallow(
      <DropdownMenu items={items} classes={{}} manualId="testId">
        <b>Toggle</b>
      </DropdownMenu>
    );

    expect(toJson(component)).toMatchSnapshot();
  });
});
