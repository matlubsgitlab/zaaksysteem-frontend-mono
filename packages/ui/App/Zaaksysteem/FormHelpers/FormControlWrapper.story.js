import { React, stories, text, boolean } from '../../story';
import { TextField } from '../../Material/TextField';
import FormControlWrapper from './FormControlWrapper';
import { withFormControlWrapper } from './library/withFormControlWrapper';

const getProps = () => {
  return {
    label: text('Label', 'Naam kenmerk'),
    error: text('Error', ''),
    help: text(
      'Help',
      'Vul hier een tekstuele omschrijving in van het kenmerk.'
    ),
    hint: text('Hint', 'Dit kan later niet meer gewijzigd worden.'),
    required: boolean('Required', false),
    placeholder: text('Placeholder', 'Placeholder waarde'),
    compact: boolean('Compact', false),
    scope: 'story',
    name: 'story',
  };
};

stories(module, __dirname, {
  'Wrapper around TextField': function WrapperAroundTextField() {
    const {
      label,
      error,
      help,
      hint,
      required,
      compact,
      scope,
      placeholder,
      name,
    } = getProps();

    return (
      <div
        style={{
          width: '50%',
        }}
      >
        <FormControlWrapper
          label={label}
          error={error}
          help={help}
          hint={hint}
          required={required}
          compact={compact}
          scope={scope}
          touched={true}
        >
          <TextField
            name={name}
            label={label}
            error={error}
            help={help}
            hint={hint}
            required={required}
            placeholder={placeholder}
          />
        </FormControlWrapper>
      </div>
    );
  },
  'Using the HOC': function usingHoc() {
    const WrappedTextField = withFormControlWrapper(TextField);

    const {
      label,
      error,
      help,
      hint,
      required,
      compact,
      scope,
      placeholder,
      name,
    } = getProps();

    return (
      <WrappedTextField
        name={name}
        label={label}
        error={error}
        help={help}
        hint={hint}
        required={required}
        compact={compact}
        placeholder={placeholder}
        scope={scope}
      />
    );
  },
});
