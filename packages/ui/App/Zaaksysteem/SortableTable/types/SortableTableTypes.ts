import { ColumnProps, TableCellProps } from 'react-virtualized';

export type SortableTablePropsType = {
  rows: RowType[];
  columns: ColumnType[];
  onRowDoubleClick: any;
  loading?: boolean;
  noRowsMessage: string;
  /** If provided, the rows will be rendered with a fixed height,
   * with content cut off with ellipsis. If not, rows will fit to
   * their content. */
  rowHeight?: number;
  selectable?: boolean;
  [key: string]: any;
};

export type RowType = {
  uuid: string;
  name: string;
  selected?: boolean;
  parent?: string;
};

export type CellRendererPropsType = Pick<
  TableCellProps,
  'dataKey' | 'rowData' | 'parent' | 'columnIndex' | 'rowIndex'
>;

export interface ColumnType
  extends Omit<ColumnProps, 'cellRenderer' | 'dataKey'> {
  defaultSort?: boolean;
  showFromWidth?: number;
  name: string;
  cellRenderer?: ({
    dataKey,
    rowData,
  }: CellRendererPropsType) => React.ReactChild;
}
