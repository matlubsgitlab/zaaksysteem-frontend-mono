# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.26.7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.26.6...@zaaksysteem/common@0.26.7) (2020-04-14)

**Note:** Version bump only for package @zaaksysteem/common





## [0.26.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.26.5...@zaaksysteem/common@0.26.6) (2020-04-10)

**Note:** Version bump only for package @zaaksysteem/common





## [0.26.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.26.4...@zaaksysteem/common@0.26.5) (2020-04-09)

**Note:** Version bump only for package @zaaksysteem/common





## [0.26.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.26.3...@zaaksysteem/common@0.26.4) (2020-04-07)


### Bug Fixes

* **PipDocuments:** MINTY-3619 - fix PipDocuments not showing all rows ([a175c33](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a175c33))





## [0.26.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.26.2...@zaaksysteem/common@0.26.3) (2020-04-06)

**Note:** Version bump only for package @zaaksysteem/common





## [0.26.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.26.1...@zaaksysteem/common@0.26.2) (2020-04-03)


### Bug Fixes

* **PipDocuments:** MINTY-3619 - fix PipDocuments not showing all rows ([67cf987](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/67cf987))





## [0.26.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.26.0...@zaaksysteem/common@0.26.1) (2020-04-01)

**Note:** Version bump only for package @zaaksysteem/common





# [0.26.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.25.0...@zaaksysteem/common@0.26.0) (2020-04-01)


### Features

* **ObjectTypeManagement:** Implement Dialog with warning of pending changes that will be lost when leaving the form ([d6ef897](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/d6ef897))





# [0.25.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.24.0...@zaaksysteem/common@0.25.0) (2020-03-31)


### Features

* **CheckboxGroup:** MINTY-3473 Add CheckboxGroup component ([61a250a](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/61a250a))





# [0.24.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.23.1...@zaaksysteem/common@0.24.0) (2020-03-30)


### Features

* **ObjectTypeManagement:** MINTY-3469 Create first page of ObjectTypeManagement wizard ([d1b8822](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/d1b8822))





## [0.23.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.23.0...@zaaksysteem/common@0.23.1) (2020-03-30)

**Note:** Version bump only for package @zaaksysteem/common





# [0.23.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.22.0...@zaaksysteem/common@0.23.0) (2020-03-30)


### Features

* **FormRenderer:** Improve performance and readability of `FormRenderer` component ([f310425](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/f310425))
* **FormRenderer:** MINTY-3486 Clean up form definition types ([3f88403](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/3f88403))
* **FormRenderer:** Refactor formRenderer to hook and update Formik to latest version 2.1.4 ([c5d1e71](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c5d1e71))
* **SteppedForm:** MINTY-3486 Add type for stepped form and account for it in all helper utilities ([d3ff7ac](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/d3ff7ac))
* **SteppedForm:** MINTY-3486 Implement stepped form ([6c5c6e3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6c5c6e3))





# [0.22.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.21.0...@zaaksysteem/common@0.22.0) (2020-03-19)


### Features

* **DocumentExplorer:** MINTY-3300 - add initial PDF preview features, misc. ([a131814](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a131814))
* **DocumentExplorer:** MINTY-3300 - change Loader, exports ([b25d770](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/b25d770))
* **DocumentExplorer:** MINTY-3300 - split up DocumentExplorer functionality, create new Intake entrypoint, add Selectable state to table ([5bae0d3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5bae0d3))
* **Intake:** MINTY-3437 - add search component ([2bb171b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/2bb171b))





# [0.21.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.20.1...@zaaksysteem/common@0.21.0) (2020-03-17)


### Features

* **CaseRequestor:** MINTY-3297 Use nested form value instead of flat so you have access to the label ([3eccfa3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/3eccfa3))
* **FormDefinition:** MINTY-3297 Allow defining type of form value for a specific field ([8bf9a81](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8bf9a81))
* **MessagePreview:** MINTY-3297 Add GenericFormMessagePreview component to display messages that contain magic strings ([13dce0f](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/13dce0f))





## [0.20.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.20.0...@zaaksysteem/common@0.20.1) (2020-03-17)

**Note:** Version bump only for package @zaaksysteem/common





# [0.20.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.19.3...@zaaksysteem/common@0.20.0) (2020-03-05)


### Bug Fixes

* **CaseDocuments:** apply MR comments ([778fd18](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/778fd18))
* **CaseDocuments:** fix error dialog not showing, search to autocomplete, fix breadcrumbs ([8d76563](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8d76563))


### Features

* **CaseDocuments:** add dynamic or fixed rowheights, description field ([3170792](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/3170792))
* **CaseDocuments:** add misc features ([8845760](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8845760))
* **CaseDocuments:** MINTY-2880 - add default file/folder sorting ([9719e3a](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/9719e3a))
* **CaseDocuments:** MINTY-2880 - improve typing, some misc fixes ([da1ef46](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/da1ef46))
* **CaseDocuments:** MINTY-2882 - enable download link ([09f2ce5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/09f2ce5))
* **CaseDocuments:** MINTY-2884 - add search functionality ([f91aea4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/f91aea4))
* **CaseDocuments:** MINTY-2884: add file icons to FileBrowser based on mime type ([4223ab5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4223ab5))
* **CaseDocuments:** MINTY-2995 - enable creating of documents from file uploads ([2792d26](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/2792d26))
* **CaseDocuments:** MINTY-3043, MINTY-3045: get documents from server, implement breadcrumb, error handling ([9519313](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/9519313))
* **CaseDocuments:** MINTY-3150 - show file status if not accepted ([7425526](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7425526))
* **CaseDocuments:** MINTY-3191 - add misc. UX tweaks, code layout ([11658c1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/11658c1))
* **CaseDocuments:** MINTY-3191 - implement misc. UX/Design changes ([a2c44c3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a2c44c3))
* **DocumentPreview:** MINTY-2960 Add case document preview to dev dashboard ([0f6fb22](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/0f6fb22))
* **DocumentPreview:** MINTY-2960 Add DocumentPreview component ([ef352ac](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ef352ac))
* **DocumentPreview:** MINTY-2960 Add DocumentPreviewModal component which wraps the DocumentPreview component in a modal ([352ae9a](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/352ae9a))
* **DocumentPreview:** MINTY-2960 Exclude iOS devices from native PDF view since it's spotty ([963d5b5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/963d5b5))





## [0.19.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.19.2...@zaaksysteem/common@0.19.3) (2020-03-03)

**Note:** Version bump only for package @zaaksysteem/common





## [0.19.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.19.1...@zaaksysteem/common@0.19.2) (2020-02-24)

**Note:** Version bump only for package @zaaksysteem/common





## [0.19.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.19.0...@zaaksysteem/common@0.19.1) (2020-02-21)

**Note:** Version bump only for package @zaaksysteem/common





# [0.19.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.18.8...@zaaksysteem/common@0.19.0) (2020-02-17)


### Features

* **MultiValueText:** MINTY-3076 Add entered value on blur ([5c098f8](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5c098f8))





## [0.18.8](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.18.7...@zaaksysteem/common@0.18.8) (2020-02-12)

**Note:** Version bump only for package @zaaksysteem/common





## [0.18.7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.18.6...@zaaksysteem/common@0.18.7) (2020-02-12)

**Note:** Version bump only for package @zaaksysteem/common





## [0.18.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.18.5...@zaaksysteem/common@0.18.6) (2020-02-12)

**Note:** Version bump only for package @zaaksysteem/common





## [0.18.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.18.4...@zaaksysteem/common@0.18.5) (2020-02-10)

**Note:** Version bump only for package @zaaksysteem/common





## [0.18.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.18.3...@zaaksysteem/common@0.18.4) (2020-02-07)

**Note:** Version bump only for package @zaaksysteem/common

## [0.18.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.18.2...@zaaksysteem/common@0.18.3) (2020-02-06)

**Note:** Version bump only for package @zaaksysteem/common

## [0.18.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.18.1...@zaaksysteem/common@0.18.2) (2020-02-06)

**Note:** Version bump only for package @zaaksysteem/common

## [0.18.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.18.0...@zaaksysteem/common@0.18.1) (2020-02-06)

**Note:** Version bump only for package @zaaksysteem/common

# [0.18.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.17.1...@zaaksysteem/common@0.18.0) (2020-01-30)

### Features

- **communication:** Add email to label of contact search field ([64d48fb](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/64d48fb))

## [0.17.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.17.0...@zaaksysteem/common@0.17.1) (2020-01-23)

**Note:** Version bump only for package @zaaksysteem/common

# [0.17.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.16.0...@zaaksysteem/common@0.17.0) (2020-01-23)

### Features

- **Communication:** MINTY-2658 Add CaseRequestor form field which fetches te current case requestor ([8af717c](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8af717c))

# [0.16.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.15.0...@zaaksysteem/common@0.16.0) (2020-01-22)

### Bug Fixes

- **CaseDocumentSelect:** Add ConfigType of CaseDocumentSelect ([1c48eb2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/1c48eb2))

### Features

- **CaseDocumentSelect:** Autoprovide case documents instead of searching by keyword ([1cab1eb](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/1cab1eb))
- **MessageForm:** Replace upload ability with ability to select case documents in case context ([e1d1f92](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/e1d1f92))

# [0.15.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.14.0...@zaaksysteem/common@0.15.0) (2020-01-14)

### Features

- **Communication:** MINTY-2704 Add `gemachtigde` option to email recipient selector ([b2a204a](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/b2a204a))

# [0.14.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.13.0...@zaaksysteem/common@0.14.0) (2020-01-10)

### Features

- **Communication:** MINTY-2658 Remove 'aanvrager' when it is a preset case requestor ([8b72fcd](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8b72fcd))
- **Communication:** MINTY-2665 Use `ContactFinder` component to search for email recipients ([c0a4ad6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c0a4ad6))
- **Communication:** MINTY-2692 Add role selection to email recipient form ([2122179](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/2122179))
- **Communication:** MINTY-2693 Add EmailRecipient selection component ([c67d8e9](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c67d8e9))

# [0.13.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.12.0...@zaaksysteem/common@0.13.0) (2020-01-09)

### Bug Fixes

- (CaseManagement) truncate task title ([c5d397b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c5d397b))
- **Forms:** add trim to string validation so spaces are not valid values ([5733b0d](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5733b0d))
- **Tasks:** MINTY-2769 - fix redirect logic in middleware ([1d09b68](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/1d09b68))
- **Tasks:** MINTY-2816 - change get_task_list endpoint to filter ([f127e36](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/f127e36))
- **UI:** fix component imports ([797f9a9](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/797f9a9))

### Features

- **CaseManagement:** add task creator ([939fbbb](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/939fbbb))
- **ContactFinder:** MINTY-2618 - add type filter support for ContactFinder ([d837b4b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/d837b4b))
- **Tasks:** MINTY-1575 - add basic form fields, DatePicker ([07ed5e5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/07ed5e5))
- **Tasks:** MINTY-1575 - add edit, delete, unlock functionality ([8dca70b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8dca70b))
- **Tasks:** MINTY-1575 - fix build/import problems ([23c0700](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/23c0700))
- **Tasks:** MINTY-2695 - process some MR feedback ([8bd89cc](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8bd89cc))
- **Tasks:** MINTY-2695 - rewrite internal logic to immediate responding to user actions ([c0f74c2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c0f74c2))

# [0.12.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.11.4...@zaaksysteem/common@0.12.0) (2020-01-09)

### Bug Fixes

- **Communication:** MINTY-2262 - allow e-mail addresses over multiple lines ([db44093](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/db44093))
- **Thread:** Fix search case API to new version as filter-status ([85aff79](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/85aff79))
- **UI:** Fix aria-labels on snackbar and dialogs ([775a287](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/775a287))

### Features

- **AddThread:** Filter out resolved cases ([7f31864](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7f31864))
- **CaseAttributes:** add Numeric field ([61d9b6e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/61d9b6e))
- **CaseAttributes:** add TextField component ([8da8bb9](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8da8bb9))
- **CaseFinder:** Display the case status of the case results ([5cbfeb0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5cbfeb0))
- **Communication:** MINTY-1798 Add dialog to import message ([b4543aa](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/b4543aa))
- **Communication:** MINTY-1925 Implement delete message action and trigger refresh using redux middleware ([4d15e36](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4d15e36))
- **Communication:** Replace omitStatus with a filter for multiple statuses ([f2f8e95](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/f2f8e95))
- **ConfirmDialog:** MINTY-1925 Add generic `ConfirmDialog` component and hook ([814f2e0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/814f2e0))
- **Forms:** MINTY-2323 - add generic FormDialog component and implement throughout ([7f46465](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7f46465))
- **i18n:** Move common locale namespace to `@zaaksysteem/common` package ([4a36e92](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4a36e92))
- **Thread:** Allow threads to be linked to cases from main view ([44a8a51](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/44a8a51))

## [0.11.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.11.3...@zaaksysteem/common@0.11.4) (2019-12-19)

**Note:** Version bump only for package @zaaksysteem/common

## [0.11.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.11.2...@zaaksysteem/common@0.11.3) (2019-11-21)

**Note:** Version bump only for package @zaaksysteem/common

## [0.11.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.11.1...@zaaksysteem/common@0.11.2) (2019-10-29)

**Note:** Version bump only for package @zaaksysteem/common

## [0.11.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.11.0...@zaaksysteem/common@0.11.1) (2019-10-25)

**Note:** Version bump only for package @zaaksysteem/common

# [0.11.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.10.2...@zaaksysteem/common@0.11.0) (2019-10-17)

### Features

- **Snackbar:** MINTY-1788 Implement snackbar in pip and main app ([dcbe4da](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/dcbe4da))

## [0.10.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.10.1...@zaaksysteem/common@0.10.2) (2019-09-30)

**Note:** Version bump only for package @zaaksysteem/common

## [0.10.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.10.0...@zaaksysteem/common@0.10.1) (2019-09-26)

**Note:** Version bump only for package @zaaksysteem/common

# [0.10.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.9.1...@zaaksysteem/common@0.10.0) (2019-09-09)

### Features

- **Thread:** Implement typescript ([896ada2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/896ada2))

## [0.9.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.9.0...@zaaksysteem/common@0.9.1) (2019-09-06)

**Note:** Version bump only for package @zaaksysteem/common

# [0.9.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.8.0...@zaaksysteem/common@0.9.0) (2019-09-05)

### Features

- MINTY-1620: adjust presets for buttons in dialogs/forms ([81adac4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/81adac4))

# [0.8.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.7.2...@zaaksysteem/common@0.8.0) (2019-08-29)

### Features

- **Alert:** MINTY-1126 Centralize `Alert` component so it can be used in multiple applications ([c44ec56](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c44ec56))
- **Communication:** MINTY-1115 Add `SearchField` to `Theads` ([5ec462a](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5ec462a))
- **Communication:** MINTY-1115 Add direction icon to indicate incoming or outgoing messages ([6b8e4fa](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6b8e4fa))
- **UI:** Upgrade to MUI4 ([ac921f6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ac921f6))

## [0.7.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.7.1...@zaaksysteem/common@0.7.2) (2019-08-27)

**Note:** Version bump only for package @zaaksysteem/common

## [0.7.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.7.0...@zaaksysteem/common@0.7.1) (2019-08-12)

**Note:** Version bump only for package @zaaksysteem/common

# [0.7.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.6.1...@zaaksysteem/common@0.7.0) (2019-08-06)

### Features

- **Admin:** MINTY-1281 Move admin app to mono repo ([f6a1d35](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/f6a1d35))

## [0.6.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.6.0...@zaaksysteem/common@0.6.1) (2019-07-31)

**Note:** Version bump only for package @zaaksysteem/common

# [0.6.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.5.0...@zaaksysteem/common@0.6.0) (2019-07-31)

### Features

- **Communication:** add general Communication setup and Thread list ([7cb92e6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7cb92e6))

# [0.5.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.4.0...@zaaksysteem/common@0.5.0) (2019-07-30)

### Features

- **FormRenderer:** MINTY-1126 Move `FormRenderer` component with validation and translations from apps project to `@zaaksysteem/common` ([b9e0245](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/b9e0245))

# [0.4.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.3.0...@zaaksysteem/common@0.4.0) (2019-07-29)

### Features

- **i18n:** MINTY-1120 Add i18next to setup and define APP_CONTEXT_ROOT variable on build time. ([a3e6a4c](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a3e6a4c))

# [0.3.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.2.0...@zaaksysteem/common@0.3.0) (2019-07-25)

### Features

- **main:** MINTY-1237 Add session state to @zaaksysteem/common and implement in app ([575898f](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/575898f))

# [0.2.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.1.0...@zaaksysteem/common@0.2.0) (2019-07-25)

### Features

- **main:** MINTY-1232 Add `connected-react-router` package to link router to redux store ([a673309](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a673309))

# 0.1.0 (2019-07-18)

### Features

- **Publishing:** MINTY-1120 Implement lerna publish command voor npm packages ([35934cc](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/commit/35934cc))
- **Setup:** MINTY-1120 Add storybook ([1dc9406](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/commit/1dc9406))
- **Setup:** MINTY-1120 Centralize create react app overrides in common package ([3cdc9d7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/commit/3cdc9d7))
- **Setup:** MINTY-1120 Setup monorepo for all apps and packges using lerna and yarn workspaces ([6bd626e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/commit/6bd626e))
