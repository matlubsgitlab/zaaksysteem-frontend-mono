const {
  babelInclude,
  addBabelPlugin,
  getBabelLoader,
  addWebpackAlias,
  useEslintRc,
} = require('customize-cra');
const rewireReactHotLoader = require('react-app-rewire-hot-loader');
// Todo: Remove dependency from common/package.json once storybook has updated to react-dev-utils 10.x
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');

const loadEslintRc = useEslintRc;

const addHotLoaderAlias = (config, env) => {
  if (env === 'production') {
    return config;
  }

  return addWebpackAlias({
    'react-dom': '@hot-loader/react-dom',
  })(config);
};

const setPublicPath = (config, basePath) => {
  config.output.publicPath = basePath;

  return config;
};

const enableHMR = (config, env, basePath) => {
  if (env === 'production') {
    return config;
  }

  const { entry } = rewireReactHotLoader(config, env);

  config.entry = [
    basePath === '/'
      ? 'webpack-dev-server/client'
      : `webpack-dev-server/client?sockPath=wss://dev.zaaksysteem.nl${basePath}sockjs-node`,
    'react-hot-loader/patch',
    'webpack/hot/dev-server',
  ].concat(entry.filter(item => item.indexOf('webpackHotDevClient') === -1));

  return config;
};

const defineVariables = (config, basePath) => {
  const definePlugin = config.plugins.find(
    plugin => typeof plugin.definitions !== 'undefined'
  );
  definePlugin.definitions['process.env'] = {
    ...definePlugin.definitions['process.env'],
    APP_CONTEXT_ROOT: `'${basePath.slice(0, -1)}'`,
    WEBPACK_BUILD_TARGET: `'${process.env.WEBPACK_BUILD_TARGET}'`,
  };

  return config;
};

const addRemovePropertiesPlugin = config => {
  if (process.env.WEBPACK_BUILD_TARGET !== 'development') {
    const plugin = [
      'react-remove-properties',
      {
        properties: ['scope', 'data-scope'],
      },
    ];
    return addBabelPlugin(plugin)(config);
  }

  return config;
};

const addOptionalChainingPlugin = config => {
  const plugin = '@babel/plugin-proposal-optional-chaining';
  return addBabelPlugin(plugin)(config);
};

const patchForkTSChecker = (config, env) => {
  config.plugins = config.plugins.filter(
    plugin => typeof plugin.tsconfig === 'undefined'
  );
  config.plugins.push(
    new ForkTsCheckerWebpackPlugin({
      async: env !== 'production',
      useTypescriptIncrementalApi: true,
    })
  );

  return config;
};

const concatChunks = (config, env) => {
  if (env === 'development') {
    const webpack = require('webpack');
    config.optimization.splitChunks = {
      cacheGroups: {
        vendors: false,
        main: false,
        default: false,
      },
    };

    config.optimization.runtimeChunk = false;
    config.plugins.push(
      new webpack.optimize.LimitChunkCountPlugin({
        maxChunks: 2,
      })
    );
  }

  return config;
};

const removeRequireEnsureWarning = config => {
  config.module.rules = config.module.rules.filter(rule =>
    rule.parser && rule.parser.requireEnsure === false ? false : true
  );

  return config;
};

/* eslint-disable no-param-reassign */
module.exports = function createReactAppOverrides(basePath) {
  return {
    webpack(config, env) {
      const { include } = getBabelLoader(config);

      config = addHotLoaderAlias(config);
      config = babelInclude([include, /packages/])(config);
      config = enableHMR(config, env, basePath);
      config = setPublicPath(config, basePath);
      config = loadEslintRc('../../.eslintrc.js')(config);
      config = defineVariables(config, basePath);
      config = addRemovePropertiesPlugin(config, env);
      config = addOptionalChainingPlugin(config, env);
      config = patchForkTSChecker(config, env);
      config = concatChunks(config, env);
      config = removeRequireEnsureWarning(config);

      return config;
    },
    devServer: configFunction => (proxy, allowedHost) => {
      // Create the default config by calling configFunction with the proxy/allowedHost parameters
      const config = configFunction(proxy, allowedHost);
      config.watchOptions.ignored = /(?!\/apps\/|\/packages\/).+\/node_modules\//g;
      // config.publicPath = basePath;
      config.clientLogLevel = 'info';
      config.overlay = true;
      config.headers = {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods':
          'GET, POST, PUT, DELETE, PATCH, OPTIONS',
        'Access-Control-Allow-Headers':
          'X-Requested-With, content-type, Authorization',
      };

      // Return your customised Webpack Development Server config.
      return config;
    },
  };
};
