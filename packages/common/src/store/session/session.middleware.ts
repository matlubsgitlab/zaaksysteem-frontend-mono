import { Middleware } from 'redux';
//@ts-ignore
import { get } from '@mintlab/kitchen-sink/source';
import { LOGIN_PATH, LOGOUT_PATH } from '../../library/auth';
import { getUrl, navigate } from '../../library/url';
import { MiddlewareHelper } from '../../types/MiddlewareHelper';
import { ActionWithPayload } from '../../types/ActionWithPayload';
import { UIRootStateType } from '../ui/ui.reducer';
import { login, LoginActionPayloadType } from './session.actions';
import {
  SESSION_AUTH_LOGIN,
  SESSION_AUTH_LOGOUT,
  SESSION_FETCH,
} from './session.constants';

const handleSessionFetchSuccess: MiddlewareHelper<UIRootStateType> = (
  store,
  next,
  action
) => {
  next(action);
  if (!get(store.getState(), 'session.data.logged_in_user')) {
    store.dispatch(login(getUrl()));
  }
};

const handleLogin: MiddlewareHelper<
  UIRootStateType,
  ActionWithPayload<LoginActionPayloadType>
> = (store, next, action) => {
  next(action);
  navigate(`${LOGIN_PATH}?referer=${action.payload}`);
};

const handleLogout: MiddlewareHelper<UIRootStateType> = (
  store,
  next,
  action
) => {
  next(action);
  navigate(LOGOUT_PATH);
};

export const sessionMiddleware: Middleware = store => next => action => {
  switch (action.type) {
    case SESSION_FETCH.SUCCESS:
      return handleSessionFetchSuccess(store, next, action);
    case SESSION_AUTH_LOGIN:
      return handleLogin(store, next, action);
    case SESSION_AUTH_LOGOUT:
      return handleLogout(store, next, action);
    default:
      return next(action);
  }
};

export default sessionMiddleware;
