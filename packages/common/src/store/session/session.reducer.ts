import { AnyAction } from 'redux';
//@ts-ignore
import { get } from '@mintlab/kitchen-sink/source';
import { AJAX_STATE_INIT } from '../../library/redux/ajax/createAjaxConstants';
import {
  handleAjaxStateChange,
  WithAjaxState,
} from '../../library/redux/ajax/handleAjaxStateChange';
import { AjaxAction } from '../../library/redux/ajax/createAjaxAction';
import { SESSION_FETCH } from './session.constants';

type SessionData = {
  logged_in_user: {
    display_name: string;
    id: string;
  };
};

export interface SessionStateType extends WithAjaxState {
  data?: SessionData;
}

export interface SessionRootStateType {
  session: SessionStateType;
}

const initialState: SessionStateType = {
  state: AJAX_STATE_INIT,
};

/**
 * @param {Object} state
 * @param {Object} action
 * @return {Object}
 */
const fetchSuccess = (
  state: SessionStateType,
  action: AjaxAction<{ [key: string]: any }>
): SessionStateType => {
  const data = get<any, SessionData & { [key: string]: any }, undefined>(
    action,
    'payload.response.result.instance'
  );
  return {
    ...state,
    data: data ? data : undefined,
  };
};

/**
 * @param {Object} state
 * @param {Object} action
 * @return {Object}
 */
export function session(
  state: SessionStateType = initialState,
  action: AnyAction
): SessionStateType {
  const handleAjaxState = handleAjaxStateChange(SESSION_FETCH);

  switch (action.type) {
    case SESSION_FETCH.SUCCESS:
      return fetchSuccess(
        handleAjaxState<SessionStateType>(
          state,
          action as AjaxAction<{
            [key: string]: any;
          }>
        ),
        action as AjaxAction<{ [key: string]: any }>
      );
    default:
      return state;
  }
}
