import { SHOW_DIALOG, HIDE_DIALOG } from './dialog.constants';
import { DialogAction, DialogActionPayload } from './dialog.actions';

export type DialogStateType = DialogActionPayload[];

const initialState: DialogStateType = [];

export default function ui(
  state: DialogStateType = initialState,
  action: DialogAction
): DialogStateType {
  const { payload } = action;

  switch (action.type) {
    case SHOW_DIALOG:
      return [...state, payload];

    case HIDE_DIALOG:
      return state.filter((dialog, index) => {
        if (payload && payload.dialogType) {
          return payload.dialogType !== dialog.dialogType;
        }

        return index !== state.length - 1;
      });

    default:
      return state;
  }
}
