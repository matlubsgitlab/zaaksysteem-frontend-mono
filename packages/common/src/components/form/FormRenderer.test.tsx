import React from 'react';
//@ts-ignore
import { mount } from 'enzyme';
import { hasValue } from '../../../../../apps/admin/src/library/value';
import FormRenderer from './FormRenderer';
import {
  FormDefinition,
  FormRendererRenderProps,
} from './types/formDefinition.types';
import * as fieldTypes from './constants/fieldTypes';

import { setFieldValue, Rule, hideFields, showFields } from './rules';

jest.mock('i18next', () => {});
jest.mock('react-i18next', () => ({
  useTranslation: () => [
    jest.fn().mockReturnValue({}),
    { addResourceBundle: jest.fn() },
  ],
}));

const formDefinition: FormDefinition<{
  trigger: string;
  value_result: string;
  field_result: string;
}> = [
  {
    name: 'trigger',
    type: fieldTypes.TEXT,
    format: 'text',
    value: '',
    required: false,
  },
  {
    name: 'value_result',
    type: fieldTypes.TEXT,
    format: 'text',
    value: '',
    required: false,
  },
  {
    name: 'field_result',
    type: fieldTypes.TEXT,
    format: 'text',
    value: '',
    required: false,
  },
];

const rules = [
  new Rule('trigger')
    .when(hasValue)
    .then(hideFields(['field_result']))
    .and(setFieldValue('value_result', 'Filled by rule engine'))
    .else(showFields(['field_result']))
    .and(setFieldValue('value_result', '')),
];

const flattenRenderProps = ({ fields }: FormRendererRenderProps) => {
  return fields.map(({ value, hidden, required, name }) => ({
    name,
    value,
    required,
    hidden,
  }));
};

describe('FormRenderer', () => {
  describe('Initial render', () => {
    test('should match snapshot', () => {
      mount(
        <FormRenderer formDefinition={formDefinition}>
          {(props: FormRendererRenderProps) => {
            const result = flattenRenderProps(props);
            expect(result).toMatchSnapshot('Initial render');

            return <div />;
          }}
        </FormRenderer>
      );
    });
  });

  describe('Triggering the rule engine', () => {
    test('should result in updated values and definition', () => {
      expect.assertions(2);
      mount(
        <FormRenderer
          formDefinition={formDefinition.map(field => {
            const value = field.name === 'trigger' ? 'test' : '';
            return {
              ...field,
              value,
            };
          })}
          rules={rules}
        >
          {(props: FormRendererRenderProps) => {
            expect(flattenRenderProps(props)).toMatchSnapshot();
            expect(props.fields).toHaveLength(2);
            return <div />;
          }}
        </FormRenderer>
      );
    });
  });
});
