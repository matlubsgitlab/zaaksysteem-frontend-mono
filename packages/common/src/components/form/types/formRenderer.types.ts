import { ComponentType } from 'react';
import { FormikProps, FormikTouched, FormikConfig } from 'formik';
import { Rule } from '../rules';
import { ValidationRule } from '../validation/createValidationRule';
import { FormRendererFormField } from './fieldComponent.types';
import { FormValuesType } from './generic.types';
import { FormDefinition } from './formDefinition.types';

export type FormRendererRenderProps<Values = any> = {
  fields: FormRendererFormField[];
} & FormikProps<FormValuesType<Values>>;

export type UseFormType<Values> = {
  formDefinition: FormDefinition<Values>;
  rules?: Rule[];
  isInitialValid?: boolean;
  enableReinitialize?: boolean;
  fieldComponents?: {
    [key: string]: ComponentType<any>;
  };
  validationMap?: {
    [key: string]: ValidationRule;
  };
  onChange?: (values: FormValuesType<Values>) => void;
  onTouched?: (
    touched: FormikTouched<Values>,
    values: FormValuesType<Values>
  ) => void;
} & Pick<FormikConfig<FormValuesType<Values>>, 'onSubmit'>;
