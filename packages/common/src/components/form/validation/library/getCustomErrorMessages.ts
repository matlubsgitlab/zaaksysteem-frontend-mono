import i18next from 'i18next';
import { AnyFormDefinitionField } from '../../types/formDefinition.types';

const constructPath = (
  field: AnyFormDefinitionField,
  type?: string
): string => {
  const handleArray = (path: string) =>
    field.multiValue ? `${path}.array` : path;
  const handleFormat = (path: string) =>
    field.format ? `${path}.${field.format}` : path;
  const handleType = (path: string) => (type ? `${path}.${type}` : path);

  const [path] = ['custom']
    .map(handleArray)
    .map(handleFormat)
    .map(handleType);

  return path;
};

export function getCustomErrorMessages(
  field: AnyFormDefinitionField,
  types: string[],
  t: i18next.TFunction
): { [key: string]: string | undefined } {
  return types.reduce((acc, type) => {
    const path = constructPath(field, type);
    const message = t(`common:validations.${path}`);

    return {
      ...acc,
      [type]: path === message ? undefined : message,
    };
  }, {});
}
