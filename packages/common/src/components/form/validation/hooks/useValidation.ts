import { useMemo } from 'react';
import * as yup from 'yup';
import { useTranslation } from 'react-i18next';
import {
  FormDefinition,
  FormValuesType,
} from '../../types/formDefinition.types';
import createValidation from '../createValidation';
import { ValidationRule } from '../createValidationRule';
import { flattenValues } from '../library/flattenValues';
import validate from '../validate';

type UseValidationReturnType<Values> = (
  values: FormValuesType<Values>
) => Promise<{ [key: string]: string }>;

export function useValidation<Values = any>(
  formDefinition: FormDefinition<Values>,
  validationMap: {
    [key: string]: ValidationRule;
  }
) {
  const [t] = useTranslation('common');
  const schema = useMemo(() => {
    return createValidation({
      formDefinition: formDefinition,
      validationMap: validationMap || {},
      t,
    });
  }, [formDefinition]);

  const validateForm: UseValidationReturnType<Values> = async values => {
    /*
     * Multi value field error are indexed like fieldName[1], this regexp is use to
     * extract the fieldName.
     */
    const MULTI_VALUE_ERROR = /(.*)\[[0-9]+\]/;

    const flattenedValues = flattenValues<Values>(
      values,
      formDefinition,
      validationMap
    );

    try {
      await validate(schema, flattenedValues);
    } catch (error) {
      const yupErrors = error as yup.ValidationError;
      const errors = yupErrors.inner.reduce<{
        [key: string]: string;
      }>((acc, error) => {
        const { message, path } = error;
        const multiValueResult = MULTI_VALUE_ERROR.exec(path);
        const key =
          multiValueResult && multiValueResult.length
            ? multiValueResult[1]
            : path;
        if (message && !acc[key]) {
          acc[key] = message;
        }
        return acc;
      }, {});

      if (errors) {
        return errors;
      }
    }

    return {};
  };

  return validateForm;
}

export default useValidation;
