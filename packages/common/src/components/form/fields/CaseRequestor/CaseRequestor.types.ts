import { FetchCaseRolesType } from '../../../../library/requests/fetchCaseRoles';
import {
  FormValue,
  FormFieldPropsType,
} from '../../types/formDefinition.types';

export type CaseRequestorConfigType = {
  valueResolver?: (role: FetchCaseRolesType) => CaseRequestorType;
  itemFilter?: (role: FetchCaseRolesType) => boolean;
  errorMessage?: string;
  caseUuid: string;
};

export type CaseRequestorType = {
  value: FormValue;
  label: string | React.ReactElement;
};

export interface CaseRequestorPropsType
  extends FormFieldPropsType<any, CaseRequestorConfigType> {
  classes: { [key: string]: any };
}
