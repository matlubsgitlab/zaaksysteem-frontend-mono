/**
 * @return {JSS}
 */
export const textareaStyleSheet = () => ({
  inputRoot: {
    paddingBottom: '0',
  },
  textarea: {
    resize: 'vertical',
  },
});
