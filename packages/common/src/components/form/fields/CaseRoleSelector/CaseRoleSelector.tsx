import React, { ReactElement } from 'react';
//@ts-ignore
import { DataProvider } from '@mintlab/ui/App/Abstract/DataProvider';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import { Select } from '@mintlab/ui/App/Zaaksysteem/Select';
//@ts-ignore
import MultilineOption from '@mintlab/ui/App/Zaaksysteem/Select/Option/MultilineOption';
//@ts-ignore
import ZsIcon from '@mintlab/ui/App/Zaaksysteem/ZsIcon';
import {
  fetchCaseRoles,
  FetchCaseRolesType,
  createDisplayName,
} from '../../../../library/requests/fetchCaseRoles';
import { FormFieldPropsType } from '../../types/formDefinition.types';

type RoleType = {
  value: string;
  label: string;
  icon: ReactElement;
};

export type CaseRoleSelectorConfigType = {
  valueResolver?: (role: FetchCaseRolesType) => string;
  subLabelResolver?: (role: FetchCaseRolesType) => string;
  itemFilter?: (role: FetchCaseRolesType) => boolean;
  caseUuid: string;
};

const defaultItemFilter = () => true;

const fetchSelectChoices = (
  config?: CaseRoleSelectorConfigType
): Promise<RoleType[] | void> =>
  config
    ? fetchCaseRoles(config.caseUuid)
        .then(response => {
          const items = response
            .filter(config.itemFilter || defaultItemFilter)
            .map<RoleType>(caseRole => {
              return {
                value: config.valueResolver
                  ? config.valueResolver(caseRole)
                  : caseRole.role.id,
                subLabel: config.subLabelResolver
                  ? config.subLabelResolver(caseRole)
                  : caseRole.subject?.attributes?.contact_information.email,
                label: createDisplayName(caseRole),
                icon: (
                  <ZsIcon size="small">
                    {caseRole.subject &&
                    caseRole.subject.type === 'organization'
                      ? 'entityType.organization'
                      : 'entityType.person'}
                  </ZsIcon>
                ),
              };
            })
            .filter(item => Boolean(item.value) && Boolean(item.label));

          return items;
        })
        .catch(() => {})
    : Promise.resolve();

interface CaseRoleSelectorPropsType
  extends FormFieldPropsType<any, CaseRoleSelectorConfigType> {
  classes: { [key: string]: any };
}

const CaseRoleSelector: React.FunctionComponent<CaseRoleSelectorPropsType> = ({
  multiValue,
  config,
  value,
  ...restProps
}) => {
  return (
    <DataProvider
      providerArguments={[config]}
      autoProvide={true}
      provider={fetchSelectChoices}
    >
      {({ data, busy }: { data: RoleType[] | null; busy: boolean }) => {
        return (
          <Select
            {...restProps}
            value={value as ValueType<any>[]}
            choices={data || []}
            isClearable={true}
            loading={busy}
            isMulti={multiValue}
            components={
              {
                Option: MultilineOption,
              } as any
            }
            filterOption={() => true}
          />
        );
      }}
    </DataProvider>
  );
};

export default CaseRoleSelector;
