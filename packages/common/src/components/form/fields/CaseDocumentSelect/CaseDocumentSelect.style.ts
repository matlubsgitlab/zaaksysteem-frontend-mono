import { makeStyles } from '@material-ui/core';

export const useCaseDocumentSelectStyle = makeStyles(
  //@ts-ignore
  ({ mintlab: { greyscale, radius } }: any) => ({
    withBackground: {
      borderRadius: radius.defaultFormElement,
      backgroundColor: greyscale.light,
    },
  })
);
