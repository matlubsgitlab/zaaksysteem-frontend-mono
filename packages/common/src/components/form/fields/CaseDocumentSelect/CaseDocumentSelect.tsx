import React from 'react';
import classnames from 'classnames';
//@ts-ignore
import { DataProvider } from '@mintlab/ui/App/Abstract/DataProvider';
import { Select } from '@mintlab/ui/App/Zaaksysteem/Select';
import { FormFieldPropsType } from '../../types/formDefinition.types';
import { searchCaseDocument } from '../../../../library/requests/searchCaseDocument';
import { useCaseDocumentSelectStyle } from './CaseDocumentSelect.style';

export interface CaseDocumentSelectProps extends FormFieldPropsType {}

type CaseDocument = {
  value: string;
  label: string;
};

export type CaseDocumentSelectConfigType = {
  caseUuid: string;
};

const fetchCaseDocument = (caseUuid: string): Promise<CaseDocument[] | void> =>
  searchCaseDocument(caseUuid)
    .then(response =>
      response.data
        .map<CaseDocument>(document => ({
          value: document.relationships.file.data.id,
          label: document.attributes.name,
        }))
        .sort((docA, docB) => docA.label.localeCompare(docB.label))
    )
    .catch(() => {});

/**
 * Renders a CaseDocumentSelect from the mintlab/ui library, based
 * on the provided value(s).
 *
 */

const CaseDocumentSelect: React.ComponentType<CaseDocumentSelectProps> = ({
  multiValue = true,
  config: { caseUuid },
  ...restProps
}) => {
  const classes = useCaseDocumentSelectStyle();

  return (
    <DataProvider
      providerArguments={[caseUuid]}
      autoProvide={true}
      provider={fetchCaseDocument}
    >
      {({ data, busy }: { data: CaseDocument[] | null; busy: boolean }) => {
        return (
          <div
            className={classnames(
              restProps.applyBackgroundColor && classes.withBackground
            )}
          >
            <Select
              {...(restProps as any)}
              choices={data || []}
              isClearable={true}
              loading={busy}
              isMulti={multiValue}
            />
          </div>
        );
      }}
    </DataProvider>
  );
};

export default CaseDocumentSelect;
