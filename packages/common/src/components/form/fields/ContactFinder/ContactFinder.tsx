import React, { useState, ReactElement } from 'react';
//@ts-ignore
import { DataProvider } from '@mintlab/ui/App/Abstract/DataProvider';
import MultilineOption from '@mintlab/ui/App/Zaaksysteem/Select/Option/MultilineOption';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import { Select } from '@mintlab/ui/App/Zaaksysteem/Select';
//@ts-ignore
import ZsIcon from '@mintlab/ui/App/Zaaksysteem/ZsIcon';
import { APICommunication } from '@zaaksysteem/generated';
import {
  searchContact,
  filtersType,
} from '../../../../library/requests/searchContact';
import { FormFieldPropsType } from '../../types/formDefinition.types';

type ContactType = {
  value: string;
  label: string;
  subLabel?: string;
  type: string;
  icon: ReactElement;
};

type ItemResolverType = (
  contact: APICommunication.Contact,
  defaultItem: ContactType
) => ContactType | null;

export type ContactFinderConfigType = {
  itemResolver?: ItemResolverType;
  filterTypes?: filtersType;
};

const defaultConfig: Required<ContactFinderConfigType> = {
  filterTypes: [],
  itemResolver: (contact, defaultItem) => defaultItem,
};

const createDefaultItem = ({
  id,
  type,
  attributes: { name, address, type: contactType },
}: APICommunication.Contact): ContactType => ({
  value: id,
  label: name,
  ...(address ? { subLabel: address } : {}),
  type: type,
  icon: (
    <ZsIcon size="small">
      {contactType === 'organization'
        ? 'entityType.organization'
        : 'entityType.person'}
    </ZsIcon>
  ),
});

const fetchContact = (
  input: string,
  config = defaultConfig as ContactFinderConfigType
): Promise<ContactType[] | void> =>
  searchContact(input, config.filterTypes)
    .then(response =>
      response.data
        .map<ContactType | null>(contact => {
          const item = createDefaultItem(contact);
          return config.itemResolver
            ? config.itemResolver(contact, item)
            : item;
        })
        .filter((item): item is ContactType => Boolean(item))
    )
    .catch(() => {});

interface ContactFinderProps
  extends FormFieldPropsType<any, ContactFinderConfigType> {
  classes: { [key: string]: any };
  choices: {
    label: string;
    value: any;
    [key: string]: any;
  }[];
}

const ContactFinder: React.FunctionComponent<ContactFinderProps> = ({
  multiValue,
  config,
  value,
  ...restProps
}) => {
  const [input, setInput] = useState('');
  const { choices } = restProps;

  return (
    <DataProvider
      providerArguments={[input, config]}
      autoProvide={input !== ''}
      provider={fetchContact}
    >
      {({ data, busy }: { data: ContactType[] | null; busy: boolean }) => {
        const normalizedChoices = data || choices;

        return (
          <Select
            {...restProps}
            value={value as ValueType<any>[]}
            choices={normalizedChoices}
            isClearable={true}
            loading={busy}
            isMulti={multiValue}
            getChoices={setInput}
            components={{
              Option: MultilineOption,
            }}
            filterOption={() => true}
          />
        );
      }}
    </DataProvider>
  );
};

export default ContactFinder;
