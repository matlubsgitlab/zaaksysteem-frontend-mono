import { TextField } from '@mintlab/ui/App/Material/TextField';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import Checkbox from '@mintlab/ui/App/Material/Checkbox';
import ChoiceChips from '@mintlab/ui/App/Zaaksysteem/ChoiceChips';
import { LoadableDatePicker } from '@mintlab/ui/App/Material/DatePicker';
import {
  TEXT,
  SELECT,
  CHECKBOX,
  CHECKBOX_GROUP,
  FILE_SELECT,
  CASE_DOCUMENT_SELECT,
  FLATVALUE_SELECT,
  EMAIL,
  TEXTAREA,
  CHOICE_CHIP,
  MULTI_VALUE_TEXT,
  CASE_FINDER,
  DATEPICKER,
  CONTACT_FINDER,
  CASE_ROLE_SELECTOR,
  CASE_REQUESTOR,
} from '../constants/fieldTypes';
import FlatValueSelect from './FlatValueSelect';
import Textarea from './Textarea';
import MultiValueText from './MultiValueText';
import FileSelect from './FileSelect/FileSelect';
import CaseDocumentSelect from './CaseDocumentSelect/CaseDocumentSelect';
import CaseFinder from './CaseFinder';
import ContactFinder from './ContactFinder/ContactFinder';
import CaseRoleSelector from './CaseRoleSelector/CaseRoleSelector';
import CaseRequestor from './CaseRequestor/CaseRequestor';
import CheckboxGroup from './CheckboxGroup/CheckboxGroup';

export const FormFields = {
  [TEXT]: TextField,
  [MULTI_VALUE_TEXT]: MultiValueText,
  [SELECT]: Select,
  [FILE_SELECT]: FileSelect,
  [CASE_DOCUMENT_SELECT]: CaseDocumentSelect,
  [FLATVALUE_SELECT]: FlatValueSelect,
  [CHECKBOX]: Checkbox,
  [CHECKBOX_GROUP]: CheckboxGroup,
  [EMAIL]: TextField,
  [SELECT]: Select,
  [TEXT]: TextField,
  [TEXTAREA]: Textarea,
  [CHOICE_CHIP]: ChoiceChips,
  [CASE_FINDER]: CaseFinder,
  [DATEPICKER]: LoadableDatePicker,
  [CONTACT_FINDER]: ContactFinder,
  [CASE_ROLE_SELECTOR]: CaseRoleSelector,
  [CASE_REQUESTOR]: CaseRequestor,
};
