import { makeStyles } from '@material-ui/core';

export const useFileSelectStyle = makeStyles(
  //@ts-ignore
  ({ mintlab: { greyscale } }) => ({
    list: {
      marginBottom: 20,
    },
    dialogRoot: {
      width: 500,
    },
    buttonRoot: {
      '&>*:first-child >*:first-child': {
        color: greyscale.darker,
      },
    },
  })
);
