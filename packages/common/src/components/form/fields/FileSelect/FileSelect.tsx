import React, { Fragment, useState } from 'react';
//@ts-ignore
import { FileList, File } from '@mintlab/ui/App/Zaaksysteem/FileSelect';
//@ts-ignore
import Button from '@mintlab/ui/App/Material/Button';
import { useTranslation } from 'react-i18next';
//@ts-ignore
import { cloneWithout, asArray } from '@mintlab/kitchen-sink/source';
import { FormFieldPropsType } from '../../types/formDefinition.types';
import { STATUS_PENDING } from '../../constants/fileStatus';
import FileSelectUpload from './library/FileSelectUpload';
import FileSelectDialog from './library/FileSelectDialog';
import { useFileSelectStyle } from './FileSelect.style';
import { FileObject } from './types/FileSelect.types';

export interface FileSelectProps extends FormFieldPropsType {
  value: any;
}

/**
 * Renders a FileSelect from the mintlab/ui library, based
 * on the provided value(s).
 *
 * Handles adding and removing files by calling onChange when needed.
 *
 * If the uploadDialog prop is false, the FileSelect will
 * render the upload interface, with the current files,
 * directly.
 * If the uploadDialog prop is true, a button will be rendered,
 * which will open a dialog containing the upload interface.
 *
 * Internally, the list of files will be always be handled as an array.
 * If there are no files, null is set as value.
 * If multiValue is true, an array of file objects will be set as value.
 * If multivalue is false, a single file object will be set as value.
 */

const FileSelect: React.ComponentType<FileSelectProps> = ({
  accept = null,
  value = null,
  uploadDialog = false,
  multiValue = true,
  name,
  onChange,
  setFieldTouched,
}) => {
  const normalizedValue = value ? asArray(value) : [];
  const classes = useFileSelectStyle();
  const [t] = useTranslation('formRenderer');
  const [dialogOpen, setDialogOpen] = useState(false);

  const generateList = () => {
    if (!normalizedValue || !normalizedValue.length) return null;

    const mapFiles = (file: FileObject) => {
      const status = () =>
        file.status
          ? {
              status: file.status,
            }
          : {};

      return (
        <File
          key={file.key}
          name={file.name}
          {...status()}
          onDeleteClick={() => removeFile(file.key)}
        />
      );
    };

    return <FileList>{normalizedValue.map(mapFiles)}</FileList>;
  };

  const list = generateList();

  const uploadDialogContent = () => {
    return (
      <Fragment>
        <FileSelectDialog
          open={dialogOpen}
          onClose={() => toggleDialog()}
          onConfirm={(files: FileObject[]) => addFiles(files)}
          classes={{
            paper: classes.dialogRoot,
          }}
          accept={accept}
          multiValue={multiValue}
        />
        <div>
          {normalizedValue && normalizedValue.length > 0 && (
            <div className={classes.list}>{list}</div>
          )}

          <Button
            action={() => toggleDialog()}
            icon={'attachment'}
            iconSize={'small'}
            presets={['small', 'semiContained']}
            classes={{
              contained: classes.buttonRoot,
            }}
          >
            {t('fileSelect.addFile')}
          </Button>
        </div>
      </Fragment>
    );
  };

  const toggleDialog = () => setDialogOpen(!dialogOpen);

  const addFiles = (files: FileObject[]) =>
    handleOnChange([...normalizedValue, ...asArray(files)]);

  const removeFile = (key: string) =>
    handleOnChange(normalizedValue.filter(thisValue => thisValue.key !== key));

  const setFileStatus = (key: string, props: Partial<FileObject>) => {
    const mapValue = (thisFile: FileObject) => {
      if (thisFile.key === key) {
        return {
          ...cloneWithout(thisFile, 'status'),
          ...props,
        };
      } else {
        return thisFile;
      }
    };
    handleOnChange(normalizedValue.map(mapValue));
  };

  const handleOnChange = (changeValue: FileObject[]) => {
    const filesPending = changeValue.some(
      file => file.status === STATUS_PENDING
    );
    const getValue = () => {
      if (!changeValue.length) return null;
      return multiValue ? changeValue : changeValue[0];
    };

    onChange({
      target: {
        name,
        value: getValue(),
      },
    } as React.ChangeEvent<any>);

    /*
     * Set the 'touched' property of this field according to
     * whether there are any pending files, so the form will not
     * flash any error messages.
     */
    setFieldTouched(name, !filesPending);
  };

  if (uploadDialog) {
    return uploadDialogContent();
  } else {
    return (
      <FileSelectUpload
        accept={accept}
        selectInstructions={t('fileSelect.selectInstructions')}
        dragInstructions={t('fileSelect.dragInstructions')}
        dropInstructions={t('fileSelect.dropInstructions')}
        orLabel={t('fileSelect.orLabel')}
        value={normalizedValue}
        multiValue={multiValue}
        list={list}
        addFiles={addFiles}
        setFileStatus={setFileStatus}
      />
    );
  }
};

export default FileSelect;
