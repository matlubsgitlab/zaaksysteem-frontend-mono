import React from 'react';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import { FormFieldPropsType } from '../types/formDefinition.types';

export interface MultiValueTextPropsType extends FormFieldPropsType {
  createOnBlur?: boolean;
}

/*
 * Wraps Select and configures it as multi value textfield
 */
const MultiValueText: React.ComponentType<MultiValueTextPropsType> = props => {
  const { multiValue, createOnBlur = true, value, ...rest } = props;

  return (
    <Select
      {...rest}
      value={value as ValueType<any>[]}
      isMulti={multiValue}
      isClearable={true}
      createOnBlur={createOnBlur}
      creatable={true}
      formatCreateLabel={(input: string) => input}
    />
  );
};

export default MultiValueText;
