import React from 'react';
//@ts-ignore
import Checkbox from '@mintlab/ui/App/Material/Checkbox';
import { FormFieldPropsType } from '../../types';
import { useCheckboxGroupStyles } from './CheckboxGroup.style';

type CheckboxValue = string | number;

export type CheckboxGroupPropsType = FormFieldPropsType<
  any,
  any,
  CheckboxValue[]
>;

export const CheckboxGroup: React.ComponentType<CheckboxGroupPropsType> = ({
  value,
  name,
  onChange,
  choices,
}) => {
  const classes = useCheckboxGroupStyles();
  const updateValue = (value: CheckboxValue[] | null) =>
    onChange({
      target: {
        name,
        value,
      },
    } as React.ChangeEvent<any>);

  const selectCheckbox = (checkboxValue: CheckboxValue) =>
    updateValue([...(value || []), checkboxValue]);

  const deselectCheckbox = (checkboxValue: CheckboxValue) =>
    updateValue(value.filter(item => item != checkboxValue));

  const isCheckboxSelected = (checkboxValue: CheckboxValue) =>
    (value || []).includes(checkboxValue);

  const getValueByName = (name: string): CheckboxValue =>
    choices
      ? choices.find(({ value }) => name === value.toString()).value
      : null;

  const handleCheckboxChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { name } = event.target;
    const value = getValueByName(name);

    return isCheckboxSelected(value)
      ? deselectCheckbox(value)
      : selectCheckbox(value);
  };

  return (
    <ul className={classes.list}>
      {choices &&
        choices.map(({ label, value }) => (
          <li key={value}>
            <Checkbox
              label={label}
              name={value.toString()}
              onChange={handleCheckboxChange}
              checked={isCheckboxSelected(value)}
            />
          </li>
        ))}
    </ul>
  );
};

export default CheckboxGroup;
