import { makeStyles } from '@material-ui/core';

export const useCheckboxGroupStyles = makeStyles({
  list: {
    margin: 0,
    padding: 0,
    listStyle: 'none',
  },
});
