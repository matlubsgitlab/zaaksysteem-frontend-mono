import {
  hasValue,
  valueBiggerThan,
  valueSmallerThan,
  valueEquals,
  valueOneOf,
} from './conditions';

describe('Rule engine', () => {
  describe('conditions', () => {
    describe('hasValue', () => {
      test('should return true when given a value', () => {
        expect(hasValue('test')).toEqual(true);
        expect(hasValue(0)).toEqual(true);
        expect(hasValue({ value: 'test' })).toEqual(true);
        expect(hasValue({ value: 0 })).toEqual(true);
      });
      test('should return false when not given a value', () => {
        expect(hasValue('')).toEqual(false);
        expect(hasValue({ value: '' })).toEqual(false);
        expect(hasValue({} as any)).toEqual(false);
      });
    });

    describe('valueBiggerThan', () => {
      const valueBiggerThan5 = valueBiggerThan(5);

      test('should return true when given value is bigger', () => {
        expect(valueBiggerThan5(10)).toEqual(true);
      });
      test('should return false when given value is not bigger', () => {
        expect(valueBiggerThan5(4)).toEqual(false);
      });
    });

    describe('valueBiggerThan', () => {
      const valueBiggerThan5 = valueBiggerThan(5);

      test('should return true when given value is bigger', () => {
        expect(valueBiggerThan5(10)).toEqual(true);
      });
      test('should return false when given value is not bigger', () => {
        expect(valueBiggerThan5(4)).toEqual(false);
      });
    });

    describe('valueSmallerThan', () => {
      const valueSmallerThan5 = valueSmallerThan(5);

      test('should return true when given value is smaller', () => {
        expect(valueSmallerThan5(4)).toEqual(true);
      });
      test('should return false when given value is not smaller', () => {
        expect(valueSmallerThan5(10)).toEqual(false);
      });
    });

    describe('valueEquals', () => {
      const valueEqualsFoo = valueEquals('Foo');

      test('should return true when given value is equal', () => {
        expect(valueEqualsFoo('Foo')).toEqual(true);
        expect(valueEqualsFoo({ value: 'Foo' })).toEqual(true);
      });
      test('should return false when given value is not equal', () => {
        expect(valueEqualsFoo('Bar')).toEqual(false);
        expect(valueEqualsFoo({ value: 'Bar' })).toEqual(false);
      });
    });

    describe('valueOneOf', () => {
      const oneOf = valueOneOf(['Foo', 'Test']);

      test('should return true when given value is equal', () => {
        expect(oneOf('Foo')).toEqual(true);
        expect(oneOf({ value: 'Foo' })).toEqual(true);
      });
      test('should return false when given value is not equal', () => {
        expect(oneOf('Bar')).toEqual(false);
        expect(oneOf({ value: 'Bar' })).toEqual(false);
      });
    });
  });
});
