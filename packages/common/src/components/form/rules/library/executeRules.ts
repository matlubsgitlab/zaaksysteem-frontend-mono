import { FormDefinition } from '../../types/formDefinition.types';
import Rule from './Rule';

export function executeRules<Values = any>(
  rules: Rule[],
  formDefinition: FormDefinition<Values>
): FormDefinition<Values> {
  return rules.reduce(
    (accumulator, rule) => rule.execute(accumulator),
    formDefinition
  );
}

export default executeRules;
