import { AnyFormDefinitionField } from '../../types';
import Rule from './Rule';
import { hasValue, valueEquals } from './conditions';
import { setRequired, hideFields } from './actions';

describe('Rule engine', () => {
  describe('Rule creation', () => {
    const getTestData = (): AnyFormDefinitionField<any>[] => [
      {
        name: 'test',
        format: 'text',
        type: 'text',
        value: 'test value',
      },
      {
        name: 'result',
        format: 'text',
        type: 'text',
        value: '',
      },
    ];

    test('Can construct simple rule', () => {
      const rule = new Rule('test')
        .when(hasValue)
        .then(setRequired(['result']));

      const [, result] = rule.execute(getTestData());
      expect(result.required).toEqual(true);
    });

    test('Rule can have a `else` action', () => {
      const rule = new Rule('test')
        .when(valueEquals('something'))
        .then(setRequired(['result']))
        .else(hideFields(['result']));

      const [, result] = rule.execute(getTestData());
      expect(result.required).toBeFalsy();
      expect(result.hidden).toEqual(true);
    });

    test('Rule can have multiple `then` actions', () => {
      const rule = new Rule('test')
        .when(hasValue)
        .then(setRequired(['result']))
        .and(hideFields(['result']));

      const [, result] = rule.execute(getTestData());
      expect(result.required).toEqual(true);
      expect(result.hidden).toEqual(true);
    });

    test('Rule can have multiple `else` actions', () => {
      const rule = new Rule('test')
        .when(valueEquals('something'))
        .else(setRequired(['result']))
        .and(hideFields(['result']));

      const [, result] = rule.execute(getTestData());
      expect(result.required).toEqual(true);
      expect(result.hidden).toEqual(true);
    });
  });
});
