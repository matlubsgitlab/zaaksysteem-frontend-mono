import {
  FormDefinition,
  PartialFormValuesType,
  AnyFormDefinitionField,
} from '../types/formDefinition.types';
import { isSteppedForm } from './formHelpers';

function mapValuesToFields<Values>(
  values: PartialFormValuesType<Values>,
  fields: AnyFormDefinitionField<Values>[]
) {
  return fields.map(item => {
    if (typeof values[item.name] !== 'undefined') {
      return {
        ...item,
        value: values[item.name],
      };
    }

    return item;
  });
}

export default function mapValuesToFormDefinition<Values>(
  values: PartialFormValuesType<Values>,
  formDefinition: FormDefinition<Values>
): FormDefinition<Values> {
  return isSteppedForm(formDefinition)
    ? formDefinition.map(step => ({
        ...step,
        fields: mapValuesToFields(values, step.fields),
      }))
    : mapValuesToFields(values, formDefinition);
}
