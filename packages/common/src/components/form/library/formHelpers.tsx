import React from 'react';
import { FormikProps, FormikErrors, FormikTouched } from 'formik';
import {
  FormRendererFormField,
  FormRendererFieldComponentPropsType,
  FormValuesType,
  FormDefinitionRefValue,
  FormDefinitionRefValueResolved,
  NestedFormValue,
  FormValue,
  FormDefinition,
  MultiStepFormDefinition,
  AnyFormDefinitionField,
} from '../types/formDefinition.types';

export function createField<Values>(fields: FormRendererFormField<Values>[]) {
  const Field: React.ComponentType<FormRendererFieldComponentPropsType<
    Values
  >> = ({ name, error }) => {
    const [{ FieldComponent, ...restProps }] = fields.filter(
      item => item.name === name
    );
    return (
      <React.Fragment>
        <FieldComponent {...restProps} error={error} />
      </React.Fragment>
    );
  };
  return Field;
}

export function getField<Values = any>(
  fields: FormRendererFormField<Values>[]
) {
  return (
    fieldName: keyof Values
  ): FormRendererFormField<Values> | undefined => {
    return fields.find(field => fieldName === field.name);
  };
}

export function getRefValue<Values = any>(
  values: FormValuesType<Values>,
  refValue: FormDefinitionRefValue<Values>
): FormDefinitionRefValueResolved {
  if (typeof refValue === 'string') {
    return values[refValue];
  }

  return Object.entries(refValue).reduce(
    (acc, [key, value]: [string, keyof Values]) => {
      return {
        ...acc,
        [key]: values[value],
      };
    },
    {}
  );
}

export const setTouchedAndHandleChange = ({
  setFieldTouched,
  handleChange,
}: Pick<FormikProps<any>, 'handleChange' | 'setFieldTouched'>) => (
  event: React.ChangeEvent<any>
) => {
  const {
    target: { name },
  } = event;
  setFieldTouched(name, true);
  handleChange(event);
};

export function hasField<Values = any>(
  fields: FormRendererFormField<Values>[]
) {
  return (fieldName: keyof Values): boolean => {
    return fields.findIndex(field => fieldName === field.name) > -1;
  };
}

const flattenSingleValueField = (
  value: FormValue,
  nestedKey: string = 'value'
) =>
  (value as NestedFormValue)[nestedKey]
    ? (value as NestedFormValue)[nestedKey].toString()
    : value.toString();

export const flattenField = (
  value: FormValue | FormValue[],
  nestedKey: string = 'value'
): string =>
  Array.isArray(value)
    ? value.map(item => flattenSingleValueField(item, nestedKey)).join(', ')
    : flattenSingleValueField(value, nestedKey);

export function isSteppedForm<Values>(
  formDefinition: FormDefinition<Values>
): formDefinition is MultiStepFormDefinition<Values> {
  return (
    typeof (formDefinition as MultiStepFormDefinition<Values>)[0].title !==
    'undefined'
  );
}
export function isStepValid<Values>(
  fields: AnyFormDefinitionField<Values>[],
  errors: FormikErrors<FormValuesType<Values>>
): boolean {
  return fields.every(field => typeof errors[field.name] === 'undefined');
}

export function isStepTouched<Values>(
  fields: AnyFormDefinitionField<Values>[],
  touched: FormikTouched<FormValuesType<Values>>
): boolean {
  return fields
    .filter(field => field.required)
    .every(field => typeof touched[field.name] !== 'undefined');
}

export function getActiveSteps<Values>(
  formDefinition: MultiStepFormDefinition<Values>
): MultiStepFormDefinition<Values> {
  return formDefinition.filter(
    ({ fields }) => fields.filter(field => !field.hidden).length > 0
  );
}
