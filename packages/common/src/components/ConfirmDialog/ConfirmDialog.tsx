import React from 'react';
import { useTranslation } from 'react-i18next';
// @ts-ignore
import Alert from '../dialogs/Alert/Alert';

export interface ConfirmDialogPropsType {
  title: string;
  body: string;
  onConfirm: () => void;
  onClose?: () => void;
  open?: boolean;
}

const ConfirmDialog: React.FunctionComponent<ConfirmDialogPropsType> = ({
  onConfirm,
  onClose,
  title,
  body,
  open = true,
}) => {
  const [t] = useTranslation('common');

  return (
    <Alert
      primaryButton={{ text: t('dialog.ok'), action: onConfirm }}
      secondaryButton={{ text: t('dialog.cancel'), action: onClose }}
      onClose={onClose}
      title={title}
      open={open}
    >
      {body}
    </Alert>
  );
};

export default ConfirmDialog;
