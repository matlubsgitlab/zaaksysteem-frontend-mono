import React from 'react';
import Modal from '@material-ui/core/Modal/Modal';
import Slide from '@material-ui/core/Slide';
import { DocumentPreview } from './DocumentPreview';
import { DocumentPreviewPropsType } from './DocumentPreview.types';

interface DocumentPreviewModalPropsType extends DocumentPreviewPropsType {
  open: boolean;
}

export const DocumentPreviewModal: React.ComponentType<DocumentPreviewModalPropsType> = ({
  open,
  ...props
}) => {
  return (
    <Modal open={open} closeAfterTransition>
      <Slide direction="up" in={open}>
        <DocumentPreview {...props} />
      </Slide>
    </Modal>
  );
};

export default DocumentPreviewModal;
