import { makeStyles } from '@material-ui/core/styles';

export const useDocumentPreviewHeaderStyles = makeStyles(() => ({
  wrapper: {
    display: 'flex',
    padding: '0 10px',
    height: 60,
    alignItems: 'center',
    backgroundColor: 'white',
  },
  title: {
    flexGrow: 1,
    verticalAlign: 'middle',
  },
}));
