import { makeStyles } from '@material-ui/core/styles';

export const useViewerControlStyles = makeStyles(() => ({
  wrapper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',

    '& > *': {
      margin: 5,
    },
  },
}));
