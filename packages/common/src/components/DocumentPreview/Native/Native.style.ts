import { makeStyles } from '@material-ui/core/styles';

export const useNativeStyles = makeStyles(() => ({
  iframe: {
    position: 'relative',
    width: '100%',
    border: 0,
  },
}));
