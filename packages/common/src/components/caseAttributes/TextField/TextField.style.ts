import { makeStyles } from '@material-ui/core/styles';

export const useTextFieldStyles = makeStyles({
  fieldContainer: {
    display: 'flex',
    alignItems: 'center',
  },
});
