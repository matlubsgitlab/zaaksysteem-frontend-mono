import { makeStyles } from '@material-ui/core/styles';

export const useNumericFieldStyles = makeStyles({
  fieldContainer: {
    display: 'flex',
    alignItems: 'center',
  },
});
