import React, { Fragment, useRef } from 'react';
import { useTranslation } from 'react-i18next';
import { useTheme } from '@material-ui/core';
//@ts-ignore
import Loader from '@mintlab/ui/App/Zaaksysteem/Loader';
//@ts-ignore
import { cloneWithout } from '@mintlab/kitchen-sink/source/object';
//@ts-ignore
import { unique } from '@mintlab/kitchen-sink/source/unique';
//@ts-ignore
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  DialogDivider,
  //@ts-ignore
} from '@mintlab/ui/App/Material/Dialog';
import createDialogActions from '@zaaksysteem/common/src/components/dialogs/library/createDialogActions';
//@ts-ignore
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import FormRenderer, {
  FormRendererProps,
} from '@zaaksysteem/common/src/components/form/FormRenderer';
import {
  FormRendererRenderProps,
  FormDefinition,
  FormValuesType,
} from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { useStyles } from './FormDialog.style';

const getDialogActions = createDialogActions({
  primaryPresets: ['primary', 'text'],
  secondaryPresets: ['default', 'text'],
});

export type FormDialogPropsType<Values> = {
  formDefinition: FormDefinition<Values>;
  onClose: Function;
  title: string;
  onSubmit: (values: FormValuesType<Values>) => void;
  scope?: string;
  saveLabel?: string;
  icon?: string;
  initializing?: boolean;
  saving?: boolean;
  isInitialValid?: boolean;
  open?: boolean;
} & Pick<FormRendererProps, 'validationMap' | 'fieldComponents' | 'rules'>;

function FormDialog<Values>({
  onSubmit,
  formDefinition,
  title,
  scope,
  saveLabel,
  icon,
  initializing = false,
  saving = false,
  validationMap,
  fieldComponents,
  rules,
  isInitialValid = false,
  onClose,
  open = true,
}: FormDialogPropsType<Values>) {
  const dialogEl = useRef(null);
  const [t] = useTranslation('common');
  const theme = useTheme<any>();
  const classes = useStyles(theme);

  return (
    <Dialog
      classes={{ paper: classes.dialogRoot }}
      aria-label={title}
      open={open}
      onClose={onClose}
      ref={dialogEl}
      disableBackdropClick={true}
    >
      <DialogTitle
        elevated={true}
        id={unique()}
        title={title}
        classes={{
          rootElevated: classes.dialogTitle,
        }}
        onCloseClick={onClose}
        scope={scope}
        {...(icon && { icon })}
      />

      {initializing && <Loader />}

      {!initializing && (
        <FormRenderer
          formDefinition={formDefinition}
          isInitialValid={isInitialValid}
          validationMap={validationMap}
          rules={rules}
          fieldComponents={fieldComponents}
        >
          {({ fields, values, isValid }: FormRendererRenderProps<Values>) => {
            const handleSubmit = () => {
              if (!isValid) return;
              onSubmit(values);
            };

            const primaryButton = {
              disabled: saving || !isValid,
              text: saveLabel ? saveLabel : t('dialog.save'),
              action() {
                onSubmit(values);
              },
            };
            const secondaryButton = {
              text: t('forms.cancel'),
              action: onClose,
            };
            const dialogActions = getDialogActions(
              primaryButton,
              secondaryButton,
              scope
            );

            return (
              <Fragment>
                <DialogContent padded={true}>
                  {fields.map(({ FieldComponent, key, type, ...rest }) => {
                    const props = cloneWithout(rest, 'definition', 'mode');

                    return (
                      <FormControlWrapper
                        {...props}
                        compact={true}
                        classes={{
                          wrapper: classes.wrapper,
                        }}
                        key={`${props.name}-formcontrol-wrapper`}
                      >
                        <FieldComponent
                          {...props}
                          t={t}
                          containerRef={dialogEl.current}
                          submit={handleSubmit}
                        />
                      </FormControlWrapper>
                    );
                  })}
                </DialogContent>
                <Fragment>
                  <DialogDivider />
                  <DialogActions>{dialogActions}</DialogActions>
                </Fragment>
              </Fragment>
            );
          }}
        </FormRenderer>
      )}
    </Dialog>
  );
}

export default FormDialog;
