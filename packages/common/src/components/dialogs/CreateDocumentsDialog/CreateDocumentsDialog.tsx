import React, { useState } from 'react';
import { FormikValues } from 'formik';
import { useTranslation } from 'react-i18next';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import translateFormDefinition from '@zaaksysteem/common/src/components/form/library/translateFormDefinition';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { formDefinition } from './CreateDocumentsDialogFormDefinition';
import { createDocuments } from './requests';
import locale from './locale';
import { CreateDocumentsDialogPropsType } from './types/CreateDocumentsDialogTypes';

const CreateDocumentsDialog: React.FunctionComponent<CreateDocumentsDialogPropsType> = ({
  caseUUID,
  directoryUUID,
  onConfirm,
  onClose,
  open,
}) => {
  const [saving, setSaving] = useState(false);
  const [t] = useTranslation();
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();

  const handleOnSubmit = async (formValues: FormikValues) => {
    setSaving(true);

    try {
      await createDocuments({
        values: formValues.files,
        caseUUID,
        directoryUUID,
      });
      onConfirm();
    } catch (errorObj) {
      openServerErrorDialog(errorObj);
    }

    setSaving(false);
  };

  return (
    <I18nResourceBundle resource={locale} namespace="FileLinkerDialog">
      {ServerErrorDialog}
      <FormDialog
        formDefinition={translateFormDefinition(formDefinition, t)}
        onSubmit={handleOnSubmit}
        title={t('FileLinkerDialog:title')}
        scope="FileLinkerDialog"
        icon="insert_drive_file"
        onClose={onClose}
        saveLabel={t('FileLinkerDialog:save')}
        saving={saving}
        open={open}
      />
    </I18nResourceBundle>
  );
};

export default CreateDocumentsDialog;
