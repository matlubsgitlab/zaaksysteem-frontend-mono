const locale = {
  nl: {
    title: 'Voeg bestanden toe',
    save: 'Toevoegen',
    label: 'Upload een of meerdere bestanden',
  },
};

export default locale;
