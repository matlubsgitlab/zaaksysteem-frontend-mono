import { FILE_SELECT } from '@zaaksysteem/common/src/components/form/constants/fieldTypes';

export const formDefinition = [
  {
    name: 'files',
    type: FILE_SELECT,
    value: null,
    required: true,
    label: 'FileLinkerDialog:label',
    placeholder: 'FileLinkerDialog:placeholder',
  },
];
