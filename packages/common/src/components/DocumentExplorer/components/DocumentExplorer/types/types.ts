import { ItemType } from '../../FileExplorer/types/FileExplorerTypes';

export type BreadCrumbItemType = {
  id: string;
  label: string;
};

export type StoreShapeType = {
  items: ItemType[];
  loading: boolean;
  path: BreadCrumbItemType[];
  location: ValidLocation;
  search: string;
  createDocumentsOpen: boolean;
};

export enum PresetLocations {
  Home = 'HOME',
  Search = 'SEARCH',
}

export type ValidLocation = PresetLocations | string;

export type ValidLocationOrNull = ValidLocation | null;

export type HandleItemClickType = (
  event: React.MouseEvent,
  item: BreadCrumbItemType
) => void;

export type ClassesType = { [key: string]: any };
