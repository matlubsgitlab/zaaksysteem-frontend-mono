import {
  DocumentItemType,
  ItemType,
} from '../../FileExplorer/types/FileExplorerTypes';

export const isDocument = (
  item: Omit<ItemType, 'modified'>
): item is DocumentItemType => {
  return item.type === 'document';
};
