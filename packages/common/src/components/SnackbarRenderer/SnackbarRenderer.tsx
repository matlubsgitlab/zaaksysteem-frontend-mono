import React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { useTranslation } from 'react-i18next';
//@ts-ignore
import Snackbar from '@mintlab/ui/App/Material/Snackbar';
import useMessages from '../../library/useMessages';
import { UIRootStateType } from '../../store/ui/ui.reducer';
import { hideSnackbar } from '../../store/ui/snackbar/snackbar.actions';

interface PropsFromState {
  messageId: string | null;
}

interface PropsFromDispatch {
  closeAction: () => void;
}

const SnackbarRenderer: React.FunctionComponent<PropsFromState &
  PropsFromDispatch> = ({ messageId, closeAction }) => {
  const [messages] = useMessages();
  const [t] = useTranslation('common');

  return messageId && messages[messageId] ? (
    <Snackbar
      message={messages[messageId]}
      onQueueEmpty={closeAction}
      closeLabel={t('snack.close')}
    />
  ) : null;
};

const mapStateToProps = ({
  ui: {
    snackbar: { messageId },
  },
}: UIRootStateType): PropsFromState => ({
  messageId,
});

const mapDispatchToProps = (dispatch: Dispatch): PropsFromDispatch => ({
  closeAction: () => dispatch(hideSnackbar()),
});

export default connect<PropsFromState, PropsFromDispatch, {}, UIRootStateType>(
  mapStateToProps,
  mapDispatchToProps
)(SnackbarRenderer);
