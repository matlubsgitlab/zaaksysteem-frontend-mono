import i18next from 'i18next';
import fecha from 'fecha';
//@ts-ignore
import { asArray } from '@mintlab/kitchen-sink/source';
import { addResourceBundle } from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import locale from '../locale/common.locale';

function initFetcha(i18n: i18next.i18n) {
  fecha.i18n = {
    ...fecha.i18n,
    ...i18n.t('common:dates', { returnObjects: true }),
  };
}

export async function initI18n(options: i18next.InitOptions, use: any[]) {
  if (use) {
    asArray(use).map(item => i18next.use(item));
  }

  await i18next.init(options);

  addResourceBundle(i18next, 'common', locale);
  initFetcha(i18next);

  return i18next;
}
