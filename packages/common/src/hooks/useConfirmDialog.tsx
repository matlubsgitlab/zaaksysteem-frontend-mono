import React, { useState } from 'react';
import ConfirmDialog from '../components/ConfirmDialog/ConfirmDialog';

export function useConfirmDialog(
  title: string,
  body: string,
  onConfirm: () => void
): [React.ReactChild, () => void] {
  const [isOpen, setOpen] = useState(false);
  const doOpen = () => setOpen(true);
  const doClose = () => setOpen(false);
  const handleConfirm = () => {
    onConfirm();
    doClose();
  };

  return [
    <ConfirmDialog
      key={title}
      title={title}
      body={body}
      onClose={doClose}
      onConfirm={handleConfirm}
      open={isOpen}
    />,
    doOpen,
  ];
}

export default useConfirmDialog;
