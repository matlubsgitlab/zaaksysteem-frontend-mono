import { request } from '@zaaksysteem/common/src/library/fetch';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { APICommunication } from '@zaaksysteem/generated';

type SearchContactRequestParams = APICommunication.SearchContactRequestParams;
type SearchContactResponseBody = APICommunication.SearchContactResponseBody;
export type SearchContactType = APICommunication.Contact;
export type filtersType =
  | undefined
  | ('employee' | 'person' | 'organization')[];

export const searchContact = (
  keyword: SearchContactRequestParams['keyword'],
  filtersType: filtersType
): Promise<SearchContactResponseBody> => {
  return request(
    'GET',
    buildUrl<SearchContactRequestParams>(
      `/api/v2/communication/search_contact`,
      {
        keyword,
        ...(filtersType && { 'filter[type]': filtersType.join(',') }),
      }
    )
  )
    .then(requestPromise => requestPromise.json())
    .catch(response => Promise.reject(response));
};
