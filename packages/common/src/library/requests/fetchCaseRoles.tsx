import { buildUrl } from '@mintlab/kitchen-sink/source';
import { APICaseManagement } from '@zaaksysteem/generated';
import { request } from './../fetch';

type SubjectType =
  | APICaseManagement.Person
  | APICaseManagement.Employee
  | APICaseManagement.Organization;

export type FetchCaseRolesType = {
  role: APICaseManagement.SubjectRelation;
  subject?: SubjectType;
};

export const fetchCaseRoles = (
  caseUuid: string
): Promise<FetchCaseRolesType[]> =>
  request(
    'GET',
    buildUrl<APICaseManagement.GetSubjectRelationsRequestParams>(
      '/api/v2/cm/case/get_subject_relations',
      {
        case_uuid: caseUuid,
        include: ['subject'],
      }
    )
  )
    .then(requestPromise => requestPromise.json())
    .then((response: APICaseManagement.GetSubjectRelationsResponseBody) => {
      return response.data
        .map<FetchCaseRolesType>(role => ({
          role,
          subject:
            response.included &&
            response.included.filter(subject => {
              return (
                role.relationships.subject &&
                subject.id === role.relationships.subject.data.id
              );
            })[0],
        }))
        .sort((roleA, roleB) =>
          roleA.role.attributes.role < roleB.role.attributes.role ? -1 : 1
        );
    })
    .catch(response => Promise.reject(response));

export const isNotPresetRequestor = (item: FetchCaseRolesType) =>
  item.role.attributes.is_preset_client === false;

/* This is a temporary work-arround untill python provides the displayName
 * for a subject entity. See: MINTY-2949 */
export const createDisplayName = ({
  role,
  subject,
}: FetchCaseRolesType): string => {
  if (subject && subject.attributes) {
    if (subject.attributes.surname) {
      const {
        surname,
        surname_prefix,
        first_names,
        first_name,
        noble_title,
      } = subject.attributes;
      const name = [
        noble_title,
        first_names,
        first_name,
        surname_prefix,
        surname,
      ]
        .filter(item => Boolean(item))
        .join(' ');

      return `${name} (${role.attributes.role})`;
    } else if (subject.attributes.name) {
      return `${subject.attributes.name} (${role.attributes.role})`;
    }
  }

  return role.attributes.role;
};

export const createDisplayNameWithEmail = ({
  role,
  subject,
}: FetchCaseRolesType): string => {
  const email = subject?.attributes?.contact_information.email;
  let subjectName: string | null = null;

  if (subject?.attributes?.surname) {
    const {
      surname,
      surname_prefix,
      first_names,
      first_name,
      noble_title,
    } = subject.attributes;
    subjectName = [
      noble_title,
      first_names,
      first_name,
      surname_prefix,
      surname,
    ]
      .filter(item => Boolean(item))
      .join(' ');
  } else if (subject?.attributes) {
    subjectName = subject.attributes.name;
  }

  const mainName = role.attributes.role;
  const formattedSubjectName = subjectName && `(${subjectName})`;
  const formattedEmail = email && `[${email}]`;

  return [mainName, formattedSubjectName, formattedEmail]
    .filter(Boolean)
    .join(' ');
};
