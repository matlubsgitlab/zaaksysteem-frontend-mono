import { request } from '@zaaksysteem/common/src/library/fetch';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { APIDocument } from '@zaaksysteem/generated';

export const searchCaseDocument = (
  caseUuid: string
): Promise<APIDocument.SearchDocumentResponseBody> => {
  return request(
    'GET',
    buildUrl<APIDocument.SearchDocumentRequestParams>(
      `/api/v2/documents/search_document`,
      {
        case_uuid: caseUuid,
      }
    )
  )
    .then(requestPromise => requestPromise.json())
    .catch(response => Promise.reject(response));
};
