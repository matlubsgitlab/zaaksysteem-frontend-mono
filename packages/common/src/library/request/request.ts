import { request as fetch } from '@zaaksysteem/common/src/library/fetch';
import { V2ServerErrorType } from '../../types/ServerError';

export const request = async <T>(
  method: string,
  url: string,
  body?: any,
  type?: string
) => {
  const tryJSON = async (response: Response) => {
    try {
      return await response.json();
    } catch (error) {
      throw {
        response,
        errors: [{ code: 'invalid/syntax' }],
      };
    }
  };

  const handleError = async (
    response: Response
  ): Promise<V2ServerErrorType> => {
    const json = await tryJSON(response);

    if (/api\/v2/.test(response.url)) {
      throw {
        response,
        ...(json.errors && { errors: json.errors }),
      };
    } else {
      throw {
        response,
        ...(json.code && { errors: [{ code: json.code }] }),
      };
    }
  };

  const response = await fetch(method, url, body, type).catch(handleError);
  const json: T = await tryJSON(response);

  return json;
};
