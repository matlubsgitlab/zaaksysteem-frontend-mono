import { Dispatch, Action, MiddlewareAPI } from 'redux';

export type MiddlewareHelper<S, A = Action<any>> = (
  store: MiddlewareAPI<Dispatch<any>, S>,
  next: Dispatch<any>,
  action: A
) => any;
