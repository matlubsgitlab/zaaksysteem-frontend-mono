const fs = require('fs');
const { promisify } = require('util');
const handle = require('./promiseHandle');

const readFileAsync = promisify(fs.readFile);

async function readSchemaFile() {
  const [schema, schemaError] = await handle(
    readFileAsync('schema.json', 'UTF-8')
  );

  if (schemaError) throw Error('Error reading schema.json file from disk');
  return JSON.parse(schema);
}

module.exports = readSchemaFile;
