const { keys, entries } = Object;

/**
 * Get a shallow clone of an object without the specified properties.
 *
 * @param {Object} object
 * @param {*} exclusions
 * @return {Object}
 */
export const cloneWithout = (object, ...exclusions) =>
  keys(object).reduce((accumulator, key) => {
    if (!exclusions.includes(key)) {
      accumulator[key] = object[key];
    }

    return accumulator;
  }, {});

/**
 * @param {Object|string} value
 * @return {boolean}
 */
export const isObject = value => typeof value !== 'string';

/**
 * @param {Array} array
 * @return {Object}
 */
export function getObject(array) {
  const [object, invalid] = array.filter(value => isObject(value));

  if (object === undefined) {
    throw new TypeError('no object');
  }

  if (invalid !== undefined) {
    throw new TypeError('multiple objects');
  }

  return object;
}

/**
 * @param {Array} rest
 * @return {Object}
 */
export function purge(...rest) {
  const object = getObject(rest);

  return entries(object).reduce((accumulator, [key, value]) => {
    if (!rest.includes(key)) {
      accumulator[key] = value;
    }

    return accumulator;
  }, {});
}

/**
 * @param rest
 * @return {Array}
 */
export function extract(...rest) {
  const object = getObject(rest);

  const map = value => {
    if (value === object) {
      return purge(...rest);
    }

    return object[value];
  };

  return rest.map(map);
}

// ZS-FIXME: complicated and unreadable.
// 1) add ESDoc documentation for function purpose, paramaters and return value
// 2) write unit tests
// 3) enable ESLint
// 4) refactor
/*eslint-disable*/
export function get(obj, path, def) {
  let fullPath = path
    .replace(/\[/g, '.')
    .replace(/]/g, '')
    .split('.')
    .filter(Boolean);

  if (!obj) return def || undefined;

  return fullPath.every(everyFunc) ? obj : def;

  function everyFunc(step) {
    return !(step && obj && (obj = obj[step]) === undefined);
  }
}

/**
 * @param {Object} object
 * @param {Object} paramGetters
 * @return {Object}
 */
export function performGetOnProperties(object, paramGetters) {
  return keys(object).reduce((acc, param) => {
    acc[param] = get(object, paramGetters[param]);

    return acc;
  }, {});
}

/**
 * @param {Object} object
 * @param rest
 * @return {Object}
 */
export function filterProperties(object, ...rest) {
  return rest.reduce(function addProperty(accumulator, x) {
    if (object.hasOwnProperty(x)) {
      accumulator[x] = object[x];
    }

    return accumulator;
  }, {});
}

export function filterPropertiesOnValues(object, filter) {
  return keys(object).reduce(function filterProps(accumulator, x) {
    if (filter(object[x])) {
      accumulator[x] = object[x];
    }

    return accumulator;
  }, {});
}
