declare module '@mintlab/kitchen-sink/source' {
  function get<D, R, A = undefined>(
    data: D,
    valuePath: string,
    alternateValue?: A
  ): R | A;

  function buildUrl<P = { [key: string]: any }>(url: string, params: P): string;

  const ENCODED_NULL_BYTE: string;

  function asArray(data: any): any[];

  function toggleItem<T>(items: T[], item: T): T[];

  function capitalize(string: string): string;
}
