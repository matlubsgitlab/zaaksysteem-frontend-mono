import { createAjaxConstants } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';
export const SAVE_COMMUNICATION = createAjaxConstants('SAVE_COMMUNICATION');
export const SET_SAVE_COMMUNICATION_PENDING = 'SET_SAVE_COMMUNICATION_PENDING';
