import { ActionCreator } from 'redux';
import { ActionWithPayload } from '@zaaksysteem/common/src/types/ActionWithPayload';
import { CommunicationContextType } from '../../types/Context.types';
import { COMMUNICATION_SET_CONTEXT } from './communincation.context.constants';

export type CommunicationSetContextActionPayload = CommunicationContextType;

export const setCommunicationContext: ActionCreator<ActionWithPayload<
  CommunicationSetContextActionPayload
>> = (context: CommunicationSetContextActionPayload) => ({
  type: COMMUNICATION_SET_CONTEXT,
  payload: context,
});
