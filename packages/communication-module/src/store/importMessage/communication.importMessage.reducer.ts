import { Reducer } from 'redux';
import {
  AJAX_STATE_INIT,
  AjaxState,
} from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';
import { handleAjaxStateChange } from '@zaaksysteem/common/src/library/redux/ajax/handleAjaxStateChange';
import { AjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import { APICommunication } from '@zaaksysteem/generated';
import { IMPORT_MESSAGE } from './communication.importMessage.constants';
import { ImportMessagePayloadType } from './communication.importMessage.actions';

export interface CommunicationImportMessageState {
  state: AjaxState;
}

const initialState: CommunicationImportMessageState = {
  state: AJAX_STATE_INIT,
};

export const add: Reducer<
  CommunicationImportMessageState,
  AjaxAction<
    APICommunication.ImportEmailMessageResponseBody,
    ImportMessagePayloadType
  >
> = (state = initialState, action) => {
  const { type } = action;

  const handleSaveAjaxStateChange = handleAjaxStateChange(IMPORT_MESSAGE);

  switch (type) {
    case IMPORT_MESSAGE.PENDING:
    case IMPORT_MESSAGE.ERROR:
    case IMPORT_MESSAGE.SUCCESS:
      return handleSaveAjaxStateChange(state, action);
    default:
      return state;
  }
};

export default add;
