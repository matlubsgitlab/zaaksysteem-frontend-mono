import { combineReducers } from 'redux';
import threadList, {
  CommunicationThreadListState,
} from './threadList/communication.threadList.reducer';
import thread, {
  CommunicationThreadState,
} from './thread/communication.thread.reducer';
import add, { CommunicationAddState } from './add/communication.add.reducer';
import context, {
  CommunicationContextState,
} from './context/communication.context.reducer';
import importMessage, {
  CommunicationImportMessageState,
} from './importMessage/communication.importMessage.reducer';

interface CommunicationState {
  context: CommunicationContextState;
  add: CommunicationAddState;
  thread: CommunicationThreadState;
  threadList: CommunicationThreadListState;
  importMessage: CommunicationImportMessageState;
}

export interface CommunicationRootStateType {
  communication: CommunicationState;
}

export const communication = combineReducers<CommunicationState>({
  threadList,
  thread,
  add,
  context,
  importMessage,
});

export default communication;
