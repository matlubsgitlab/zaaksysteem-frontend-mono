import {
  AnyMessageType,
  ExternalMessageType,
} from '../../../types/Message.types';
import { CommunicationContextContextType } from '../../../types/Context.types';

const isExternalMessage = (
  message: AnyMessageType
): message is ExternalMessageType => {
  return typeof (message as ExternalMessageType).messageType !== 'undefined';
};

export const isMessageUnread = (
  message: AnyMessageType,
  context: CommunicationContextContextType
): boolean => {
  if (isExternalMessage(message)) {
    return context === 'pip'
      ? message.readPip === null
      : message.readEmployee === null;
  }

  return false;
};
