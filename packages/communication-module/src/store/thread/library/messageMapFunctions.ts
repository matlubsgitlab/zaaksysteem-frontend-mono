import { APICommunication } from '@zaaksysteem/generated';
import {
  MessageType,
  NoteMessageType,
  ContactMomentMessageType,
  AttachmentType,
  ExternalMessageType,
} from '../../../types/Message.types';
import { toParticipantsObject } from '../../library/participants';

const mapAttachments = (
  attachment: APICommunication.Attachment
): AttachmentType => ({
  id: attachment.id,
  name: attachment.attributes.name,
  size: attachment.attributes.size,
  //@ts-ignore
  downloadUrl: attachment.links.download,
  //@ts-ignore
  previewUrl: attachment.links.preview,
});

export const mapGenericMessageProperties = (
  message: APICommunication.Message
): MessageType => ({
  content: message.attributes.content,
  createdDate: message.meta.message_date || message.meta.created,
  id: message.id,
  summary: message.meta.summary,
  type: message.type,
  threadUuid: message.relationships.thread.data.id,
});

export const mapNoteMessage = (
  message: APICommunication.MessageNote
): NoteMessageType => ({
  ...mapGenericMessageProperties(message),
  sender: {
    name: message.relationships.created_by.data.attributes.name,
    type: message.relationships.created_by.data.attributes.type,
  },
});

export const mapContactMomentMessage = (
  message: APICommunication.MessageContactMoment
): ContactMomentMessageType => ({
  ...mapGenericMessageProperties(message),
  channel: message.attributes.channel,
  direction: message.attributes.direction,
  recipient: {
    name: message.relationships.recipient.data.attributes.name,
    type: message.relationships.recipient.data.attributes.type,
  },
  sender: {
    name: message.relationships.created_by.data.attributes.name,
    type: message.relationships.created_by.data.attributes.type,
  },
});

/* eslint complexity: [2, 7] */
export const mapExternalMessage = (
  message: APICommunication.MessageExternal
): ExternalMessageType => {
  const {
    attributes: { subject, message_type, participants, is_imported },
    relationships: { attachments, created_by },
    meta: { read_employee, read_pip },
  } = message;

  return {
    ...mapGenericMessageProperties(message),
    subject: subject || '',
    attachments: attachments.data.map(mapAttachments),
    messageType: message_type || 'pip',
    ...(participants
      ? { participants: toParticipantsObject(participants) }
      : {}),
    sender: created_by
      ? {
          name: created_by.data.attributes.name,
          type: created_by.data.attributes.type,
        }
      : null,
    hasSourceFile: is_imported,
    readEmployee: read_employee,
    readPip: read_pip,
  };
};
