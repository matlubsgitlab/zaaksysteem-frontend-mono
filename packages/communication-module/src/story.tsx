import React from 'react';
import { MemoryRouter } from 'react-router-dom';
import { createStore, combineReducers } from 'redux';
import { Provider } from 'react-redux';
//@ts-ignore
import { createStory, addStories } from '@mintlab/ui/App/story';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import { communication } from './store/communication.reducer';
import locale from './locale/communication.locale';

export const stories = (
  module: any,
  __dirname: string,
  stories: any,
  stores?: any
) => {
  const story = createStory(module, __dirname);

  story.addDecorator((story: () => React.ReactElement) => (
    <I18nResourceBundle resource={locale} namespace="communication">
      {story()}
    </I18nResourceBundle>
  ));

  story.addDecorator((story: () => React.ReactElement) => (
    <MemoryRouter>{story()}</MemoryRouter>
  ));

  story.addDecorator((story: () => React.ReactElement) => (
    <Provider store={createStore(combineReducers({ communication }))}>
      {story()}
    </Provider>
  ));

  addStories(story, stories, stores);
};

export default stories;
