import React from 'react';
import { useTranslation } from 'react-i18next';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';
import {
  FormDefinition,
  FormValuesType,
} from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { ImportMessageFormFields } from '../../../types/Message.types';

export type ImportMessageDialogPropsType = {
  caseUuid: string;
  onSubmit: (
    values: FormValuesType<ImportMessageFormFields>,
    caseUuid: string
  ) => void;
  busy: boolean;
  onClose: () => void;
  [key: string]: any;
};

const ImportMessageDialog: React.FunctionComponent<ImportMessageDialogPropsType> = ({
  onSubmit,
  onClose,
  caseUuid,
  busy,
}) => {
  const [t] = useTranslation('communication');

  const formDefinition: FormDefinition<ImportMessageFormFields> = [
    {
      name: 'file_uuid',
      type: fieldTypes.FILE_SELECT,
      value: null,
      required: true,
      format: 'file',
      accept: ['.msg', '.eml', '.mail'],
      multiValue: false,
      hint: t('dialogs.importMessage.select.hint'),
    },
  ];

  const handleOnSubmit = (values: FormValuesType<ImportMessageFormFields>) =>
    onSubmit(values, caseUuid);

  return (
    <FormDialog<ImportMessageFormFields>
      formDefinition={formDefinition}
      onSubmit={handleOnSubmit}
      onClose={onClose}
      saving={busy}
      title={t('dialogs.importMessage.title')}
      scope="import-message-dialog"
    />
  );
};

export default ImportMessageDialog;
