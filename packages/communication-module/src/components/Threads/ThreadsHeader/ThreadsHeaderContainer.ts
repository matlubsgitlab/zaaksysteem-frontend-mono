import { connect } from 'react-redux';
import { showDialog } from '@zaaksysteem/common/src/store/ui/dialog/dialog.actions';
import { CommunicationRootStateType } from '../../../store/communication.reducer';
import {
  TYPE_EXTERNAL_MESSAGE,
  TYPE_CONTACT_MOMENT,
  TYPE_NOTE,
} from '../../../library/communicationTypes.constants';
import { CommunicationContextCapabilitiesType } from '../../../types/Context.types';
import { IMPORT_MESSAGE_DIALOG } from '../../Dialogs/dialog.constants';
import ThreadsHeader, { ThreadsHeaderPropsType } from './ThreadsHeader';

type PropsFromStateType = Pick<
  ThreadsHeaderPropsType,
  | 'canCreate'
  | 'canFilter'
  | 'filterOptions'
  | 'rootPath'
  | 'defaultCreateType'
  | 'canImportMessage'
>;

const getDefaultCreateTypeFromCapabilities = (
  capabilities: CommunicationContextCapabilitiesType
): string => {
  const options: [string, () => boolean][] = [
    [TYPE_CONTACT_MOMENT, () => capabilities.canCreateContactMoment],
    [TYPE_NOTE, () => capabilities.canCreateNote],
    [
      TYPE_EXTERNAL_MESSAGE,
      () => capabilities.canCreateEmail || capabilities.canCreatePipMessage,
    ],
  ];

  const [defaultType] = options
    .filter(([, condition]) => condition())
    .map(([type]) => type);

  return defaultType || '';
};

const mapStateToProps = ({
  communication: {
    threadList: { threadList },
    context: { rootPath, capabilities },
  },
}: CommunicationRootStateType): PropsFromStateType => {
  const filterOptions = threadList.reduce<string[]>((acc, item) => {
    return acc.includes(item.type) ? acc : [...acc, item.type];
  }, []);
  const { canFilter, canCreate } = capabilities;

  return {
    rootPath,
    canFilter,
    filterOptions,
    canCreate,
    defaultCreateType: getDefaultCreateTypeFromCapabilities(capabilities),
    canImportMessage: capabilities.canImportMessage,
  };
};

type PropsFromDispatchType = Pick<ThreadsHeaderPropsType, 'onImportMessage'>;

const mapDispathToProps: PropsFromDispatchType = {
  onImportMessage: () =>
    showDialog({
      dialogType: IMPORT_MESSAGE_DIALOG,
    }),
};

export default connect<
  PropsFromStateType,
  PropsFromDispatchType,
  {},
  CommunicationRootStateType
>(
  mapStateToProps,
  mapDispathToProps
)(ThreadsHeader);
