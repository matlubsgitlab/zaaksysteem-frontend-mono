import React from 'react';
//@ts-ignore
import Icon from '@mintlab/ui/App/Material/Icon';
import { useDirectionIconStyleSheet } from './DirectionIcon.style';

type DirectionIconPropsType = {
  direction: keyof ReturnType<typeof useDirectionIconStyleSheet>;
};

const DirectionIcon: React.FunctionComponent<DirectionIconPropsType> = ({
  direction,
}) => {
  const classes = useDirectionIconStyleSheet();
  return (
    <Icon size={16} classes={{ root: `${classes.root} ${classes[direction]}` }}>
      forward
    </Icon>
  );
};

export default DirectionIcon;
