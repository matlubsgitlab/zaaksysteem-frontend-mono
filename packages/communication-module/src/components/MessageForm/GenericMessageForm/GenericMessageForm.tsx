import React, { ReactElement, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import FormRenderer from '@zaaksysteem/common/src/components/form/FormRenderer';
import translateFormDefinition from '@zaaksysteem/common/src/components/form/library/translateFormDefinition';
import { FormRendererRenderProps } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
//@ts-ignore
import Button from '@mintlab/ui/App/Material/Button';
//@ts-ignore
import { addScopeProp } from '@mintlab/ui/App/library/addScope';
import CaseFinder from '@zaaksysteem/common/src/components/form/fields/CaseFinder';
import {
  CASE_FINDER,
  CASE_SELECTOR,
  EMAIL_RECIPIENT,
} from '../../../library/communicationFieldTypes.constants';
import { resolveMagicStrings } from '../../../library/resolveMagicStrings';
import CaseSelector from './fields/CaseSelector/CaseSelector';
import { useGenericMessageFormStyle } from './GenericMessageForm.style';
import EmailRecipient from './fields/EmailRecipient/EmailRecipient';
import { GenericMessageFormPropsType } from './GenericMessageForm.types';
import { renderField } from './library/renderField';
import GenericMessageFormPreview from './GenericMessageFormPreview';

const typeMap = {
  [CASE_FINDER]: CaseFinder,
  [CASE_SELECTOR]: CaseSelector,
  [EMAIL_RECIPIENT]: EmailRecipient,
};

/* eslint complexity: [2, 7] */
function GenericAddForm({
  save,
  cancel,
  busy,
  formDefinition,
  formName,
  top,
  caseUuid,
  mapPreviewValues,
  enablePreview = false,
  primaryButtonLabelKey = 'forms.add',
}: GenericMessageFormPropsType): ReactElement {
  const [t] = useTranslation('communication');
  const [previewActive, setPreviewActive] = useState(false);
  const classes = useGenericMessageFormStyle();
  const translatedFormDefinition = useMemo(
    () => translateFormDefinition(formDefinition, t),
    [formDefinition]
  );

  const renderForm = ({
    fields,
    values,
    isValid,
  }: FormRendererRenderProps<any>) => {
    if (previewActive) {
      resolveMagicStrings(values, '');
    }

    const formContent = previewActive ? (
      <GenericMessageFormPreview
        values={values}
        fields={fields}
        mapPreviewValues={mapPreviewValues}
        caseUuid={caseUuid || ''}
      />
    ) : (
      fields.map(renderField({ classes, formName }))
    );

    return (
      <div className={classes.formWrapper}>
        {top}
        {formContent}

        <div className={classes.buttons}>
          <Button
            disabled={!isValid || busy}
            action={() => {
              save(values);
            }}
            presets={['primary', 'contained']}
            {...addScopeProp(formName, 'save')}
          >
            {t(primaryButtonLabelKey)}
          </Button>
          {enablePreview && (
            <Button
              action={() => {
                setPreviewActive(!previewActive);
              }}
              presets={['text', 'contained']}
              {...addScopeProp(formName, 'preview')}
            >
              {previewActive ? t('forms.closePreview') : t('forms.openPreview')}
            </Button>
          )}
          <Button
            action={cancel}
            presets={['text', 'contained']}
            {...addScopeProp(formName, 'cancel')}
          >
            {t('forms.cancel')}
          </Button>
        </div>
      </div>
    );
  };

  return (
    <FormRenderer
      formDefinition={translatedFormDefinition}
      fieldComponents={typeMap}
      isInitialValid={false}
    >
      {renderForm}
    </FormRenderer>
  );
}

export default GenericAddForm;
