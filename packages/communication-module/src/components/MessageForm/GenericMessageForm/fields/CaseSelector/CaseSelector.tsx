import React from 'react';
//@ts-ignore
import { DataProvider } from '@mintlab/ui/App/Abstract/DataProvider';
//@ts-ignore
import MultilineOption from '@mintlab/ui/App/Zaaksysteem/Select/Option/MultilineOption';
import FlatValueSelect from '@zaaksysteem/common/src/components/form/fields/FlatValueSelect';
import { FormFieldPropsType } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { fetchCases } from '../../../../../library/requests';
import { CaseChoiceType } from '../../../../../types/Message.types';

export type CaseSelectorConfigType = {
  contactUuid: string;
};

const fetchCasesHandler = (
  contactUuid: string
): Promise<CaseChoiceType[] | void> => {
  return fetchCases(contactUuid)
    .then(response =>
      response.data
        .filter(caseItem => caseItem.attributes.status !== 'resolved')
        .map<CaseChoiceType>(caseItem => ({
          value: caseItem.id,
          label: `${caseItem.attributes.display_id}: ${caseItem.attributes.case_type_name}`,
          subLabel: caseItem.attributes.description,
        }))
    )
    .catch(() => {});
};

export interface CaseSelectorPropsType
  extends FormFieldPropsType<unknown, CaseSelectorConfigType> {}

const CaseSelector: React.ComponentType<CaseSelectorPropsType> = ({
  config,
  ...restProps
}) => {
  if (!config || !config.contactUuid) {
    console.error(
      `The CaseSelector component cannot be used without config.contactUuid.`
    );
    return null;
  }

  return (
    <DataProvider
      autoProvide={true}
      provider={fetchCasesHandler}
      providerArguments={[config.contactUuid]}
    >
      {({ data, busy }: { data: CaseChoiceType[] | null; busy: boolean }) => {
        const normalizedChoices = data || [];
        return (
          <FlatValueSelect
            {...restProps}
            choices={normalizedChoices}
            isClearable={true}
            loading={busy}
            components={{
              Option: MultilineOption,
            }}
            filterOption={() => true}
          />
        );
      }}
    </DataProvider>
  );
};

export default CaseSelector;
