import {
  Rule,
  valueEquals,
  showFields,
  hideFields,
} from '@zaaksysteem/common/src/components/form/rules';
import { EmailRecipientFormValuesType } from './EmailRecipient.types';

export default [
  new Rule<EmailRecipientFormValuesType>('recipient_type')
    .when(valueEquals('other'))
    .then(showFields(['other']))
    .else(hideFields(['other'])),

  new Rule<EmailRecipientFormValuesType>('recipient_type')
    .when(valueEquals('colleague'))
    .then(showFields(['colleague']))
    .else(hideFields(['colleague'])),

  new Rule<EmailRecipientFormValuesType>('recipient_type')
    .when(valueEquals('role'))
    .then(showFields(['role']))
    .else(hideFields(['role'])),

  new Rule<EmailRecipientFormValuesType>('recipient_type')
    .when(valueEquals('authorized'))
    .then(showFields(['authorized']))
    .else(hideFields(['authorized'])),

  new Rule<EmailRecipientFormValuesType>('recipient_type')
    .when(valueEquals('requestor'))
    .then(showFields(['requestor']))
    .else(hideFields(['requestor'])),
];
