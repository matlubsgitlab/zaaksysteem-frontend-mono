import React, { ComponentType, useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import classnames from 'classnames';
import {
  FormFieldPropsType,
  FormRendererRenderProps,
  FormValuesType,
  NestedFormValue,
} from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import FormRenderer from '@zaaksysteem/common/src/components/form/FormRenderer';
import translateFormDefinition from '@zaaksysteem/common/src/components/form/library/translateFormDefinition';
import mapValuesToFormDefinition from '@zaaksysteem/common/src/components/form/library/mapValuesToFormDefinition';
import { useStyles } from './EmailRecipient.style';
import { EmailRecipientFormValuesType } from './EmailRecipient.types';
import rules from './EmailRecipient.rules';
import getFormDefinition from './emailRecipient.formDefinition';

export type EmailRecipientConfigType = {
  caseUuid?: string;
  selectedRecipientType?: string;
};

export interface EmailRecipientPropsType
  extends FormFieldPropsType<
    EmailRecipientFormValuesType,
    EmailRecipientConfigType,
    NestedFormValue | NestedFormValue[]
  > {}

const getRecipientTypeFromValue = (
  value: EmailRecipientPropsType['value'],
  multiValue?: boolean
) =>
  multiValue
    ? (value as NestedFormValue[])[0]?.recipientType
    : (value as NestedFormValue)?.recipientType;

export const EmailRecipient: ComponentType<EmailRecipientPropsType> = ({
  name,
  setFieldValue,
  setFieldTouched,
  refValue,
  value,
  multiValue,
  error,
  config,
}) => {
  const [t] = useTranslation('communication');
  const classes = useStyles();
  const selectedRecipientType =
    getRecipientTypeFromValue(value, multiValue) ||
    config?.selectedRecipientType;

  const formDefinition = useMemo(() => {
    const caseUuid = config && config.caseUuid ? config.caseUuid : refValue;
    return getFormDefinition({
      ...config,
      selectedRecipientType,
      caseUuid: caseUuid ? caseUuid.toString() : undefined,
    });
  }, [config, refValue]);

  const translatedFormDefinition = useMemo(() => {
    return translateFormDefinition(formDefinition, t);
  }, [formDefinition]);

  const formDefinitionWithValues = useMemo(() => {
    return selectedRecipientType
      ? mapValuesToFormDefinition<EmailRecipientFormValuesType>(
          {
            [selectedRecipientType]: value,
          },
          translatedFormDefinition
        )
      : translatedFormDefinition;
  }, [formDefinition, config]);

  const getActiveField = (
    values: FormValuesType<EmailRecipientFormValuesType>
  ): keyof EmailRecipientFormValuesType => {
    switch (values.recipient_type) {
      case 'colleague':
      case 'role':
      case 'authorized':
      case 'requestor':
        return values.recipient_type;

      default:
        return 'other';
    }
  };

  return (
    <FormRenderer
      onChange={values => {
        const value = values[getActiveField(values)] || [];
        const valueAsArray = Array.isArray(value)
          ? (value as NestedFormValue[])
          : ([value] as NestedFormValue[]);

        setFieldValue(
          name,
          valueAsArray.map(item => ({
            ...item,
            recipientType: values.recipient_type,
          }))
        );
      }}
      onTouched={(touched, values) => {
        setFieldTouched(name, Boolean(touched[getActiveField(values)]));
      }}
      rules={rules}
      formDefinition={formDefinitionWithValues}
    >
      {({ fields }: FormRendererRenderProps<EmailRecipientFormValuesType>) => {
        return (
          <div>
            {fields.map(
              ({
                name: fieldName,
                FieldComponent,
                applyBackgroundColor,
                label,
                ...restFieldProps
              }) => (
                <div key={fieldName} className={classes.row}>
                  {label && <div className={classes.label}>{label}</div>}
                  <div
                    className={classnames(
                      applyBackgroundColor && classes.withBackground,
                      classes.field
                    )}
                  >
                    <FieldComponent
                      name={fieldName}
                      error={error}
                      {...restFieldProps}
                    />
                  </div>
                </div>
              )
            )}
          </div>
        );
      }}
    </FormRenderer>
  );
};

export default EmailRecipient;
