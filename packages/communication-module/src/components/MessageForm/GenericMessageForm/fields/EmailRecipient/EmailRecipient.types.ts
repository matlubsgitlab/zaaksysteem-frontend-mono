import { NestedFormValue } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';

export type EmailRecipientFormValuesType = {
  recipient_type: string;
  other: NestedFormValue[];
  colleague: NestedFormValue[];
  role: NestedFormValue[];
  authorized: NestedFormValue[];
  requestor: NestedFormValue;
};
