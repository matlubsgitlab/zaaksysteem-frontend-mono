import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
//@ts-ignore
import Button from '@mintlab/ui/App/Material/Button';
//@ts-ignore
import { addScopeProp } from '@mintlab/ui/App/library/addScope';
import EmailReplyFormContainer from './Email/EmailReplyFormContainer';
import PipReplyContainer from './Pip/PipReplyContainer';

export interface ReplyFormPropsType {
  type: 'pip' | 'email';
  caseUuid: string;
}

const ReplyForm: React.ComponentType<ReplyFormPropsType> = ({
  type,
  caseUuid,
}) => {
  const [replyIsOpen, setReplyOpen] = useState(false);
  const [t] = useTranslation('communication');
  const closeReplyForm = () => setReplyOpen(false);

  return replyIsOpen ? (
    <div>
      {type === 'email' ? (
        <EmailReplyFormContainer caseUuid={caseUuid} cancel={closeReplyForm} />
      ) : (
        <PipReplyContainer caseUuid={caseUuid} cancel={closeReplyForm} />
      )}
    </div>
  ) : (
    <Button
      action={() => {
        setReplyOpen(true);
      }}
      icon="add"
      presets={['primary', 'contained']}
      {...addScopeProp(type, 'reply', 'open')}
    >
      {t('replyForm.openButtonLabel')}
    </Button>
  );
};

export default ReplyForm;
