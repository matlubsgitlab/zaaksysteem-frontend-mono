import React from 'react';
import { SaveMessageFormValuesType } from '../../../../types/Message.types';
import {
  SimpleExternalMessageForm,
  SimpleExternalMessageFormProps,
} from '../../SimpleExternalMessageForm/SimpleExternalMessageForm';

export interface PipReplyFormPropsType
  extends Pick<
    SimpleExternalMessageFormProps,
    | 'initialValues'
    | 'caseUuid'
    | 'contactUuid'
    | 'cancel'
    | 'busy'
    | 'context'
    | 'enablePreview'
  > {
  caseUuid: string;
  doSave: (
    caseUuid: string,
    threadUuid: string,
    values: SaveMessageFormValuesType
  ) => void;
  threadUuid: string;
}

export const PipReplyForm: React.ComponentType<PipReplyFormPropsType> = ({
  doSave,
  caseUuid,
  threadUuid,
  ...rest
}) => {
  return (
    <div>
      <SimpleExternalMessageForm
        {...rest}
        caseUuid={caseUuid}
        save={values => doSave(caseUuid, threadUuid, values)}
      />
    </div>
  );
};

export default PipReplyForm;
