import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { FormDefinition } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { SaveNoteFormValuesType } from '../../../../types/Message.types';

const noteFormDefinition: FormDefinition<SaveNoteFormValuesType> = [
  {
    name: 'content',
    type: fieldTypes.TEXT,
    multi: true,
    value: '',
    required: true,
    placeholder: 'communication:addFields.note',
    isMultiline: true,
    rows: 7,
  },
];

export default noteFormDefinition;
