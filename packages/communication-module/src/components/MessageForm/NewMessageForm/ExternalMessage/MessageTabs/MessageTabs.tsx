import React from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import MUITabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import {
  TYPE_PIP_MESSAGE,
  TYPE_EMAIL,
  TYPE_MIJN_OVERHEID,
  TYPE_EXTERNAL_MESSAGE,
} from '../../../../../library/communicationTypes.constants';
import { useMessageTabsStyle } from './MessageTabs.style';

export interface MessageTabsPropsType {
  subtype: string;
  rootPath: string;
  canCreatePipMessage: boolean;
  canCreateEmail: boolean;
  canCreateMijnOverheid: boolean;
}

/* eslint-disable complexity */
const Tabs: React.FunctionComponent<MessageTabsPropsType> = ({
  rootPath,
  canCreatePipMessage,
  canCreateEmail,
  canCreateMijnOverheid,
  subtype,
}) => {
  type LinkComponentPropsType = {
    linktype: string;
  };

  const LinkComponent = React.forwardRef<
    HTMLAnchorElement,
    LinkComponentPropsType
  >((props, ref) => (
    <Link
      innerRef={ref}
      to={`${rootPath}/new/${TYPE_EXTERNAL_MESSAGE}/${props.linktype}`}
      {...props}
    />
  ));
  LinkComponent.displayName = 'TabLink';

  const [t] = useTranslation('communication');
  const classes = useMessageTabsStyle();

  if (!canCreatePipMessage || !canCreateEmail) {
    return null;
  }

  return (
    <MUITabs
      classes={{
        root: classes.tabsRoot,
        indicator: classes.indicator,
      }}
      value={subtype}
      indicatorColor="primary"
    >
      {canCreatePipMessage && (
        <Tab
          label={t('threadTypes.pip_message')}
          value={TYPE_PIP_MESSAGE}
          linktype={TYPE_PIP_MESSAGE}
          disableTouchRipple={true}
          component={LinkComponent}
          classes={{
            wrapper: classes.tabWrapper,
          }}
        />
      )}
      {canCreateEmail && (
        <Tab
          label={t('threadTypes.email')}
          value={TYPE_EMAIL}
          linktype={TYPE_EMAIL}
          disableTouchRipple={true}
          component={LinkComponent}
          classes={{
            wrapper: classes.tabWrapper,
          }}
        />
      )}
      {canCreateMijnOverheid && (
        <Tab
          label={t('threadTypes.mijn_overheid')}
          value={TYPE_MIJN_OVERHEID}
          linktype={TYPE_MIJN_OVERHEID}
          disableTouchRipple={true}
          component={LinkComponent}
          classes={{
            wrapper: classes.tabWrapper,
          }}
        />
      )}
    </MUITabs>
  );
};

export default Tabs;
