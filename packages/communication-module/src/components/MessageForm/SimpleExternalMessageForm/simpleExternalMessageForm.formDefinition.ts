import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import {
  FormDefinition,
  FormDefinitionField,
} from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { CaseDocumentSelectConfigType } from '@zaaksysteem/common/src/components/form/fields/CaseDocumentSelect/CaseDocumentSelect';
import { CaseRequestorConfigType } from '@zaaksysteem/common/src/components/form/fields/CaseRequestor/CaseRequestor.types';
import {
  isNotPresetRequestor,
  createDisplayName,
} from '@zaaksysteem/common/src/library/requests/fetchCaseRoles';
import { CASE_SELECTOR } from '../../../library/communicationFieldTypes.constants';
import { SaveMessageFormValuesType } from '../../../types/Message.types';
import { CommunicationContextContextType } from '../../../types/Context.types';
import { CaseSelectorConfigType } from '../GenericMessageForm/fields/CaseSelector/CaseSelector';

type SimpleExternalMessageFormDefinitionPayloadType = {
  caseUuid?: string;
  contactUuid?: string;
  context: CommunicationContextContextType;
};

/* eslint complexity: [2, 8] */
export const getSimpleExternalMessageFormDefinition = ({
  caseUuid,
  contactUuid,
  context,
}: SimpleExternalMessageFormDefinitionPayloadType): FormDefinition<SaveMessageFormValuesType> => [
  ...(caseUuid && context !== 'pip'
    ? [
        <
          FormDefinitionField<
            SaveMessageFormValuesType,
            CaseRequestorConfigType
          >
        >{
          name: 'requestor',
          type: fieldTypes.CASE_REQUESTOR,
          value: null,
          applyBackgroundColor: true,
          config: {
            caseUuid,
            itemFilter: isNotPresetRequestor,
            valueResolver: role => ({
              label: createDisplayName(role),
              value: role.subject?.id,
            }),
          },
        },
      ]
    : []),
  ...(contactUuid && !caseUuid
    ? [
        <
          FormDefinitionField<SaveMessageFormValuesType, CaseSelectorConfigType>
        >{
          name: 'case_uuid',
          type: CASE_SELECTOR,
          value: '',
          required: true,
          placeholder: 'communication:addFields.selectCase',
          label: 'communication:addFields.selectCase',
          isSearchable: false,
          config: {
            contactUuid,
          },
        },
      ]
    : [
        <FormDefinitionField<SaveMessageFormValuesType>>{
          name: 'case_uuid',
          value: caseUuid,
          type: fieldTypes.TEXT,
          hidden: true,
        },
      ]),
  {
    name: 'subject',
    type: fieldTypes.TEXT,
    multi: true,
    format: 'text',
    value: '',
    required: true,
    placeholder: 'communication:addFields.subject',
  },
  {
    name: 'content',
    type: fieldTypes.TEXTAREA,
    multi: true,
    format: 'text',
    value: '',
    required: true,
    placeholder: 'communication:addFields.message',
    isMultiline: true,
    rows: 7,
  },
  ...(caseUuid && context !== 'pip'
    ? [
        <
          FormDefinitionField<
            SaveMessageFormValuesType,
            CaseDocumentSelectConfigType
          >
        >{
          name: 'attachments',
          type: fieldTypes.CASE_DOCUMENT_SELECT,
          value: null,
          placeholder: 'communication:addFields.attachmentsPlaceholder',
          multiValue: true,
          applyBackgroundColor: true,
          config: { caseUuid },
        },
      ]
    : [
        <FormDefinitionField<SaveMessageFormValuesType>>{
          name: 'attachments',
          type: fieldTypes.FILE_SELECT,
          value: null,
          required: false,
          format: 'file',
          uploadDialog: true,
          multiValue: true,
        },
      ]),
];

export default getSimpleExternalMessageFormDefinition;
