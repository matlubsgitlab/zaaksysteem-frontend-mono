import React, { useState } from 'react';
import { DocumentPreviewModal } from '@zaaksysteem/common/src/components/DocumentPreview';
import { AttachmentType } from '../../../types/Message.types';
import { useAttachmentListStyle } from './AttachmentList.style';
import Attachment, { AttachmentPropsType } from './Attachment/Attachment';

export type AttachmentListPropsType = {
  attachments: AttachmentType[];
} & Pick<AttachmentPropsType, 'canBeAddedToCase'> & {
    addToCase: (id: string) => void;
    canOpenPDFPreview: boolean;
  };

const showHideContextualMenu = (show: boolean) => {
  top.window.postMessage(
    {
      type: 'showHideContextualMenu',
      data: {
        show,
      },
    },
    '*'
  );
};

const AttachmentList: React.FunctionComponent<AttachmentListPropsType> = ({
  attachments,
  canBeAddedToCase,
  canOpenPDFPreview,
  addToCase,
}) => {
  const classes = useAttachmentListStyle();
  const [previewOpen, setPreviewOpen] = useState(false);
  const [previewURL, setPreviewURL] = useState('');

  return (
    <div className={classes.wrapper}>
      <DocumentPreviewModal
        open={previewOpen}
        title={'Document preview'}
        url={previewURL}
        onClose={() => {
          setPreviewOpen(false);
          showHideContextualMenu(true);
        }}
      />
      {attachments.map((attachment: AttachmentType) => (
        <Attachment
          key={attachment.id}
          {...attachment}
          canBeAddedToCase={canBeAddedToCase}
          onAddToCase={() => addToCase(attachment.id)}
          {...(canOpenPDFPreview && attachment.previewUrl
            ? {
                handlePreviewAction: () => {
                  if (attachment.previewUrl) {
                    setPreviewURL(attachment.previewUrl);
                    setPreviewOpen(true);
                    showHideContextualMenu(false);
                  }
                },
              }
            : {})}
        />
      ))}
    </div>
  );
};

export default AttachmentList;
