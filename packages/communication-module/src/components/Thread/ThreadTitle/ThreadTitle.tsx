import React from 'react';
import { useTranslation } from 'react-i18next';
//@ts-ignore
import { H4 } from '@mintlab/ui/App/Material/Typography';
import Tag from '../../shared/Tag/Tag';
import TopLink from '../../shared/TopLink/TopLink';
import ActionMenu, {
  ActionGroupsType,
  ActionGroupType,
} from '../../shared/ActionMenu/ActionMenu';
import { CommunicationContextContextType } from '../../../types/Context.types';
import getPathToCaseForContext from '../../../library/getPathToCaseForContext';
import { useThreadTitleStyle } from './ThreadTitle.style';

export type ThreadTitlePropsType = {
  title: string;
  context: CommunicationContextContextType;
  caseNumber?: number;
  showLinkToCase?: boolean;
  actionButtons?: ActionGroupsType | ActionGroupType;
};

const ThreadTitle: React.FunctionComponent<ThreadTitlePropsType> = ({
  title,
  context,
  caseNumber,
  showLinkToCase,
  actionButtons = [],
}) => {
  const classes = useThreadTitleStyle();
  const [t] = useTranslation('communication');

  return (
    <div className={classes.wrapper}>
      <H4 classes={{ root: classes.title }}>{title}</H4>
      {showLinkToCase && caseNumber && (
        <Tag style={classes.tag}>
          <TopLink
            style={classes.link}
            href={getPathToCaseForContext(context, caseNumber)}
          >
            {`${t('thread.tags.case')} ${caseNumber}`}
          </TopLink>
        </Tag>
      )}
      {Boolean(actionButtons.length) && (
        <ActionMenu actions={actionButtons} scope={'thread:message:header'} />
      )}
    </div>
  );
};

export default ThreadTitle;
