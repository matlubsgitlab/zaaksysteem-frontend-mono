import React from 'react';
import { useTranslation } from 'react-i18next';
import { ExternalMessageType } from '../../../types/Message.types';
import ThreadTitle, { ThreadTitlePropsType } from './ThreadTitle';

export type EmailThreadTitlePropsType = {
  message: ExternalMessageType;
  showAddThreadToCaseDialog: () => void;
  canAddThreadToCase: boolean;
} & Pick<ThreadTitlePropsType, 'context' | 'showLinkToCase'>;

const EmailThreadTitle: React.FunctionComponent<EmailThreadTitlePropsType> = ({
  message,
  showAddThreadToCaseDialog,
  canAddThreadToCase,
  ...rest
}) => {
  const [t] = useTranslation('communication');
  const addThreadToCaseAction = canAddThreadToCase
    ? [
        {
          action: showAddThreadToCaseDialog,
          label: t('threads.actions.addThreadToCase'),
        },
      ]
    : [];
  const actionButtons = [...addThreadToCaseAction];

  return (
    <ThreadTitle
      {...rest}
      title={message.subject}
      actionButtons={actionButtons}
    />
  );
};

export default EmailThreadTitle;
