import React from 'react';
import { useTranslation } from 'react-i18next';
import { useConfirmDialog } from '@zaaksysteem/common/src/hooks/useConfirmDialog';
import { APIDocument } from '@zaaksysteem/generated';
import ActionMenu, {
  ActionGroupsType,
} from '../../../shared/ActionMenu/ActionMenu';
import { SubTypeOfMessageOrNone } from '../../../../types/Message.types';

export type MessageActionsPropsType = {
  id: string;
  messageType: SubTypeOfMessageOrNone;
  onAddSourceFileToCaseAction: (
    messageUuid: string,
    messageType: SubTypeOfMessageOrNone,
    outputFormat: APIDocument.CreateDocumentFromEmailRequestBody['output_format']
  ) => void;
  onDeleteAction: (messageUuid: string) => void;
  canDeleteMessage: boolean;
  canAddSourceFileToCase: boolean;
  hasSourceFile: boolean;
};

const MessageActions: React.ComponentType<MessageActionsPropsType> = ({
  onAddSourceFileToCaseAction,
  onDeleteAction,
  id,
  canAddSourceFileToCase,
  canDeleteMessage,
  hasSourceFile,
  messageType,
}) => {
  const [t] = useTranslation('communication');
  const [confirmDeleteDialog, confirmDelete] = useConfirmDialog(
    t('thread.deleteMessage.title'),
    t('thread.deleteMessage.body'),
    () => onDeleteAction(id)
  );
  const mainActionGroup = [
    {
      label: t('thread.messageActions.delete'),
      action: confirmDelete,
      condition: canDeleteMessage,
    },
    {
      label: t('thread.messageActions.addSourceFileToCaseOriginal'),
      action: () => onAddSourceFileToCaseAction(id, messageType, 'original'),
      condition:
        (canAddSourceFileToCase && hasSourceFile) || messageType === 'pip',
    },
    {
      label: t('thread.messageActions.addSourceFileToCasePDF'),
      action: () => onAddSourceFileToCaseAction(id, messageType, 'pdf'),
      condition: canAddSourceFileToCase && messageType === 'email',
    },
  ];
  const actions: ActionGroupsType = [mainActionGroup];

  return mainActionGroup.length > 0 ? (
    <React.Fragment>
      <ActionMenu actions={actions} scope={'thread:message:header'} />
      {confirmDeleteDialog}
    </React.Fragment>
  ) : null;
};

export default MessageActions;
