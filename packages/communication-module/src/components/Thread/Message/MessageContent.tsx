import React from 'react';
import classNames from 'classnames';
//@ts-ignore
import { Body2 } from '@mintlab/ui/App/Material/Typography';
import { AttachmentType } from '../../../types/Message.types';
import {
  TYPE_CONTACT_MOMENT,
  TYPE_NOTE,
} from './../../../library/communicationTypes.constants';
import AttachmentListContainer from './../AttachmentList/AttachmentListContainer';
import { useMessageContentStyle } from './MessageContent.style';

type MessageContentPropsType = {
  content: string;
  type: string;
  attachments?: AttachmentType[];
};

const MessageContent: React.FunctionComponent<MessageContentPropsType> = ({
  content,
  type,
  attachments,
}) => {
  const classes = useMessageContentStyle();
  return (
    <div
      className={classNames(classes.content, {
        [classes.background]: [TYPE_CONTACT_MOMENT, TYPE_NOTE].includes(type),
      })}
    >
      <Body2 classes={{ root: classes.body }}>{content}</Body2>
      {attachments && <AttachmentListContainer attachments={attachments} />}
    </div>
  );
};

export default MessageContent;
