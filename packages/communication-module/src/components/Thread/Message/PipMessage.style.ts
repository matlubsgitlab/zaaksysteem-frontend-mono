import { makeStyles } from '@material-ui/core';

export const usePipMessageStyle = makeStyles({
  panel: {
    padding: 0,
    boxShadow: 'none',
    '&:before': {
      height: 0,
    },
  },
  summary: { padding: 0 },
  content: {
    margin: 0,
    display: 'inline',
    minWidth: 0,
  },
  expanded: {
    margin: 0,
    '&$expanded': {
      margin: 0,
      minHeight: 48,
    },
  },
  details: { padding: 0, display: 'inline' },
});
