import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import {
  ExpansionPanel,
  ExpansionPanelSummary,
  ExpansionPanelDetails,
} from '@material-ui/core';
import { ExternalMessageType } from '../../../types/Message.types';
import MessageHeader from './MessageHeader';
import MessageContent from './MessageContent';
import { usePipMessageStyle } from './PipMessage.style';
import MessageSenderIcon from './MessageSenderIcon/MessageSenderIcon';
import CreateExternalMessageTitle from './library/CreateExternalMessageTitle';

type PipMessagePropsType = {
  message: ExternalMessageType;
  caseNumber?: string;
  expanded?: boolean;
  isUnread: boolean;
};

export const PipMessage: React.FunctionComponent<PipMessagePropsType> = ({
  message,
  isUnread,
  expanded = false,
}) => {
  const classes = usePipMessageStyle();
  const [t] = useTranslation('communication');
  const {
    content,
    createdDate,
    sender,
    type,
    summary,
    subject,
    attachments,
    id,
  } = message;
  const [isExpanded, setExpanded] = useState(isUnread || expanded);
  const icon = <MessageSenderIcon type={sender ? sender.type : 'person'} />;
  const title = CreateExternalMessageTitle(message);

  return (
    <ExpansionPanel
      classes={{ root: classes.panel, expanded: classes.expanded }}
      expanded={isExpanded}
    >
      <ExpansionPanelSummary
        onClick={() => setExpanded(!isExpanded)}
        classes={{
          root: classes.summary,
          content: classes.content,
          expanded: classes.expanded,
        }}
      >
        <MessageHeader
          date={createdDate}
          title={title}
          icon={icon}
          id={id}
          isUnread={isUnread}
          info={isExpanded ? subject : summary}
          hasAttachment={Boolean(attachments.length)}
          {...(sender &&
            sender.type === 'employee' && {
              subTitle: t('thread.pipMessage.subTitle'),
            })}
        />
      </ExpansionPanelSummary>
      <ExpansionPanelDetails classes={{ root: classes.details }}>
        <MessageContent
          content={content}
          type={type}
          attachments={attachments}
        />
      </ExpansionPanelDetails>
    </ExpansionPanel>
  );
};

export default PipMessage;
