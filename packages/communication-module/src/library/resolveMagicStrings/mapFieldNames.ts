import { FieldMapType } from './resolveMagicStrings.types';

const getFieldName = (name: string): [string, string] | undefined => {
  const fieldMap: FieldMapType = [
    ['content', 'body'],
    ['to', 'to'],
    ['cc', 'cc'],
    ['bcc', 'bcc'],
    ['subject', 'subject'],
  ];

  return fieldMap.filter(
    ([internalName, externalName]) =>
      name === internalName || name === externalName
  )[0];
};

export const mapFieldNameToApi = (name: string): string | null => {
  const fieldName = getFieldName(name);
  return fieldName ? fieldName[1] : null;
};

export const mapFieldNameToInternal = (name: string): string => {
  const fieldName = getFieldName(name);
  return fieldName ? fieldName[0] : name;
};
